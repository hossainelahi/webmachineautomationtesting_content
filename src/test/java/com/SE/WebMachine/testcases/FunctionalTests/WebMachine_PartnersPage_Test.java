package com.SE.WebMachine.testcases.FunctionalTests;


/* LJ Java SE Libraries*/
import java.util.Hashtable;
import java.util.Map;

/* DH Java third party Libraries*/
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/* DH Java Schneider Libraries*/
import com.SE.WebMachine.base.WebMachine_Base_Page;
//import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.exceptions.Schneider_TimeoutException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.pages.StickyBars;
import com.SE.WebMachine.pages.WebMachine_DispatchPage;
import com.SE.WebMachine.pages.WebMachine_PartnersPage;
//import com.SE.WebMachine.pages.WebMachine_WorkPage;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.DataUtil;

public class WebMachine_PartnersPage_Test extends WebMachine_BasePage_Test{


@Parameters({"aut_environment"})
@BeforeSuite
public void checkSuiteRunStatus(@Optional String environment){

	boolean fromTestNG = environment == null || environment.length() == 0 ? false : true;
	super.setUp(environment);
	logTestEnvironment("A", fromTestNG);
	logTestDomain();

}
@Test(dataProvider = "WebMachinePartnersPage_TestData")
public void WebMachine_HomePageTest(Hashtable<String, String> configData, 
		Hashtable<Integer, String> testSteps, Map<String, Schneider_PageElement> pageElements){
	
	if(configData.get("Runmode").equalsIgnoreCase("N")){
		System.out.println("Skipping as Flag is N for this data set for WebMachine_PartnersPageTest");
		throw new SkipException("Skipping as Flag is N for this data set");
	}
	Reporter.log("\n", true);	
	Reporter.log("\n", true);	
	Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
	Reporter.log(logTime() + " " + ":::::START: PARTNERS' PAGE TEST FOR WEBMACHINE:::::::::::::::" , true);
	Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);

	try{
		//Schneider_PageElement p = pageElements.get("acceptCookies");
	WebMachine_DispatchPage.initialize(configData);
	WebMachine_DispatchPage.launch(testData.get("baseURL"));
//	WebMachine_DispatchPage.skipVideoLink(pageElements.get("skipVideo"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
//	WebMachine_DispatchPage.clickCookiesLink(pageElements.get("acceptCookies"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
//	WebMachine_DispatchPage.stayOnGlobalWebsite(pageElements.get("stayOnGlobalWebsite"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
	WebMachine_DispatchPage.awaitPresenceOfElement(pageElements.get("schneiderLogo"));
	
	
	boolean overallResult = true;
	testngLevel = configData.get("TestingLevel");
	summariseTestingLevel(this.getClass().getSimpleName(), testngLevel);
	Reporter.log("\n", true);
	

	overallResult = getOverallResult(overallResult, carouselDispatch("1", "Dispatch page")); 	// done Lokesh	
	
	overallResult = getOverallResult(overallResult, validateStickyBars("2", "Partners' page")); 	// done Lokesh
	overallResult = getOverallResult(overallResult, validateTiles("3", "Partners' page")); 	// done Lokesh
	

	Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		Reporter.log(logTime() + " " + ":::END: PARTNERS' PAGE TEST FOR WEBMACHINE:::::::::::::::::" , true);
//		System.out.println("value of overall result is >> "+overallResult);
		summariseTest(this.getClass().getSimpleName(), overallResult);
	//	extent.endTest();
		
		
	}
	catch (Schneider_TimeoutException sTE){
		Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + sTE.getMessage() + ". Site is no longer available to test.", true);	
	} 
	/*catch (Schneider_IndexOutOfBoundsException iOOBE) {
		Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + iOOBE.getMessage() + ".", true);		}
*/	catch (Throwable t) {
		Reporter.log(logTime() + " " + ":::::::TEST ABORTED. Please investiage the Error or Exception in the following Stack trace ...", true);
		t.printStackTrace();
	}		
}


// go to base url and then click on @Home and return home landing page url

private String zoneUrl(String zoneLink, String whichPage){
	
	String reqZoneUrl = WebMachine_Base_Page.getZoneUrl(zoneLink ,whichPage,testData.get("baseURL"));
	//System.out.println(reqZoneUrl+ "   finally");
	return reqZoneUrl;
}


	// added by Lokesh 8-2015  // req 11  // dispatch page zones validation
	
	//@Test(dependsOnMethods = { "validateLifeIsOnLogo" }, description = "Ensure that the current page contains required web elements")
		@Test(dependsOnMethods = { "validateCountries" }, description = "Ensure that the current page contains required web elements")
		private boolean carouselDispatch(String reqNo, String pageLocation) {
			Reporter.log(logTime() + "  " + "Req " +reqNo + " START. " + "Validation " + pageLocation + " "+ " Main", true);
			boolean result = WebMachine_DispatchPage.carouselDispatch("Req 1", "Dispatch", "Carousel", testData.get("baseURL"));		
			String status = result == true ? "PASSED" : "FAILED";
			Reporter.log(logTime() + "  " + "Req " +reqNo + " END. " + "Validation " + pageLocation + " "+ " Main - " + status, true);
			return evaluateResult(result, pageLocation+ " Main");
		}

		// added by Lokesh 8-2015  // req 12
	//	@Test(dependsOnMethods = { "validateSELogo" }, description = "Ensure that the current page contains required web elements")
		@Test(dependsOnMethods = { "carouselDispatch" }, description = "Ensure that the current page contains required web elements")
		private boolean validateStickyBars(String reqNo, String pageLocation) {
			Reporter.log(logTime() + "  " + "Req " +reqNo + " START. " + "Validation " + pageLocation + " "+ " Main", true);
			WebMachine_PartnersPage.getStickyBars();
			boolean result = StickyBars.validateStickyBars("Req 2", "Work", "Sticky Bars", zoneUrl(Constants.WebMachine_DispatchPage.zoneCLink,"Dispatch"));		
			String status = result == true ? "PASSED" : "FAILED";
			Reporter.log(logTime() + "  " + "Req " +reqNo + " END. " + "Validation " + pageLocation + " "+ " Main - " + status, true);
			return evaluateResult(result, pageLocation+ " Main");
		}
		
		
		// added by Lokesh 8-2015  // req 13
	//	@Test(dependsOnMethods = { "validateSELogo" }, description = "Ensure that the current page contains required web elements")
		@Test(dependsOnMethods = { "validateStickyBars" }, description = "Ensure that the current page contains required web elements")
		private boolean validateTiles(String reqNo, String pageLocation) {
			Reporter.log(logTime() + "  " + "Req " +reqNo + " START. " + "Validation " + pageLocation + " "+ " Main", true);
			//WebMachine_HomePage.getStickyBars();
			boolean result = WebMachine_PartnersPage.validateTiles("Req 3", "Work", "Solution Items", zoneUrl(Constants.WebMachine_DispatchPage.zoneCLink,"Dispatch"));		
			String status = result == true ? "PASSED" : "FAILED";
			Reporter.log(logTime() + "  " + "Req " +reqNo + " END. " + "Validation " + pageLocation + " "+ " Main - " + status, true);
			return evaluateResult(result, pageLocation+ " Main");
		}
		
		

		
@DataProvider
public Object[][] WebMachinePartnersPage_TestData(){
	//return TestUtil.getdata("WebMachine_DispatchPageTest", WebMachine_Base_Page.xls);
	//return DataUtil.getTestData("DispatchPageTest", WebMachine_Base_Page.xls);`
	Object[][] obj = DataUtil.getTestData("DispatchPageTest", WebMachine_Base_Page.xls);
	return obj;
}
}

