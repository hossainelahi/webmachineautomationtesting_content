package com.SE.WebMachine.testcases.FunctionalTests;

import java.util.Hashtable;
import java.util.Map;

import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.exceptions.Schneider_TimeoutException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.pages.WebMachine_DispatchPage;
//import com.SE.WebMachine.tools.Driver;
//import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.DataUtil;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_CustomerCareForm_Test extends WebMachine_BasePage_Test{

	@Parameters({"aut_environment"})
	@BeforeSuite
	public void checkSuiteRunStatus(@Optional String environment){
		boolean fromTestNG = environment == null || environment.length() == 0 ? false : true;
		super.setUp(environment);
		System.out.println(testData.get("baseURL"));
		logTestEnvironment("A", fromTestNG);
		logTestDomain();
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
	}
	
	
	@Test(dataProvider = "WebMachineCustomerCareForm_TestData")
	public void WebMachine_DispatchPageTest(Hashtable<String, String> configData, 
			Hashtable<Integer, String> testSteps, Map<String, Schneider_PageElement> pageElements){
		
		if(configData.get("Runmode").equalsIgnoreCase("N")){
			System.out.println("Skipping as Flag is N for this data set for WebMachine_DispatchPageTest");
			throw new SkipException("Skipping as Flag is N for this data set");
		}
		Reporter.log("\n", true);	
		Reporter.log("\n", true);	
		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		Reporter.log(logTime() + " " + ":::::START: CUSTOMER CARE FORM TEST FOR WEBMACHINE:::::::::::::::" , true);
		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);

		try{
			//Schneider_PageElement p = pageElements.get("acceptCookies");
		WebMachine_DispatchPage.initialize(configData);
		//Driver.getBaseUrl("http://www-sqe1.schneider-electric.com/us/en/");
		WebMachine_DispatchPage.launch(testData.get("baseURL"));
		System.out.println(testData.get("baseURL"));
		WebMachine_DispatchPage.skipVideoLink(pageElements.get("skipVideo"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
		WebMachine_DispatchPage.clickCookiesLink(pageElements.get("acceptCookies"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
		WebMachine_DispatchPage.awaitPresenceOfElement(pageElements.get("schneiderLogo"));

		boolean overallResult = true;
		testngLevel = configData.get("TestingLevel");
		summariseTestingLevel(this.getClass().getSimpleName(), testngLevel);
		Reporter.log("\n", true);	
		
		if(testData.get("baseURL").contains("/us/en") && ((testData.get("baseURL").contains("sqe") || testData.get("baseURL").contains("SQE"))))
		{
			overallResult = getOverallResult(overallResult, validateCustomerCareForm("1", "Customer Care Form"));
		}else{
			Reporter.log(logTime() + " " + "......TEST ABORTED: TEST SITE URL MUST BE SQE", true);	
		}
		
		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		Reporter.log(logTime() + " " + ":::END: CUSTOMER CARE FORM TEST FOR WEBMACHINE:::::::::::::::::" , true);
		summariseTest(this.getClass().getSimpleName(), overallResult);
			
		}
		catch (Schneider_TimeoutException sTE){
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + sTE.getMessage() + ". Site is no longer available to test.", true);	
		} 
		catch (Schneider_IndexOutOfBoundsException iOOBE) {
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + iOOBE.getMessage() + ".", true);		}
		catch (Throwable t) {
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED. Please investiage the Error or Exception in the following Stack trace ...", true);
			t.printStackTrace();
		}		
	}
	
	// req 1
	//@Test(description = "Ensure that the current page contains required web elements")
	public boolean validateCustomerCareForm(String reqNo, String pageLocation) {
		Reporter.log(logTime() + "  " + "Req " +reqNo + " START. " + "Validation " + pageLocation + " "+ "Customer Care Form", true);
		boolean result = WebMachine_DispatchPage.getCustomerCareForm().validateCustomerCareForm("Req " +reqNo, "Customer Care Form", "xPath", testData.get("baseURL"), Constants.WebMachine_DispatchPage.header_Top_Menu);
		String status = result == true ? "PASSED" : "FAILED";
		Reporter.log(logTime() + "  " + "Req " +reqNo + " END. " + "Validation " + pageLocation + " "+ "SE Logo - " + status, true);
		return evaluateResult(result, "Header SE Logo");
	}
	
	
	@DataProvider
	public Object[][] WebMachineCustomerCareForm_TestData(){

		Object[][] obj =  

				DataUtil.getTestData("DispatchPageTest", WebMachine_Base_Page.xls);

		
		return obj;
	}
}
