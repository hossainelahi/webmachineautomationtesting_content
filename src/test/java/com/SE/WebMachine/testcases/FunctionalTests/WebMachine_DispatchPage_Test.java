package com.SE.WebMachine.testcases.FunctionalTests;

/* DH Java SE Libraries*/
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;


/* DH Java third party Libraries*/
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


/* DH Java Schneider Libraries*/
import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.exceptions.Schneider_TimeoutException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.pages.WebMachine_DispatchPage;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.DataUtil;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_DispatchPage_Test extends WebMachine_BasePage_Test{
	
	@Parameters({"aut_environment"})
	@BeforeSuite
	public void checkSuiteRunStatus(@Optional String environment){
		boolean fromTestNG = environment == null || environment.length() == 0 ? false : true;
		super.setUp(environment);
		logTestEnvironment("A", fromTestNG);
		logTestDomain();
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
	}
	@Test(dataProvider = "WebMachineDispatchPage_TestData")
	public void WebMachine_DispatchPageTest(Hashtable<String, String> configData, 
			Hashtable<Integer, String> testSteps, Map<String, Schneider_PageElement> pageElements){
		
		if(configData.get("Runmode").equalsIgnoreCase("N")){
			System.out.println("Skipping as Flag is N for this data set for WebMachine_DispatchPageTest");
			throw new SkipException("Skipping as Flag is N for this data set");
		}
		Reporter.log("\n", true);	
		Reporter.log("\n", true);	
		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		Reporter.log(logTime() + " " + ":::::START: DISPATCH PAGE TEST FOR WEBMACHINE:::::::::::::::" , true);
		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);

		try{
			//Schneider_PageElement p = pageElements.get("acceptCookies");
		WebMachine_DispatchPage.initialize(configData);
		WebMachine_DispatchPage.launch(testData.get("baseURL"));
		
		WebMachine_DispatchPage.skipVideoLink(pageElements.get("skipVideo"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
		WebMachine_DispatchPage.clickCookiesLink(pageElements.get("acceptCookies"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
		WebMachine_DispatchPage.stayOnGlobalWebsite(pageElements.get("stayOnGlobalWebsite"), testData.get("baseURL"), "Req 0", "Dispatch Page", "");
		WebMachine_DispatchPage.awaitPresenceOfElement(pageElements.get("schneiderLogo"));

		boolean overallResult = true;
		testngLevel = configData.get("TestingLevel");
		summariseTestingLevel(this.getClass().getSimpleName(), testngLevel);
		Reporter.log("\n", true);	
		
		overallResult = getOverallResult(overallResult, carouselDispatch("1", "Dispatch page")); 	// done Lokesh	

		Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		Reporter.log(logTime() + " " + ":::END: DISPATCH PAGE TEST FOR WEBMACHINE:::::::::::::::::" , true);
		summariseTest(this.getClass().getSimpleName(), overallResult);
			
		}
		catch (Schneider_TimeoutException sTE){
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + sTE.getMessage() + ". Site is no longer available to test.", true);	
		} 
		catch (Schneider_IndexOutOfBoundsException iOOBE) {
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED: " + iOOBE.getMessage() + ".", true);		}
		catch (Throwable t) {
			Reporter.log(logTime() + " " + ":::::::TEST ABORTED. Please investiage the Error or Exception in the following Stack trace ...", true);
			t.printStackTrace();
		}		
	}
	

		// added by Lokesh 8-2015  // req 1  validating dispatch page zones
		
		//@Test(dependsOnMethods = { "validateLifeIsOnLogo" }, description = "Ensure that the current page contains required web elements")
			@Test(description = "Ensure that the current page contains required web elements")
			private boolean carouselDispatch(String reqNo, String pageLocation) {
				Reporter.log(logTime() + "  " + "Req " +reqNo + " START. " + "Validation " + pageLocation + " "+ " Main", true);
				boolean result = WebMachine_DispatchPage.carouselDispatch("Req 1", "Dispatch", "Carousel", testData.get("baseURL"));		
				String status = result == true ? "PASSED" : "FAILED";
				Reporter.log(logTime() + "  " + "Req " +reqNo + " END. " + "Validation " + pageLocation + " "+ " Main - " + status, true);
				return evaluateResult(result, pageLocation+ " Main");
			}
	
	@DataProvider
	public Object[][] WebMachineDispatchPage_TestData(){

		Object[][] obj = DataUtil.getTestData("DispatchPageTest", WebMachine_Base_Page.xls);
		return obj;
	}
}
