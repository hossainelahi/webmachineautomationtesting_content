package com.SE.WebMachine.testcases.FunctionalTests;

/* DH Java SE Libraries*/
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


/* DH Java third party Libraries*/
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
//import org.testng.annotations.AfterSuite;
import org.testng.asserts.SoftAssert;


/* DH Java Schneider Libraries*/
import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.Xls_Reader;


public class WebMachine_BasePage_Test {
	@SuppressWarnings("unused")
	private static final TimeUnit SECONDS = null;
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	{
		System.setProperty("atu.reporter.config", Constants.Paths.ATU_ReporterConfig_FILE_PATH);
	}
	public static Xls_Reader xls = new Xls_Reader(Constants.Paths.XlsReader_FILE_PATH);	
	protected static Hashtable<String, String> testData = new Hashtable<String, String>();
	protected Properties Config = null;
	protected Properties ENV = null;
	private ArrayList<String> failures = new ArrayList<String>();
	String testngLevel = new String();
	String currentUrl = new String();

	@SuppressWarnings("unused")
	protected WebMachine_BasePage_Test() { // Constructor is made protected
		Config = new Properties();
		ENV = new Properties();
		try {
			FileInputStream fs = new FileInputStream(Constants.Paths.Config_FILE_PATH);
			if (fs == null) {
			} else
				Config.load(fs);
			String filename = Config.getProperty("environment") + ".properties";
			fs = new FileInputStream(Constants.Paths.Config_FOLDER_PATH
					+ filename);
			ENV.load(fs);
		} catch (Exception e) {
		}
	}
	public String startOfTest(String purpose, String... testParameters) {
		String start = logTime() + " " + testParameters[0]; 
		String summary = " \"" + testParameters[1] + "\" \"" 
				+ testParameters[2] + "\" " 
				+ purpose + "";
		Reporter.log(start + " START " + summary , true);
		return summary;
	}
	public void endOfTest(String summary, boolean testOutcome,
			String... testParameters) {
		String status = testOutcome == true ? "PASSED" : "FAILED";
		Reporter.log(logTime() + " " + testParameters[0] + " END. " + status + ": " + 
		summary.substring(summary.indexOf(testParameters[1])), true);
	}
	public void languageConverter(String strPrintMessage) throws UnsupportedEncodingException {
		PrintStream sysout = new PrintStream(System.out, true, "UTF-8");
		sysout.println(strPrintMessage);
	}
	public void takeABreak(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
			Reporter.log(logTime()+ "  ....Putting thread to sleep for "
					+ seconds + " seconds", false);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	public void log(String mesg) {
		APPLICATION_LOGS.debug(mesg);
	}
	protected static String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	protected void setUp(String environment) {
		environment = environment == null || environment.length() == 0 ? ENV.getProperty("baseURL") : environment;
		testData.put("baseURL", environment);
		String homePageURL = ENV.getProperty("homePageURL") == null || ENV.getProperty("homePageURL").length() == 0 ? "" : ENV.getProperty("homePageURL");
		testData.put("homePageURL", homePageURL);
		testData.put("testDomain", getBaseDomain(testData));
		String testId = this.getClass().getName();
		testId = testId.substring(testId.indexOf("_")+1);
		testData.put("testId", testId);
	}
	protected String getBaseDomain(Hashtable<String, String> testParameters) {
		String baseDomain = testParameters.get("baseURL");
		if (baseDomain.contains("int")) {
			baseDomain = "int";
		}
		if (baseDomain.contains("sqe")) {
			baseDomain = "sqe";
		}
		if (baseDomain.contains("pre")) {
			baseDomain = "pre";
		}
		return baseDomain;
	}
	protected void logTestDomain(){
		Reporter.log(logTime() + "  Test Domain is " + testData.get("testDomain").trim(), true);
	}
	protected void logTestEnvironment(String group, boolean fromTestNG){
		String source = fromTestNG == true ? "Jenkins/Maven/TestNG/CommandLine" : "FileSystem Properties file, because no argument was passed from Jenkins/Maven/TestNG/CommandLine.";
		Reporter.log(logTime() + "  ....Test Environment is \"" + testData.get("baseURL").trim() + "\". Read in from " + source, true);
	}
	protected void summariseTestingLevel(String scriptName, String testngLevel) {
		
		Reporter.log(logTime() + "  ..These tests reflect the WebMachine_Test Coverage.docx, located on Box in ", true);
		Reporter.log(logTime() + "  ..https://schneider-electric.app.box.com/files/0/f/2651870361/WebMachine_Aut_Framework", true);
		Reporter.log(logTime() + "  ...."+scriptName + " TestNG assertion level is \""+testngLevel + "\".", true);
		Reporter.log(logTime() + "  ....(Test Data worksheet in " + Constants.Paths.XlsReader_FILE_PATH + ")", true);
		
		if (testngLevel.equalsIgnoreCase("Script")){
			Reporter.log(logTime() + "  ....This means that all TESTS in the script will, at the very least, be attempted.", true);
			Reporter.log(logTime() + "  ....Some may fail or succeed, check the console output. The overall TestNG result output is based on the cumulative results of ALL tests in the script.", true);
		}
		else if (testngLevel.equalsIgnoreCase("Method")){
			Reporter.log(logTime() + "  ....This means that the first TEST to fail will terminate the running of the overall script.", true);
			Reporter.log(logTime() + "  ....The overall TestNG result output is based on the individual tests.", true);
		}
	}
	protected void summariseTest(String testName, boolean overallResult) {
		if (failures.size() > 0 && overallResult == false){
			String allFailures = new String();
			for (String failure : failures){
				allFailures = allFailures.concat(failure + "; ");
			}
			Reporter.log(logTime() + " " + testName + " FAILED TESTS are ..............", true);
			if (allFailures.trim().contains(";;")){
				allFailures = allFailures.trim().replace(";;", ";");
			}
			Reporter.log(logTime() + " " + allFailures.trim(), true);
			Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		}
		else{
			Reporter.log(logTime() + ":::::::::::::::::::::::::::::::::::::::::::::::::SCRIPT RESULT = PASSED !!:::::::::::::::::", true);	
			Reporter.log(logTime() + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
		}
		assertScript(testName, overallResult);
	}
	protected boolean evaluateResult(boolean result, String testPurpose) {
		if (testngLevel.equalsIgnoreCase("Script")){
			try{
				Assert.assertTrue(result);
				result = true;
			}catch (AssertionError ae){
				failures.add(testPurpose);
				result = false;
			}
			Reporter.log("\n", true);	
		}
		else if (testngLevel.equalsIgnoreCase("Method")){
			Reporter.log("\n", true);
			Assert.assertTrue(result, Constants.WebMachine_CommonConstants.MethodFailureMessage + " " + this.getClass().getSimpleName());
		}
		return result;
	}
	protected boolean getOverallResult(boolean overallResult, boolean result) {
		if (overallResult){
			overallResult = result == false ? result : true;
		}
		return overallResult;
	}
	private void assertScript(String testName, boolean overallResult) {
		SoftAssert softAssert = new SoftAssert();
		//softAssert.assertTrue(overallResult, testName + " failed.");
//		System.out.println("test name >> "+testName+" overallResult >>"+overallResult);
		softAssert.assertEquals(overallResult, true, testName+" "+overallResult);
		//softAssert.assertTrue(overallResult);
		//softAssert.assertTrue(false);
//		System.out.println("before assertall");
		softAssert.assertAll();
	}
	//@AfterSuite(alwaysRun = true)
	@AfterClass(alwaysRun = true)
	public void tearDownSuite() {
		try{
			tearDown();
			Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log(logTime() + " " + ":::::::::::::::::::::CLOSED BROWSER:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log("\n", true);
		}
		catch(Exception ex){

		}
	}
	private void tearDown() {
		Driver.tearDown();
	}
}
