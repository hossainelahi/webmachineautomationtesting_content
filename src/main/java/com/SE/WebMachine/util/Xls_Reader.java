package com.SE.WebMachine.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.pageElements.Schneider_PageElementFactory;



public class Xls_Reader extends WebMachine_Reader {
	public static String filename = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\testdata\\WebMachine_TestData.xlsx";
	public  String path;
	public  FileInputStream fis = null;
	public  FileOutputStream fileOut =null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row   =null;
	private XSSFCell cell = null;
	
	public Xls_Reader(String path) {
		
		this.path=path;
		try {
			fis = new FileInputStream(path);
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(0);
			//System.out.println("sheet name "+sheet.getSheetName());
			fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	// returns the row count in a sheet
	public int getRowCount(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return 0;
		else{
		sheet = workbook.getSheetAt(index);
		int number=sheet.getLastRowNum()+1;
		return number;
		}
		
	}
	
	// returns the data from a cell
	public String getCellData(String sheetName,String colName,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);
		int col_Num=-1;
		if(index==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row=sheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
				col_Num=i;
		}
		if(col_Num==-1)
			return "";
		
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null)
			return "";
		cell = row.getCell(col_Num);
		
		if(cell==null)
			return "";
		//System.out.println(cell.getCellType());
		if(cell.getCellType()==Cell.CELL_TYPE_STRING)
			  return cell.getStringCellValue();
		else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC || cell.getCellType()==Cell.CELL_TYPE_FORMULA ){
			  
			  String cellText  = String.valueOf(cell.getNumericCellValue());
			  if (HSSFDateUtil.isCellDateFormatted(cell)) {
		           // format in form of M/D/YY
				  double d = cell.getNumericCellValue();

				  Calendar cal =Calendar.getInstance();
				  cal.setTime(HSSFDateUtil.getJavaDate(d));
		            cellText =
		             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
		           cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
		                      cal.get(Calendar.MONTH)+1 + "/" + 
		                      cellText;
		           
		           //System.out.println(cellText);

		         }

			  
			  
			  return cellText;
		  }else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
		      return ""; 
		  else 
			  return String.valueOf(cell.getBooleanCellValue());
		
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colName +" does not exist in xls";
		}
	}
	
	// returns the data from a cell
	public String getCellData(String sheetName,int colNum,int rowNum){
		try{
			if(rowNum <=0)
				return "";
		
		int index = workbook.getSheetIndex(sheetName);

		if(index==-1)
			return "";
		
	
		sheet = workbook.getSheetAt(index);
		row = sheet.getRow(rowNum-1);
		if(row==null)
			return "";
		cell = row.getCell(colNum);
		if(cell==null)
			return "";
		
	  if(cell.getCellType()==Cell.CELL_TYPE_STRING)
		  return cell.getStringCellValue();
	  else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC || cell.getCellType()==Cell.CELL_TYPE_FORMULA ){
		  
		  String cellText  = String.valueOf(cell.getNumericCellValue());
		  if (HSSFDateUtil.isCellDateFormatted(cell)) {
	           // format in form of M/D/YY
			  double d = cell.getNumericCellValue();

			  Calendar cal =Calendar.getInstance();
			  cal.setTime(HSSFDateUtil.getJavaDate(d));
	            cellText =
	             (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
	           cellText = cal.get(Calendar.MONTH)+1 + "/" +
	                      cal.get(Calendar.DAY_OF_MONTH) + "/" +
	                      cellText;
	           
	          // System.out.println(cellText);

	         }

		  
		  
		  return cellText;
	  }else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
	      return "";
	  else 
		  return String.valueOf(cell.getBooleanCellValue());
		}
		catch(Exception e){
			
			e.printStackTrace();
			return "row "+rowNum+" or column "+colNum +" does not exist  in xls";
		}
	}
	
	// returns true if data is set successfully else false
	public boolean setCellData(String sheetName,String colName,int rowNum, String data){
		try{
		fis = new FileInputStream(path); 
		workbook = new XSSFWorkbook(fis);

		if(rowNum<=0)
			return false;
		
		int index = workbook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		sheet = workbook.getSheetAt(index);
		

		row=sheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(row.getCell(i).getStringCellValue().trim().equals(colName))
				colNum=i;
		}
		if(colNum==-1)
			return false;

		sheet.autoSizeColumn(colNum); 
		row = sheet.getRow(rowNum-1);
		if (row == null)
			row = sheet.createRow(rowNum-1);
		
		cell = row.getCell(colNum);	
		if (cell == null)
	        cell = row.createCell(colNum);

	    // cell style
	    //CellStyle cs = workbook.createCellStyle();
	    //cs.setWrapText(true);
	    //cell.setCellStyle(cs);
	    cell.setCellValue(data);

	    fileOut = new FileOutputStream(path);

		workbook.write(fileOut);

	    fileOut.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	// returns true if data is set successfully else false
	public boolean setCellData(String sheetName,String colName,int rowNum, String data,String url){
		//System.out.println("setCellData setCellData******************");
		try{
		fis = new FileInputStream(path); 
		workbook = new XSSFWorkbook(fis);

		if(rowNum<=0)
			return false;
		
		int index = workbook.getSheetIndex(sheetName);
		int colNum=-1;
		if(index==-1)
			return false;
		
		
		sheet = workbook.getSheetAt(index);
		//System.out.println("A");
		row=sheet.getRow(0);
		for(int i=0;i<row.getLastCellNum();i++){
			//System.out.println(row.getCell(i).getStringCellValue().trim());
			if(row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(colName))
				colNum=i;
		}
		
		if(colNum==-1)
			return false;
		sheet.autoSizeColumn(colNum); //ashish
		row = sheet.getRow(rowNum-1);
		if (row == null)
			row = sheet.createRow(rowNum-1);
		
		cell = row.getCell(colNum);	
		if (cell == null)
	        cell = row.createCell(colNum);
			
	    cell.setCellValue(data);
	    XSSFCreationHelper createHelper = workbook.getCreationHelper();

	    //cell style for hyperlinks
	    //by default hypelrinks are blue and underlined
	    CellStyle hlink_style = workbook.createCellStyle();
	    XSSFFont hlink_font = workbook.createFont();
	    hlink_font.setUnderline(XSSFFont.U_SINGLE);
	    hlink_font.setColor(IndexedColors.BLUE.getIndex());
	    hlink_style.setFont(hlink_font);
	    //hlink_style.setWrapText(true);

	    XSSFHyperlink link = createHelper.createHyperlink(XSSFHyperlink.LINK_FILE);
	    link.setAddress(url);
	    cell.setHyperlink(link);
	    cell.setCellStyle(hlink_style);
	      
	    fileOut = new FileOutputStream(path);
		workbook.write(fileOut);

	    fileOut.close();	

		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	
	// returns true if sheet is created successfully else false
	public boolean addSheet(String  sheetname){		
		
		FileOutputStream fileOut;
		try {
			 workbook.createSheet(sheetname);	
			 fileOut = new FileOutputStream(path);
			 workbook.write(fileOut);
		     fileOut.close();		    
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	// returns true if sheet is removed successfully else false if sheet does not exist
	public boolean removeSheet(String sheetName){		
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1)
			return false;
		
		FileOutputStream fileOut;
		try {
			workbook.removeSheetAt(index);
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    fileOut.close();		    
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}
		return true;
	}
	// returns true if column is created successfully
	public boolean addColumn(String sheetName,String colName){
		//System.out.println("**************addColumn*********************");
		
		try{				
			fis = new FileInputStream(path); 
			workbook = new XSSFWorkbook(fis);
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return false;
			
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		
		sheet=workbook.getSheetAt(index);
		
		row = sheet.getRow(0);
		if (row == null)
			row = sheet.createRow(0);
		
		//cell = row.getCell();	
		//if (cell == null)
		//System.out.println(row.getLastCellNum());
		if(row.getLastCellNum() == -1)
			cell = row.createCell(0);
		else
			cell = row.createCell(row.getLastCellNum());
	        
	        cell.setCellValue(colName);
	        cell.setCellStyle(style);
	        
	        fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
		    fileOut.close();		    

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
		
		
	}
	// removes a column and all the contents
	public boolean removeColumn(String sheetName, int colNum) {
		try{
		if(!isSheetExist(sheetName))
			return false;
		fis = new FileInputStream(path); 
		workbook = new XSSFWorkbook(fis);
		sheet=workbook.getSheet(sheetName);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		XSSFCreationHelper createHelper = workbook.getCreationHelper();
		style.setFillPattern(HSSFCellStyle.NO_FILL);
		
	    
	
		for(int i =0;i<getRowCount(sheetName);i++){
			row=sheet.getRow(i);	
			if(row!=null){
				cell=row.getCell(colNum);
				if(cell!=null){
					cell.setCellStyle(style);
					row.removeCell(cell);
				}
			}
		}
		fileOut = new FileOutputStream(path);
		workbook.write(fileOut);
	    fileOut.close();
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
  // find whether sheets exists	
	public boolean isSheetExist(String sheetName){
		int index = workbook.getSheetIndex(sheetName);
		if(index==-1){
			index=workbook.getSheetIndex(sheetName.toUpperCase());
				if(index==-1)
					return false;
				else
					return true;
		}
		else
			return true;
	}
	
	// returns number of columns in a sheet	
	public int getColumnCount(String sheetName){
		// check if sheet exists
		if(!isSheetExist(sheetName))
		 return -1;
		
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);
		
		if(row==null)
			return -1;
		
		return row.getLastCellNum();
		
		
		
	}
	//String sheetName, String testCaseName,String keyword ,String URL,String message
	public boolean addHyperLink(String sheetName,String screenShotColName,String testCaseName,int index,String url,String message){
		//System.out.println("ADDING addHyperLink******************");
		
		url=url.replace('\\', '/');
		if(!isSheetExist(sheetName))
			 return false;
		
	    sheet = workbook.getSheet(sheetName);
	    
	    for(int i=2;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName, 0, i).equalsIgnoreCase(testCaseName)){
	    		//System.out.println("**caught "+(i+index));
	    		setCellData(sheetName, screenShotColName, i+index, message,url);
	    		break;
	    	}
	    }


		return true; 
	}
	public int getCellRowNum(String sheetName,String colName,String cellValue){
		
		for(int i=2;i<=getRowCount(sheetName);i++){
	    	if(getCellData(sheetName,colName , i).equalsIgnoreCase(cellValue)){
	    		return i;
	    	}
	    }
		return -1;
		
	}
	public int getCellColumnIndex(XSSFSheet sheet, Integer rowNum, String cellContent) throws Exception{
		//XSSFSheet sheet = wb.getSheetAt(0);
		//sheet = workbook.getSheet("BO");  //DH
		//sheet = workbook.getSheetAt(0);
		XSSFRow row = sheet.getRow(rowNum);
		String name = sheet.getSheetName();

		int cellColumnIndex = -1;
		for (int cellNumber = 0; cellNumber < row.getLastCellNum(); cellNumber++) {
			Cell cell = row.getCell(cellNumber);
			if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK && 
					cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				continue;
			}
			if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
				String text = cell.getStringCellValue();
				if (text.contains(cellContent)) {
					cellColumnIndex = cellNumber;
					break;
				}
				else{
					continue;
				}
			}
		}
		
		if (cellColumnIndex == -1) {
			throw new Exception("None of the cells in column number " + 
					cellColumnIndex + " equalled " + cellContent);
		}
		return cellColumnIndex;

	}		
	public XSSFWorkbook getWorkbook() {
		return workbook;
	}
	public void setWorkbook(XSSFWorkbook workbook) {
		this.workbook = workbook;
	}
	public Cell getNextCell(CellReference cellReference, XSSFSheet sheet) {
		Cell cell = null;
		Row row = sheet.getRow(cellReference.getRow()+1);
		if (row != null) {
			cell = row.getCell(cellReference.getCol());

		}
		return cell;
	}
	public Hashtable<Integer, Row> getCellIndexAndRow(XSSFSheet sheet, String cellContent, 
			CellReference cellReference){
		/*
		 *  This is the method to find the row number
		 */

		Hashtable<Integer, Row> cellIndexAndRow = new Hashtable<Integer, Row>();

		Integer rowNum = 0; 

		rowCheck:
			for(Row row : sheet) {
				cellCheck:
					for(Cell cell : row) {
						if (cell.getCellType() != Cell.CELL_TYPE_STRING){
								//cell.getRowIndex() != cellReference.getRow()){
							continue rowCheck;
						}
						if(cell.getRichStringCellValue().getString().contains(cellContent)){
							rowNum = row.getRowNum();
							cellIndexAndRow.put(0, row);
							break rowCheck;
						}
						else if(cellReference != null && cellReference.getCol() > -1){
							if (cellContent.contains("CONFIGURATION DATA - End")){
								if(cell.getRichStringCellValue().getString().contains(cellContent)){
									if (cell.getColumnIndex() == cellReference.getCol()){
										rowNum = row.getRowNum();
										cellIndexAndRow.put(0, row);
										break rowCheck;
									}
								}
								else{
									continue rowCheck;
								}
							}
							if (cellContent.contains("TEST STEPS - End")){
								if(cell.getRichStringCellValue().getString().contains(cellContent)){
									if (cell.getColumnIndex() == cellReference.getCol()){
										rowNum = row.getRowNum();
										cellIndexAndRow.put(0, row);
										break rowCheck;
									}
								}
								else{
									continue rowCheck;
								}
							}	
							if (cellContent.contains("SCRIPT DATA STORAGE - End")){
								if(cell.getRichStringCellValue().getString().contains(cellContent)){
									if (cell.getColumnIndex() == cellReference.getCol()){
										rowNum = row.getRowNum();
										cellIndexAndRow.put(0, row);
										break rowCheck;
									}
								}
								else{
									continue rowCheck;
								}
							}							
							if (cellContent.contains("Test Steps Start")){
								if(cell.getRichStringCellValue().getString().contains(cellContent)){
									if (cell.getColumnIndex() == cellReference.getCol()){
										rowNum = row.getRowNum();
										cellIndexAndRow.put(0, row);
										break rowCheck;
									}
								}
								else{
									continue rowCheck;
								}
							}            	
							if (cellContent.contains("Script Data Storage Start")){
								//&& cellReference.getRow() == cell.getRowIndex()){
							if(cell.getRichStringCellValue().getString().contains(cellContent)){
								if (cell.getColumnIndex() == cellReference.getCol()){
									rowNum = row.getRowNum()+1;
									cellIndexAndRow.put(0, row);
									break rowCheck;
								}
							}
							else{
								continue rowCheck;
							}
						}            	
						else{
							continue cellCheck;
						}							
						}
					}   
			}
		return cellIndexAndRow;
	}
	public Map<String, Schneider_PageElement> getPageElements(XSSFSheet sheet,
			CellReference dataStartCellReference,
			CellReference dataEndCellReference) {
		//Hashtable<String, PageElement> scriptData = new Hashtable<String, PageElement>();
		
		//Set<PageElement> pageElements = new TreeSet<PageElement>(new PageElementByPageLocationAndIndex());
		Map<String, Schneider_PageElement> pageElements = new HashMap<String, Schneider_PageElement>();
		
		int startingRow = dataStartCellReference.getRow();
		int endingRow = dataEndCellReference.getRow();
		Integer index = new Integer(dataStartCellReference.getCol());
		String dataKey = new String();
		String dataValue = new String();
		int attributeLocation = 0;
		int cellIndex = 0;
		Schneider_PageElement pageElement = null;
		rowCheck:
			for(Row row : sheet) {
				cellCheck:
					for(Cell cell : row) {

						if (row.getRowNum() == startingRow){
							if (cell.getCellType() == Cell.CELL_TYPE_STRING && 
									cell.getStringCellValue().equalsIgnoreCase("terminus")){
								/*check the next row. This is a script data check, 
								 * some attributes may have values, others may not
								 * which may be strings, numeric or a mixture of both
								 * Instead we need a termination cell to know where the limit 
								 * of the script data is
								 */
								break cellCheck;
							}
							if (cell.getColumnIndex() == index){
								dataKey = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == cellIndex+1){
								dataValue = getDataValue(dataValue, cell);								
								pageElement = setupPageElement(dataKey,
										dataValue, pageElement, cell);
								cellIndex++;
								if ((dataKey.length() < 1 || dataKey == null)) 
									break cellCheck;
							}
						}
						else{
							continue rowCheck;
						}

					}  
		cellIndex = 0;
		if (!dataKey.equalsIgnoreCase("pageElementKey")){
			pageElements.put(dataKey, pageElement);
			//pageElements.add(pageElement);
			
		}

		startingRow = startingRow+1;
		if (row.getRowNum() == endingRow - 1){
			break rowCheck;
		}
		//index++;
			}
		return pageElements;
	}
	private Schneider_PageElement setupPageElement(String dataKey, String dataValue,
			Schneider_PageElement pageElement, Cell cell) {
		if (dataKey.equalsIgnoreCase("pageElementKey")){
			if (dataValue.equalsIgnoreCase("type") && cell.getColumnIndex() != 0){
				pageElement = Schneider_PageElementFactory.getPageElement(dataValue);
			}	
		}
		else{
				if (!(pageElement.getKey().equalsIgnoreCase(dataKey))){
					pageElement = Schneider_PageElementFactory.getPageElement(dataValue);
					pageElement.setKey(dataKey);
				}
		}
		pageElement.setAttributeLocation(cell.getColumnIndex());	
		//In the process of setting up dummy PageElement object for reference purposes
		if (pageElement != null){		
			switch (pageElement.getAttributeLocation()){
				case 1: pageElement.setKey(dataKey); 
						pageElement.setType(dataValue);
						break;
				case 2: pageElement.setScriptValue(dataValue); break;	
				case 3: 
					pageElement.setPageLocation(dataValue); break;
				case 4: if (pageElement.getKey().equalsIgnoreCase("pageElementKey")){
							pageElement.setPageLocationIndex(pageElement.getAttributeLocation()); break;
						}
						else{
							dataValue = dataValue.substring(0, dataValue.indexOf("."));
							pageElement.setPageLocationIndex(Integer.valueOf(dataValue)); break;
						}
				case 5: pageElement.setLocator(dataValue); break;
				case 6: pageElement.setLink(dataValue); break;
				case 7: pageElement.setLinkedPageText(dataValue); break;
				case 8: pageElement.setLinkedWebElement(dataValue); break;
				case 9: pageElement.setName(dataValue); break;
				case 10: pageElement.setParentPage(dataValue); break;
			}
		}

		
		return pageElement;
	}
	private String getDataValue(String dataValue, Cell cell) {
		if (cell.getCellType() == Cell.CELL_TYPE_STRING || 
				cell.getCellType() == Cell.CELL_TYPE_BLANK){
			dataValue = cell.getStringCellValue();
		}
		else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
			Double numericValue = cell.getNumericCellValue();
			dataValue = Double.toString(numericValue);
		}
		else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){
			Boolean booleanValue = cell.getBooleanCellValue();
			dataValue = Boolean.toString(booleanValue);
		}
		return dataValue;
	}
	public Hashtable<Integer, String> getTestStepsData(
			XSSFSheet sheet, CellReference startCellReference,
			CellReference endCellReference) {

		Hashtable<Integer, String> testData = new Hashtable<Integer, String>();
		int startingRow = startCellReference.getRow()+1;
		int endingRow = endCellReference.getRow();

		Integer index = new Integer(startCellReference.getCol());
		Integer testStepKey = new Integer(0);
		String testStepValue = new String();
		rowCheck:
			for(Row row : sheet) {
				if (row.getRowNum() == startingRow){
				cellCheck:
					for(Cell cell : row) {
						if (cell.getCellType() != Cell.CELL_TYPE_STRING && cell.getCellType() != Cell.CELL_TYPE_NUMERIC){
							/*check the next row. This is a test steps check, 
							 * we're expecting the data to be numeric AND then a string
							 */
							continue rowCheck;
						}
//						if (row.getRowNum() == startingRow){
							if (cell.getColumnIndex() == index){
								testStepKey = (int) cell.getNumericCellValue();
							}
							if (cell.getColumnIndex() == index+1){
								testStepValue = cell.getStringCellValue();
								if (testStepKey > 0) break cellCheck;
							}
							//continue cellCheck;
//						}
//						else{
//							continue rowCheck;
//						}
					}   
				testData.put(testStepKey, testStepValue);
				startingRow = startingRow+1;
				if (row.getRowNum() == endingRow - 1){
					break rowCheck;
				}
				}
			}
		return testData;
	}
	public Hashtable<String, String> getTestConfigData(XSSFSheet sheet,
			CellReference startCellReference,
			CellReference endCellReference) {
		
		Hashtable<String, String> testData = new Hashtable<String, String>();
		int startingRow = startCellReference.getRow();
		int endingRow = endCellReference.getRow();

		Integer index = new Integer(startCellReference.getCol());
		String testStepKey = new String();
		String testStepValue = new String();
		rowCheck:
			for(Row row : sheet) {
				cellCheck:
					for(Cell cell : row) {
						if (cell.getCellType() != Cell.CELL_TYPE_STRING && cell.getCellType() != Cell.CELL_TYPE_NUMERIC){
							/*check the next row. This is a test steps check, 
							 * we're expecting the data to be numeric AND then a string
							 */
							continue rowCheck;
						}
						if (row.getRowNum() == startingRow){
							if (cell.getColumnIndex() == index){
								testStepKey = cell.getStringCellValue();
							}
							if (cell.getColumnIndex() == index+1){
								if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
									String cellValue = Double.toString(cell.getNumericCellValue());
									if (cellValue.contains(".")){
										testStepValue = cellValue.substring(0, cellValue.indexOf("."));
									}
									else{
										testStepValue = cellValue;
									}
								}
								else{
									testStepValue = cell.getStringCellValue();
								}
								
								if (testStepKey.length() > 0) break cellCheck;
							}
						}
						else{
							continue rowCheck;
						}
					}   
				testData.put(testStepKey, testStepValue);
				startingRow = startingRow+1;
				if (row.getRowNum() == endingRow){
					break rowCheck;
				}
			}
		return testData;
	}

}
