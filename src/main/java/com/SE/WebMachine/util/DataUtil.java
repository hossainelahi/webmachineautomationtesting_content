package com.SE.WebMachine.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.SE.WebMachine.pageElements.Schneider_PageElement;


public class DataUtil {
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	static Hashtable<String, String> configurationData;	
	static Map<String, Schneider_PageElement> pageElements;
	static Hashtable<Integer, String> testSteps;
	static Hashtable <String, String> testParameters = new Hashtable <String, String>();
	
	public static Object[][] getData(String testCaseName, Xls_Reader xls){
		
		// This will read data from excel for respective test case
		// > Find row num on which test is starting
		// > Find total number of rows of data
		// > Find total number of columns of data
		// > Extract data
//		CellReference cellReference = locateStartingCell(xls, "BO", "Requirement ID");
//		PLRequirement plRequirement = new PLRequirement();
//		plRequirement.setRequirementId(xls.getRequirementId(cellReference));
	

		
		// Find row num on which test is starting
		int testStartRowNum = 1;
		while(!xls.getCellData("Test Data", 0, testStartRowNum).equalsIgnoreCase(testCaseName)){
			testStartRowNum++;
			// This will iterate over each and every cell in column 0 till it finds the testcaseName
		}
		
		System.out.println("Test Case "+testCaseName+ " starts from row : " + testStartRowNum);	
		
		
		// Find total number of rows of data
		int dataStartsRowNum = testStartRowNum + 2;
		int rowsOfData = 0;
		while(!xls.getCellData("Test Data", 0, dataStartsRowNum+rowsOfData).equals("")){
			rowsOfData++;
		}
		
		System.out.println("Total data sets for test case "+testCaseName+ " are "+ rowsOfData);
		
		
		// Find total number of columns of data
		int ColStartRowNum = testStartRowNum + 1;
		int ColsOfData = 0;
		while(!xls.getCellData("Test Data", ColsOfData, ColStartRowNum).equals("")){
			ColsOfData++;
		}
		System.out.println("Total number of Parameters for test case "+testCaseName+ " are "+ ColsOfData);
		
		
		// Extract data into HashTables and put one hash table in one single row of first column
		Object testdata[][] = new Object[rowsOfData][1];
		Hashtable<String, String> table = null; 
		int index = 0;
		
		for(int rNum = dataStartsRowNum; rNum<dataStartsRowNum+rowsOfData; rNum++){
			table = new Hashtable<String, String>();
			String forloging = "   ";
			for(int cNum = 0;cNum<ColsOfData;cNum++){			
				String ParameterName = xls.getCellData("Test Data",cNum,ColStartRowNum);
				String ParameterValue = xls.getCellData("Test Data",cNum, rNum);
				forloging = forloging+" " +ParameterName+" -------- "+ParameterValue +"-------";
				//System.out.print(ParameterValue+" -------- ");
				table.put(ParameterName, ParameterValue);
			}
			//forloging = forloging + " ";
			APPLICATION_LOGS.debug(forloging);
			testdata[index][0] = table; // putting table in testdata array starting from index 0 
			index++;
		}
		
		return testdata;
	}

	private static CellReference locateCellReference(Xls_Reader xls, XSSFSheet sheet, 
			String cellContent, Cell cell, CellReference cellReference) {
		
		Integer cellRow = new Integer(0);
		Integer cellColumn = new Integer(0);
			try {
				if (cellContent.contains("WebMachine")){
					cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);
				}
				else if (cellContent.contains("CONFIGURATION DATA - End")){
					if (cell.getStringCellValue().contains(cellContent)){
						cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);	
					}
				}
				else if (cellContent.contains("SCRIPT DATA STORAGE - End")){
					if (cell.getStringCellValue().contains(cellContent)){
						cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);	
					}
				}
				else if (cellContent.contains("Start")){
					cellReference = new CellReference(cell.getRowIndex()+1, cell.getColumnIndex(), true, true);
				}
				else if (cellContent.contains("Script")){
					cellReference = new CellReference(cell.getRowIndex()+1, cell.getColumnIndex(), true, true);
				}	
				else if (cellContent.contains("pageElementKey")){
					cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);
				}				
				else if (cellContent.contains("Runmode")){
					cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);
				}	
				else if (cellContent.contains("TEST STEPS - End")){
					if (cell.getStringCellValue().contains(cellContent)){
						cellReference = new CellReference(cell.getRowIndex(), cell.getColumnIndex(), true, true);	
					}
				}				
				else{
					cellColumn = xls.getCellColumnIndex(sheet, cell.getRowIndex(), cellContent);
					cellReference = new CellReference(cell.getRowIndex(), cellColumn, true, true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		//}

		return cellReference;
	}

	public static String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static boolean getRunmode(String testName, String testSuite, Xls_Reader xls){
		
		// This will Iterate through every row of test cases sheet and check for the testName.
		// If found it will check its runmode and return boolean value or else return false by default.
		
		for(int rNum = 2; rNum<=xls.getRowCount(testSuite);rNum++){
			String testCaseName = xls.getCellData(testSuite, "TCID", rNum);
			if(testCaseName.equalsIgnoreCase(testName)){
				// check runmode
				if(xls.getCellData(testSuite, "Runmode", rNum).equalsIgnoreCase("Y")){
					return true;
				}else {
					return false;
				}
					
			}
				
		}
		
		
		return false;
	}
	
	
	public static boolean getSuiteRunmode(String suiteName, Xls_Reader xls){
			
			// This will Iterate through rows of test suites sheet and check for the runmode of automation suite.
			// If found it will check its runmode and return boolean value or else return false by default.
			
			for(int rNum = 2; rNum<=xls.getRowCount("Test Suites");rNum++){
				String testSuiteName = xls.getCellData("Test Suites", "Test Suite ID", rNum);
				if(testSuiteName.equalsIgnoreCase(suiteName)){
					// check runmode
					if(xls.getCellData("Test Suites", "Runmode", rNum).equalsIgnoreCase("Y")){
						return true;
					}else {
						return false;
					}
						
				}
					
			}
			
			
			return false;
		}
	

	// update results for a particular data set	
	public static void reportDataSetResult(Xls_Reader xls, String testCaseName, int rowNum,String result){	
		xls.setCellData(testCaseName, "Results", rowNum, result);
	}


	public static Object[][] getTestData(String sheetName, WebMachine_Reader xls) {
		
		Object testdata[][] = new Object[1][3];
		
		XSSFSheet sheet = ((Xls_Reader) xls).getWorkbook().getSheet(sheetName);
		CellReference testIdCellReference = new CellReference(0, 0, true, true);

		/*Step 1. Setup the Configuration data*/
		Cell finalConfigCell = setConfigurationData((Xls_Reader) xls, sheet, testParameters, testIdCellReference);
		
		/*Step 2. Setup the Test Steps data*/
		Cell finalTestStepsCell = setTestSteps((Xls_Reader) xls, sheet, finalConfigCell, testIdCellReference);

		/*Step 3. Setup the Script data*/
		CellReference scriptCellReference = locateCellReference((Xls_Reader) xls, sheet, "Script", finalTestStepsCell, null);
		Cell finalScriptCell = getPageElements((Xls_Reader) xls, sheet, testParameters, scriptCellReference);
		
		
		testdata[0][0] = configurationData;
		testdata[0][1] = testSteps;
		testdata[0][2] = pageElements;
		
		return testdata;
		
	}


	private static Cell getPageElements(Xls_Reader xls, XSSFSheet sheet,
			Hashtable<String, String> testParameters,
			CellReference scriptCellReference) {
//		Cell scriptCell = xls.getThisCell(scriptCellReference, sheet);
//		PageElement pageElement = createPageElement(sheet, scriptCell, scriptCellReference);
		
		Cell firstScriptDataCell = xls.getNextCell(scriptCellReference, sheet);
		CellReference scriptStartCellReference = locateCellReference(xls, sheet, firstScriptDataCell.getStringCellValue(), 
				firstScriptDataCell, null);
		Hashtable<Integer, Row> cellIndexAndRow = 
				xls.getCellIndexAndRow(sheet, "SCRIPT DATA STORAGE - End", scriptStartCellReference);

		Row row = cellIndexAndRow.get(0); 
		Cell scriptEndCell = row.getCell(0);
		CellReference scriptEndCellReference = locateCellReference(xls, sheet, 
				"SCRIPT DATA STORAGE - End", scriptEndCell, null);

		if (scriptEndCellReference.getRow() - scriptStartCellReference.getRow() == 1){
			//no test data exists, move on
		}
		else{
			pageElements = xls.getPageElements(sheet, scriptStartCellReference, scriptEndCellReference);
		}
		return scriptEndCell;
		
	}


	private static Schneider_PageElement createPageElement(
			XSSFSheet sheet, Cell scriptCell, CellReference scriptCellReference) {
		int startingRow = scriptCellReference.getRow();
		Integer index = new Integer(scriptCellReference.getCol());
		String pageElementAttribute = new String();
		int scriptCellColumn = scriptCell.getColumnIndex();
		int cellRowIndex = scriptCell.getRowIndex();
		int cellReferenceRowIndex = scriptCellReference.getRow();
		rowCheck:
			for(Row row : sheet) {
				cellCheck:
					for(Cell cell : row) {
						if (row.getRowNum() == startingRow){
							if (cell.getColumnIndex() == scriptCellColumn){
								pageElementAttribute = cell.getStringCellValue();
								if (!(pageElementAttribute.length() > 0)) {
									scriptCellColumn = scriptCellColumn+1;
								}
								else{
									scriptCellColumn = scriptCellColumn+1;
									continue cellCheck;
								}
							}
						}
					}   
			}

		return null;
	}


	private static Cell setTestSteps(Xls_Reader xls, XSSFSheet sheet,
			Cell finalConfigCell, CellReference cellReference) {
		
		CellReference testStepsStartCellReference = locateCellReference(xls, sheet, "Start", 
				finalConfigCell, cellReference);
		Hashtable<Integer, Row> cellIndexAndRow = xls.getCellIndexAndRow(sheet, "TEST STEPS - End", 
				testStepsStartCellReference);

		Row row = cellIndexAndRow.get(new Integer(testStepsStartCellReference.getCol()));
		Cell testStepsEndCell = row.getCell(0);
		CellReference testStepsEndCellReference = locateCellReference(xls, sheet, "TEST STEPS - End", 
				testStepsEndCell, testStepsStartCellReference);

		if ((testStepsEndCellReference.getRow()-1) - (testStepsStartCellReference.getRow()+1) == 1){
			//no test data exists, move on
		}
		else{
			testSteps = xls.getTestStepsData(sheet, testStepsStartCellReference, testStepsEndCellReference);
		}
		return testStepsEndCell;
	}


	private static Cell setConfigurationData(Xls_Reader xls, XSSFSheet sheet,
			Hashtable<String, String> testParameters,
			CellReference testIdCellReference) {

		Cell firstCell = xls.getNextCell(testIdCellReference, sheet);
		CellReference configurationStartCellReference = 
				locateCellReference(xls, sheet, firstCell.getStringCellValue(), firstCell, null);
		Hashtable<Integer, Row> cellIndexAndRow = xls.getCellIndexAndRow(sheet, "CONFIGURATION DATA - End", configurationStartCellReference);

		Row row = cellIndexAndRow.get(0);
		Cell configurationEndCell = row.getCell(0);
		CellReference configurationEndCellReference = 
				locateCellReference(xls, sheet, "End", configurationEndCell, configurationStartCellReference);

		if (configurationEndCellReference.getRow() - configurationStartCellReference.getRow() == 1){
			//no test data exists, move on
		}
		else{
			configurationData = xls.getTestConfigData(sheet, configurationStartCellReference, configurationEndCellReference);
		}
		return configurationEndCell;
		
		
	}


	public static Hashtable<String, String> getTestParameters() {
		return testParameters;
	}


	public static Hashtable<Integer, String> getTestSteps() {
		return testSteps;
	}


	public static Hashtable<String, String> getConfigurationData() {
		return configurationData;
	}


	public static void setConfigurationData(
			Hashtable<String, String> configurationData) {
		DataUtil.configurationData = configurationData;
	}

	public static Map<String, Schneider_PageElement> getScriptData() {
		return pageElements;
	}
	
}
