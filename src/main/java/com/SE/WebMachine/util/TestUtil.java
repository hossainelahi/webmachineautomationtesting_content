package com.SE.WebMachine.util;

import java.util.Hashtable;

import org.apache.log4j.Logger;


public class TestUtil {
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	
	public static Object[][] getdata(String testCaseName, Xls_Reader xls){
		// Find row num on which test is starting
		int testStartRowNum = 1;
		while(!xls.getCellData("Test Data", 0, testStartRowNum).equalsIgnoreCase(testCaseName)){
			testStartRowNum++;
			// This will iterate over each and every cell in column 0 till it finds the testcaseName
		}
		// Find total number of rows of data
		int dataStartsRowNum = testStartRowNum + 2;
		int rowsOfData = 0;
		while(!xls.getCellData("Test Data", 0, dataStartsRowNum+rowsOfData).equals("")){
			rowsOfData++;
		}
		// Find total number of columns of data
		int ColStartRowNum = testStartRowNum + 1;
		int ColsOfData = 0;
		while(!xls.getCellData("Test Data", ColsOfData, ColStartRowNum).equals("")){
			ColsOfData++;
		}
		// Extract data into HashTables and put one hash table in one single row of first column
		Object testdata[][] = new Object[rowsOfData][1];
		Hashtable<String, String> table = null; 
		int index = 0;
		
		for(int rNum = dataStartsRowNum; rNum<dataStartsRowNum+rowsOfData; rNum++){
			table = new Hashtable<String, String>();
			String forloging = "   ";
			for(int cNum = 0;cNum<ColsOfData;cNum++){			
				String ParameterName = xls.getCellData("Test Data",cNum,ColStartRowNum);
				String ParameterValue = xls.getCellData("Test Data",cNum, rNum);
				forloging = forloging+" " +ParameterName+" -------- "+ParameterValue +"-------";
				table.put(ParameterName, ParameterValue);
			}
			APPLICATION_LOGS.debug(forloging);
			testdata[index][0] = table; // putting table in testdata array starting from index 0 
			index++;
		}
		return testdata;
	}

	
	public static boolean getRunmode(String testName, String testSuite, Xls_Reader xls){
		for(int rNum = 2; rNum<=xls.getRowCount(testSuite);rNum++){
			String testCaseName = xls.getCellData(testSuite, "TCID", rNum);
			if(testCaseName.equalsIgnoreCase(testName)){
				if(xls.getCellData(testSuite, "Runmode", rNum).equalsIgnoreCase("Y")){
					return true;
				}else {
					return false;
				}
			}
		}
		return false;
	}
	
	
public static boolean getSuiteRunmode(String suiteName, Xls_Reader xls){
		
		// This will Iterate through rows of test suites sheet and check for the runmode of automation suite.
		// If found it will check its runmode and return boolean value or else return false by default.
		
		for(int rNum = 2; rNum<=xls.getRowCount("Test Suites");rNum++){
			String testSuiteName = xls.getCellData("Test Suites", "Test Suite ID", rNum);
			if(testSuiteName.equalsIgnoreCase(suiteName)){
				// check runmode
				if(xls.getCellData("Test Suites", "Runmode", rNum).equalsIgnoreCase("Y")){
					return true;
				}else {
					return false;
				}
			}
		}
		
		
		return false;
	}
	public static void reportDataSetResult(Xls_Reader xls, String testCaseName, int rowNum,String result){	
		xls.setCellData(testCaseName, "Results", rowNum, result);
	}

	
}
