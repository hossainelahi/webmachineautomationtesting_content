package com.SE.WebMachine.util;

// This will have all the constants, web element locators to be used in WebMachine automation tests.

public class Constants {

	public static class Paths{
		
		// This will have Paths of all external resources.
		
		public static String  Config_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\config.properties";
		public static String XlsReader_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\testdata\\WebMachine_TestData.xlsx";
		public static String Config_FOLDER_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\";
		public static String ATU_ReporterConfig_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\atu.properties";
	
	}
	public class WebMachine_GuidedTourPage{
		// This will have locators of the web elements on WebMachine Guided Tour page.
		//public static final String CloseTheGuidedVisit_Link = ".//*[@id='index']/div[10]/ul";
		//public static final String CloseTheGuidedVisit_Link = ".//*[@id='index']/div[8]/div/div/div/p[3]";
		
		//public static final String CloseTheGuidedVisit_Link = "//p[@class='v-guide-end-v']";
		public static final String CloseTheGuidedVisit_Link = "v-guide-start-v"; 	
		public static final String CloseTheGuidedVisit_LinkOnDispatchLoad = "v-guide-end-v"; 	
		public static final String CloseTheGuidedVisit_LinkOnAjaxCountry = "//*[@id='dispatch']/div[4]/div[1]/p[5]";		
		//public static final String AccessTheSite_Link = "v-guide-close.v-guide-main-el";
		//public static final String AccessTheSite_Link = "//*[@id='index']/p/text()";
		public static final String AccessTheSite_Link = "//*[@id='index']/p/span";
		public static final String AccessTheSite_LinkOnDispatchLoad = "v-guide-close";
		public static final String AccessTheSite_LinkOnAjaxCountry = "//*[@id='dispatch']/div[4]/p";
		public static final String AcceptCookies_Link = "buorgclose"; //DH

		
		
		
		
	}
	
	public class WebMachine_DispatchPage{
		// This will have locators of the web elements on WebMachine A2 dispatch page.
		//public static final String SELogoHeader_Link = "h1>a[title = 'Schneider-Electric']>img.logo-header";
		
		public static final String header_Top_Menu = "//*[@class='header-content'] //*[@class='main-menu']/li/ul/li[not(@id) and (not(@class) or (@class='') or (@class='selected') )]";
			//   "//li[@class='menu']/ul[@class='main-menu']/li[not(@id) and (not(@class) or (@class='') or (@class='selected') )]";   // added by Lokesh 6-2015
		
		public static final String header_Container = "//div[@class='header-container'] ";   //added by Lokesh 7-2015
		
		public static final String header_MegaMenu_Left_Category = "//li[@class='drop-category selected'] //div[@class='wrapper']/ul[not(@class)]/li";  // added by Lokesh 7-2015
		
		public static final String header_MMLeftCat_RightMenu_linkClass = " //div/a[@class='leaf']";  // added by Lokesh
		
		public static final String header_MegaMenu_Right_Category =" //div/ul[not(@class)]/li"; // added by Lokesh
		
		
		
		public static final String dispatchZones = "//*[@class='slides'] //*[contains(@id,'zone')]"; // added by Lokesh
		
		public static final String zoneSection_Link = "//*[contains(@id,'zone')]"; // added by Lokesh
		
		public static final String selectCountry_UpperHeader_Link = "//*[@id='header-country-selector']/a";
		//		"//*[@class='footer-bottom-bar'] //*[@class='footer-locale'] //a[@class='country-selector']"; // added by Lokesh
		
		public static final String geographyElements = "//*[@class='language-details']/li";  // added by Lokesh
		
		public static final String zoneALink = "//*[@id='zoneA']";
		
		public static final String zoneBLink = "//*[@id='zoneB']";
		
		public static final String zoneCLink = "//*[@id='zoneC']";
		
		
		
		//public static final String header_Top_Menu = "//li[@class='menu']/ul[@class='main-menu']/li[not(@id) and (not(@class) or (@class='') )]";   // added by Lokesh 6-2015
		//public static final String SelectCountry_Header_Link = "a.language-selector dropdown";
		public static final String SelectCountry_Header_Link = "//*[@id='header-menu']/header/nav/ul/li[1]/a";
		//public static final String SelectCountry_Header_Link = "//a[@class='language-selector dropdown']";
		
		public static final String OurCompany_Header_Link = "//div[@id='header-menu']/header/nav/ul/li[2]/a";
		public static final String FaceBook_Header_Link = "//div[@class='social deployed']/a[1]";
		public static final String Twitter_Header_Link = "//div[@class='social deployed']/a[2]";
		public static final String MoreSocialMedia_Header_Link = "//div[@class='social deployed']/a[3]";
		public static final String Social_Options_Header_Link = "//ul[@class='social-options']"; // ul.social-options
		public static final String AtHome_Link = "//a[@id='zoneA']";  // @Home section
		public static final String ZoneB_Link = "//a[@id='zoneB']";  // @Work Section
		public static final String ZoneC_Link = "//a[@id='zoneC']";  // @Partners Section
		//public static final String CountryName_Link = "//a[@class='language-selector dropdown']";
		public static final String CountryName_Link = "*[@id='header-menu']/header/nav/ul/li[1]/a";
		public static final String ZoneSection_Link = "//div[@id='diagonal-splash']//a";
		//public static final String ZoneSection_Link = "//*[@id='diagonal-splash']/div[@id='nav']";
		
		public static final String DispatchZones = "//div[@id='diagonal-splash']";
		public static final String Continents_Links = "//ul[@class='language-details']/li";
		public static final String AccessTheSite_Link = ".//*[@id='index']/p";
		public static final String FinishGuidedVisit_Link = "//*[@id='index']/div[8]/div/div/div/p[3]";
//		public static final String Geography_Options = "//*[@id='header-menu']/header/nav/ul/li[1]/ul/li";
//		public static final String Geography_Elements = "//*[@id='header-menu']/header/nav/ul/li[1]/ul/li";
		public static final String Geography_Elements = "//*[@id='page']/div/div/ul/li";
														 //*[@id="header-menu"]/header/nav/ul/li[1]/a
		public static final String Country_Options = "//*[@id='page']/div/div/ul/li[X]/ul/li[Y]/a";
	}
	
	public class WebMachine_CommonConstants{
		
		// This class will contain all the common constants(WebElements) used on different pages of WebMachine
		
		public static final String countrySelectorClose = "//*[@id='notification-close']";
		
		public static final String schneiderHeaderLogo = "//*[@class='logo-header']/img"; // added by Lokesh 6-2015 
		
		public static final String searchIconOnHeader = "//*[contains(@class,'icon-search-b2')]";  // code refactoring by Lokesh  
//				"//*[@class='search-button']/i[@class='icon-search-b2']";  // added by Lokesh 7-2015
		
		public static final String loginJoinPartner = "//*[@id='header-login-link']/a";
				//	"//*[@class='login-link']";
		
		public static final String whereDropDownCssPath= "div.item";
				
		public static final String SELogoHeader_Link = "//a[@title = 'Schneider Electric']/img";
	
		public static final String LifeIsOnFooter_Link = "//img[@alt='Default Alternative Text']";
			//	"//img[@alt='Life is On by Schneider Electric']";
				//"//*[contains(@src,'LIO_footer.png')]"; 
				//"//ul[@class='footer-main'] //a/img[contains(@src,'footer.png')]";  // added by Lokesh 7-2015 
				//"//*[@id='footer-social']/img";
		
		public static final String footerSocial_Link = "//ul[@class='footer-links'] //ul[@class='footer-social']/li";   // added by Lokesh
		
		public static final String footerMain_Link = "//ul[@class='footer-main'] //a[@title]"; // added by Lokesh
		
		public static final String footerMainList = "//ul[@class='footer-main']/li";   // added by Lokesh
		
		public static final String footerLinksList = "//ul[@class='footer-links']/li";
			//	+ " //ul[not(@class)]";   // added by Lokesh
		
		public static final String rotatingCarousel = "//*[@class='pagination']/li"; // added by Lokesh 
		
		public static final String footerBottomList = "//ul[@class='footer-about']/li";
		
		public static final String customerCareRadioButton = "//*[@for='mktoRadio_12002_1']";
		
		public static final String customerCare_whatCanHelpWith = "//*[@for='SupportCategory__c']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='item']";
		
		public static final String customerCare_whatCanHelpWithOptionOne = "//*[@for='SupportCategory__c']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='selectize-dropdown-content'] //div[not(@data-value='')]";
		
		public static final String customerCare_whatSegmant = "//*[@for='subject']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='item']";
		
		public static final String customerCare_whatSegmantOptionOne = "//*[@for='subject']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='selectize-dropdown-content'] //div[not(@data-value='')]";
		
		public static final String customerCare_narrowYourSelection = "//*[@for='Subject_Lvl2']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='item']";
		
		public static final String customerCare_narrowYourSelectionOptionOne = "//*[@for='Subject_Lvl2']/following-sibling::div[@class='selectize-control mktoField mktoHasWidth single'] //div[@class='selectize-dropdown-content'] //div[not(@data-value='')]";
		
		public static final String customerCare_submitButton = "//button[@class='mktoButton']";
		
		
		public static final String FaceBook_Footer_Link = "//div[@class='social']/a";
		public static final String Twitter_Footer_Link = "//div[@class='social']/a[2]";
		public static final String MoreSocialMedia_Footer_Link = "//div[@class='social']/a[3]";
		public static final String Social_Options_Footer_Link = "//ul[@class='socials']";
		public static final String FooterLinks = "//ul[@id='footer-links']";      // get count of footer links, could be 2 or 3
		public static final String HomeFooter_Links = "//ul[@id='footer-links']/li[1]/ul";
		public static final String WorkFooter_Links = "//ul[@id='footer-links']/li[2]/ul";
		public static final String PartnerFooter_Links = "//ul[@id='footer-links']/li[3]/ul";
		public static final String AboutFooter_Links = "//ul[@id='footer-about']";
		public static final String SE_Legal1_Link = "//ul[@id='footer-legals']/li[1]/a";
		public static final String SE_Legal2_Link ="//ul[@id='footer-legals']/li[2]/a";
		public static final String Search_Inputbox = "//input[@id='search_input_field']";
		public static final String SearchIcon_Button = "//div[@id='search-form']/form/input[2]";
		public static final String SearchResultPage_Check = "//input[@id='search_input_field']";
		public static final String AboutUSFooterColumn_Link = "//ul[@id='footer-about']/li";
		public static final String FooterSection_Links = "//ul[@id='footer-links']/li";
		public static final String FooterLegal_Links = "//ul[@id='footer-legals']/li";
		//public static final String FooterSocial_Links = "//div[@id='footer-social']/div[@class='social']/a";
		public static final String FooterSocial_Links = "//*[@id='footer-social']/div/a";
		public static final String FooterMoreSocial_Link = "//div[@id='footer-social']/div/a[3]";
		public static final String FooterMoreSocialOptions_Links = "//div[@class]/ul[@id='socials']/li";
		public static final String FooterMoreSocialOptions_Link = "//*[@id='socials']/li";
		public static final String HeaderSocial_Links = "//div[@id='header-menu']/header/div[2]/a";
		public static final String HeaderMoreSocial_Link = "//div[@id='header-menu']/header/div[2]/a[3]";
		public static final String HeaderMoreSocialOptions_Links = "//ul[@class='social-options']/li";
		public static final String HeaderMoreSocialOptions_Link = "//*[@id='header-menu']/header/div[2]/ul/li";		
		public static final String CanonicalURL_Check = "//link[@rel='canonical']";
		public static final String CanonicalURL_Check_Alternative = "//head//link[@rel='canonical']";
		public static final String LandingPageCountryName = "span.CountryTitle";
//		public static final String CloseTheAccessTheSite_Link = ".//p[@class='v-guide-end-v']";
//		public static final String CloseTheGuidedVisit_Link = "//*[@id='index']"; 
		public static final String CountryNames_Link = "//*[@id='odmn_globallinks']/li[1]/span";
		public static final String MethodFailureMessage = "INDIVIDUAL TEST RESULT = FAILED !!. See following stacktrace plus above console output for point of failure in";
		


		
		public static final String AtHomeFooter_Link = "//*[@id='footer-links']/li[1]/a";
		public static final String CloseTheChat_Link = "liveagent_invite_button_573A000000005IQ";
		
		/*
		 * Even though the preceding String reads "v-guide-close v-guide-main-el" in the HTML, 
		 * it must be set here as follows "v-guide-close.v-guide-main-el" otherwise you'll get a 
		 * org.openqa.selenium.InvalidSelectorException: with the following message: 
		 * Compound class names are not supported. Consider searching for one class name and 
		 * filtering the results or use CSS selectors.
		 */
		
	}
	
	public class WebMachine_WorkPage{
		
		public static final String solutionItemBoxes= "//*[contains(@class,'picks-questions')]";
	}
	
	public class WebMachine_HomeLandingPage{
		
		public static final String LifeIsOn_Img = "//img[contains(@src,'life-is-on.png')]";
     	//public static final String HomeHeaderTopMenu_Links = "//nav[@class='top-menu']/ul/li";
     	public static final String HomeHeaderTopMenu_Links = ".//*[@id='header-menu']/header/nav/ul/li";
		//public static final String HomeHeaderHorizontalMenu_Links = "//div[@class='wrapper clearfix']/ul/li";
     	public static final String HomeHeaderHorizontalMenu_Links = "//*[@id='header-menu']/nav[1]/div/ul/li";
		
		public static final String LanguageSelectorDropdown = "//a[@class='language-selector dropdown']";
		public static final String HomeTipsSection1 = "//*[@id='page']/div[2]/div[1]/ul/li[1]/a/h3";
		public static final String HomeTipsSection_Links1 = "//ul[@class='tips-section']/li";		
		public static final String HomeTipsSection_Links2 = "//*[@id='page']/div[2]/div[1]/ul/li";
//		public static final String HomeTipsSection_Links2 = "//*[@id='page']/div[2]/div[1]/ul";
		public static final String HomeProductsSection_Links = "//div[@id='home-products']";
//		public static final String HomeProductsSection_List = "//ul[@class='home-products']/li";  // added by lokesh
		public static final String HomeProductsSection_List = "//*[@id='home-products']/section/ul/li"; //added by DH
		public static final String HomeProductsSection_Arrow = "//*[@id='home-products']/section/div[2]"; //added by DH
		//public static final String HomePicksSection_Links = "//div[@class='picks']";
//		public static final String HomePicksSection_Links = "//*[@id='page']/div[2]/div[1]/div[3]";
		public static final String HomePicksSection_Links = "//*[@id='page']/div[2]/div[1]/div[3]/div/ul/li";
		//public static final String EmailSignUp_Inputbox = "//input[@id='registrationEmailAddress']";
		
		public static final String EmailSignUp_Inputbox = "//*[@id='registrationEmailAddress']";
		// DH public static final String EmailSignUp_Button = "//form[@id='registrationForm']/input[2]";
		public static final String EmailSignUp_Button = "//*[@id='registrationForm']/input[2]";
		
		
		public static final String EmailSubscriptionConfirmation_check = "//div[@id='thk_content']";
//		public static final String ContactForm_Link = "//div[@class='wrapper clearfix']//ul/li[5]/a";
		public static final String ContactForm_Link = "//*[@id='support-bar']/label/span";
//		public static final String Contact_Link = "//*[@id='page']/div[2]/div/div/div/p[1]/a";		
		public static final String Contact_Link = "//*[@id='support-bar-icons']/ul/li[1]/a/span";			
		public static final String ContactFormName_Inputbox = "//input[@id='name']";
		public static final String ContactFormCompany_Inputbox = "//input[@id='company']";
		public static final String ContactFormJobTitle_Inputbox = "//input[contains(@id,'VNPj')]";
		public static final String ContactFormEmailID_Inputbox = "//input[@id='email']";
		public static final String ContactFormPostalCode_Inputbox = "00NA0000006VNPe";
		public static final String ContactFormPhone_Inputbox = "//input[@id='phone']";
		public static final String ContactFormAddress_Inputbox = "//textarea[contains(@id,'VNPe')]";
		//public static final String ContactFormCategory_Dropbox = "//select[contains(@id,'Ap2')]";
		public static final String ContactFormCategory_Dropbox = "00NA00000081Ap2";
		public static final String ContactFormDescription_Inputbox = "//textarea[@id='description']";
//		public static final String ContactFormSubmit_Button = "//input[@id='submit']"; DH
		//public static final String ContactFormSubmit_Button = "myform"; DH
		public static final String ContactFormSubmit_Button = "//*[@id='myform']/table[5]/tbody/tr[1]/td[2]/a/img";
//		public static final String ContactForm_Frame = "//div[@id='iframe-content']"; DH
		public static final String ContactForm_Frame = "//*[@id='page']/div[2]/div/div/h1";
		public static final String ContactFormConfirmation_check = "//div[@id='contact_form_message_ok']";
		public static final String ContactFormFailMessage_check = "//div[@id='globalErrorMsgContainer']";
		public static final String CloseTheGuidedVisit_Link = "//div[@id='globalErrorMsgContainer']";
		public static final String ZoneA_Id = "zoneA";  // @Home section
		public static final String AtHome_Link = "//*[@id='page']/div[2]/div/div/ul[1]/li/a";  // @Home link
		public static final String ContactFormCategory_DropboxLabel = "labelRenseignement";
		//public static final String HomeFooterSocial_Links = "//div[@class='footer-container']/footer/ul[@class='footer-links']/li/ul[@class='footer-social']/li/a";
		public static final String HomeFooterSocial_Links = "//div[@class='footer-container-old']/footer[@class='wrapper clearfix']/div[@id='footer-social']/div[@class='social']/a";
		public static final String CountryName_Link = "//*[@id='header-menu']/div/div/a";
	}
	
	public class WebMachine_ProductsPage{
		
		public static final String AllProducts_Link = "div.product-list-content > h1";
		public static final String ProductsTab_Link = "//nav[@class='menu']/div/ul/li[3]/a";
		public static final String ProductsPageHeader = "div.product-list-content > h1";
		public static final String TotalProductCategories = "div.product-list-content > section";
		public static final String ProductCategoryName = "//div[@class='product-list-content']/section";
		public static final String WhereToBuy_Link = "//div[@class='desc_product']//a";
		public static final String CategoryProducts = "//*[@id='page']/div[2]/div/div[1]/section[@class='product-list-cat deployed']";
		public static final String WhereToBuy_Button = "//*[@id='page']/div[2]/div/div[1]/div[2]/a";
	}
	
	public class WebMachine_BeInspiredPage{
		public static final String BeInspiredTab_Link = "//nav[@class='menu']/div/ul/li[1]/a";
//		public static final String BeInspiredTab_Link = ".//*[@id='header-menu']/nav[1]/div/ul/li[1]/a";		
		public static final String BeInspiredPageTitle = "div#vertical_picks>h1";
		public static final String BeInspiredTiles = "//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String DemoRoom_Link = "//div[@id='vertical_picks']/ul/li[1]/a";
		public static final String DemoRoomPageTitle = "div#vertical_picks>h1";
		public static final String DemoRoomTiles ="//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String DemoRoomName = "//div[@id='vertical_picks']/ul/li[1]/a/div/span[2]/em";
		public static final String DemoRoomProduct = "//nav[@class='category-list']/ul/li";  // added by Lokesh
		
	}
	
	public class WebMachine_YourProjectPage{
		
		public static final String YourProjectTab_Link = "//nav[@class='menu']/div/ul/li[2]/a";
		public static final String YourProjectPageTitle = "div#vertical_picks>h1>p";
		public static final String YourProjectTiles = "//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String YourProjectDemoTileName = "//div[@id='vertical_picks']/ul/li";
		public static final String YourProjectDemoTile_Link = "li.tall-right>a";
		public static final String YourProjectDemoTilePageTitle = "div.editorial-content>h1";
		
	}
	
}
