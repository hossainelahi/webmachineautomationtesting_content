package com.SE.WebMachine.exceptions;

import org.openqa.selenium.remote.SessionNotFoundException;

public class Schneider_SessionNotFoundException extends SessionNotFoundException {
	private String message = "Driver has been closed in this test or in an earlier test script.";

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
