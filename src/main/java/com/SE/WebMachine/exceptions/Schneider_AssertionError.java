package com.SE.WebMachine.exceptions;

import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;

public class Schneider_AssertionError extends AssertionError {

	private WebElement webElement;
	private PageElement pageElement;
	private StringBuilder message = new StringBuilder("Expected [] but found []");
	private String actual;
	private String expected;

	public Schneider_AssertionError(WebElement webElement, PageElement pageElement){
		super();
		this.webElement = webElement;
		this.pageElement = pageElement;
	}

	public Schneider_AssertionError(WebElement webElement) {
		this.webElement = webElement;
	}

	public WebElement getWebElement() {
		return webElement;
	}

	public PageElement getPageElement() {
		return pageElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

	public void setPageElement(PageElement pageElement) {
		this.pageElement = pageElement;
	}

	public String getMessage() {
		message.insert(message.indexOf("[")+1, webElement.getText());
		message.insert(message.lastIndexOf("[")+1, pageElement.getName());
		return message.toString();
	}

	public void setMessage(StringBuilder message) {
		this.message = message;
	}

	public void setActual(String actual) {
		this.actual = actual;
	}

	public void setExpected(String expected) {
		this.expected = expected;
	}

	public String getActual() {
		return actual;
	}

	public String getExpected() {
		return expected;
	}

	
}
