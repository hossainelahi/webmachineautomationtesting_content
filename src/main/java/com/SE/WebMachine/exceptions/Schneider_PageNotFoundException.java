package com.SE.WebMachine.exceptions;

import com.SE.WebMachine.pageElements.PageElement;

public class Schneider_PageNotFoundException extends Exception {

	private String message = "Could not locate page ";
	private String url = null;
	private String context = null;
	private PageElement pageElement;

	public Schneider_PageNotFoundException(PageElement pageElement) {
		this.pageElement = pageElement;
	}

	public Schneider_PageNotFoundException(String url, String context) {
		this();
		this.url = url;
		this.context = context;
		this.message = this.message.concat(url).concat(context);
	}

	public Schneider_PageNotFoundException() {
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PageElement getPageElement() {
		return pageElement;
	}

	public String getUrl() {
		return url;
	}

	public String getContext() {
		return context;
	}

	protected void setUrl(String url) {
		this.url = url;
	}

	protected void setContext(String context) {
		this.context = context;
	}
	
	
}
