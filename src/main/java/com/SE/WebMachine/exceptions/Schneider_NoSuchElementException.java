package com.SE.WebMachine.exceptions;

import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;


public class Schneider_NoSuchElementException extends Exception {

	private PageElement pageElement;
	private WebElement webElement;
	private String message = "This element could not be found.";

	public Schneider_NoSuchElementException(WebElement webElement,
			PageElement pageElement) {
		this.pageElement = pageElement;
		this.webElement = webElement;
	}

	public String getMessage(){
		return this.message ;
	}
	public PageElement getPageElement() {
		return pageElement;
	}
	
}
