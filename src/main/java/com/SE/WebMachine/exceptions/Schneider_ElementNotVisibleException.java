package com.SE.WebMachine.exceptions;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;


public class Schneider_ElementNotVisibleException extends ElementNotVisibleException{
	private WebElement webElement;
	private PageElement pageElement;
	private StringBuilder envEmessage = new StringBuilder(" is not visible on this page.");
	
	private static String message = "";
	
	public Schneider_ElementNotVisibleException(WebElement webElement, 
			PageElement pageElement) {
		super(message);
		this.webElement = webElement;
		this.pageElement = pageElement;
	}
	public WebElement getWebElement() {
		return webElement;
	}

	public PageElement getPageElement() {
		return pageElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

	public void setPageElement(PageElement pageElement) {
		this.pageElement = pageElement;
	}

	public String getMessage() {
		envEmessage.insert(0, pageElement.getType() + "" + pageElement.getName());
		return envEmessage.toString();
	}
}
