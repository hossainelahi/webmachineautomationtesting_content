package com.SE.WebMachine.exceptions;

public class Schneider_BrowserNotSupportedException extends Exception {

	private String message = "No such browser or browser not supported.";

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
