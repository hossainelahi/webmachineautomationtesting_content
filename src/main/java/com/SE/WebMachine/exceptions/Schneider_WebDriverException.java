package com.SE.WebMachine.exceptions;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;

public class Schneider_WebDriverException extends WebDriverException{

	private PageElement pageElement;
	private WebElement webElement;

	public Schneider_WebDriverException(WebElement webElement,
			PageElement pageElement) {
		this.pageElement = pageElement;
		this.webElement = webElement;
	}

	public PageElement getPageElement() {
		return pageElement;
	}

	public WebElement getWebElement() {
		return webElement;
	}

	public void setPageElement(PageElement pageElement) {
		this.pageElement = pageElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

}
