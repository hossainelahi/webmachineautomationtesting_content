package com.SE.WebMachine.exceptions;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;


public class Schneider_TimeoutException extends TimeoutException {
	private WebElement webElement;
	private PageElement pageElement;
	private StringBuilder message = new StringBuilder(" timed out while searching for this link - ");
	public Schneider_TimeoutException(WebElement webElement,PageElement pageElement) {
		this.webElement = webElement;
		this.pageElement = pageElement;
	}
	public Schneider_TimeoutException(String timeOutMessage) {
		StringBuilder newMessage = new StringBuilder(timeOutMessage);
		setMessage(newMessage);
		//this.message = newMessage;
	}
	public WebElement getWebElement() {
		return webElement;
	}
	public PageElement getPageElement() {
		return pageElement;
	}
	public String getMessage() {
		if (pageElement != null){
			message.insert(0, pageElement.getType() + "" + pageElement.getName());
			message.append(pageElement.getType() + "" + pageElement.getName());
		}
		return message.toString();
	}
	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}
	public void setPageElement(PageElement pageElement) {
		this.pageElement = pageElement;
	}
	public void setMessage(StringBuilder Message) {
		this.message = Message;
	}
	
}
