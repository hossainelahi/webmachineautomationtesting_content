package com.SE.WebMachine.exceptions;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.SE.WebMachine.pageElements.PageElement;

public class Schneider_StaleElementReferenceException extends StaleElementReferenceException{
	private WebElement webElement;
	private PageElement pageElement;
	private StringBuilder envEmessage = new StringBuilder(" is not visible because the reference to " + getWebElement().getText() + " is 'stale'. In the code try calling getActivePageAndWebElements() immediately before you interact with the web page.");
	
	private static String message = "";
	
	public Schneider_StaleElementReferenceException(WebElement webElement, 
			PageElement pageElement) {
		super(message);
		this.webElement = webElement;
		this.pageElement = pageElement;
	}
	public WebElement getWebElement() {
		return webElement;
	}

	public PageElement getPageElement() {
		return pageElement;
	}

	public void setWebElement(WebElement webElement) {
		this.webElement = webElement;
	}

	public void setPageElement(PageElement pageElement) {
		this.pageElement = pageElement;
	}

	public String getMessage() {
		envEmessage.insert(0, pageElement.getType() + "" + pageElement.getName());
		return envEmessage.toString();
	}
}
