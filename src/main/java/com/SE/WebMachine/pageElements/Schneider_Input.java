package com.SE.WebMachine.pageElements;



public class Schneider_Input extends Schneider_PageElement {

	public Schneider_Input(String type) {
		super(type);
	}

	public Schneider_Input() {
		super();
	}
	@Override
	protected boolean isClickable() {
		return false;
	}
}
