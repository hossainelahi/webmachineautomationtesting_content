package com.SE.WebMachine.pageElements;


public class Schneider_Language {

	private String id;
	private String languageName;
	private String language_code;
	public Schneider_Language(String languageName) {
		this.languageName = languageName;
	}
	public Schneider_Language() {
	}
	public String getId() {
		return id;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.languageName = name;
	}
	public String getLanguage_code() {
		return language_code;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}

}
