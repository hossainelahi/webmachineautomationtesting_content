package com.SE.WebMachine.pageElements;


public class Schneider_Dropdown extends Schneider_PageElement {

	private Integer itemId;
	private Schneider_Language language;
	
	@Override
	protected boolean isClickable() {
		// TODO Auto-generated method stub
		return false;
	}

	public Integer getItemId() {
		return itemId;
	}

	public Schneider_Language getLanguage() {
		return language;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public void setLanguage(Schneider_Language language) {
		this.language = language;
	}

}
