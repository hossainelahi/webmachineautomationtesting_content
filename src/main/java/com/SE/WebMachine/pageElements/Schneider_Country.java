package com.SE.WebMachine.pageElements;

public class Schneider_Country {

	private String value;
	private String countryName;
	private boolean isPRM;

	public Schneider_Country(String value, String countryName, boolean isPRM) {
		this.value = value;
		this.countryName = countryName;
		this.isPRM = isPRM;
	}

	public Schneider_Country(String countryName) {
		this.countryName = countryName;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return countryName;
	}

	public boolean isPRM() {
		return isPRM;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setName(String name) {
		this.countryName = name;
	}

	public void setPRM(boolean isPRM) {
		this.isPRM = isPRM;
	}

}
