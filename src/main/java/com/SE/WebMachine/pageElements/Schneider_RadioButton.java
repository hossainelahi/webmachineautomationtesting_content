package com.SE.WebMachine.pageElements;

public class Schneider_RadioButton extends Schneider_Button {

	private Integer radioId;
	private Schneider_Country country;

	public Schneider_RadioButton(Integer buttonNumber) {
		this.radioId = buttonNumber;
	}

	public Schneider_RadioButton() {
	}

	public Integer getRadioId() {
		return radioId;
	}

	public void setRadioId(Integer radioId) {
		this.radioId = radioId;
	}

	public void setCountry(Schneider_Country country) {
		this.country = country;
	}

	public Schneider_Country getCountry() {
		return country;
	}

}
