package com.SE.WebMachine.pageElements;

import org.openqa.selenium.WebElement;

public class Schneider_User {

	private Schneider_PageElement dropdownPageElement;
	private WebElement dropdownWebElement;
	private Schneider_PageElement inputNamePageElement;
	private WebElement inputNameWebElement;
	private String inputNameWebElementValue;
	private Schneider_PageElement inputPasswordPageElement;
	private WebElement inputPasswordWebElement;
	private String inputPasswordWebElementValue;
	private String dropdownWebElementValue;
	private Schneider_PageElement btnSavePageElement;
	private WebElement btnSaveWebElement;
	private WebElement btnNextWebElement;
	private Schneider_PageElement btnNextPageElement;
	private Schneider_PageElement lblStatusPageElement;
	private WebElement lblStatusWebElement;
	private String lblStatusWebElementValue;

	public void setDropdownPageElement(Schneider_PageElement pageElement) {
		this.dropdownPageElement = pageElement;
	}

	public void setDropdownWebElement(WebElement webElement) {
		this.dropdownWebElement = webElement;
	}

	public Schneider_PageElement getDropdownPageElement() {
		return dropdownPageElement;
	}

	public WebElement getDropdownWebElement() {
		return dropdownWebElement;
	}

	public void setInputNamePageElement(Schneider_PageElement inputNamePageElement) {
		this.inputNamePageElement = inputNamePageElement;
		
	}

	public void setInputNameWebElement(WebElement inputNameWebElement) {
		this.inputNameWebElement = inputNameWebElement;
		
	}

	public void setInputNameWebElementValue(String inputNameWebElementValue) {
		this.inputNameWebElementValue = inputNameWebElementValue;
		
	}

	public void setinputPasswordPageElement(Schneider_PageElement inputPasswordPageElement) {
		this.inputPasswordPageElement = inputPasswordPageElement;
		
	}

	public void setInputPasswordWebElement(WebElement inputPasswordWebElement) {
		this.inputPasswordWebElement = inputPasswordWebElement;
		
	}

	public void setInputPasswordWebElementValue(String inputPasswordWebElementValue) {
		this.inputPasswordWebElementValue = inputPasswordWebElementValue;
		
	}

	public Schneider_PageElement getInputNamePageElement() {
		return inputNamePageElement;
	}

	public WebElement getInputNameWebElement() {
		return inputNameWebElement;
	}

	public String getInputNameWebElementValue() {
		return inputNameWebElementValue;
	}

	public Schneider_PageElement getInputPasswordPageElement() {
		return inputPasswordPageElement;
	}

	public WebElement getInputPasswordWebElement() {
		return inputPasswordWebElement;
	}

	public String getInputPasswordWebElementValue() {
		return inputPasswordWebElementValue;
	}

	public void setDropdownWebElementValue(String dropdownWebElementValue) {
		this.dropdownWebElementValue = dropdownWebElementValue;
	}

	public String getDropdownWebElementValue() {
		return dropdownWebElementValue;
	}

	public void setBtnSavePageElement(Schneider_PageElement btnSavePageElement) {
		this.btnSavePageElement = btnSavePageElement;
	}
	public void setSaveWebElement(WebElement btnSaveWebElement) {
		this.btnSaveWebElement = btnSaveWebElement;
	}

	public Schneider_PageElement getBtnSavePageElement() {
		return btnSavePageElement;
	}

	public WebElement getBtnSaveWebElement() {
		return btnSaveWebElement;
	}

	public void setInputPasswordPageElement(Schneider_PageElement inputPasswordPageElement) {
		this.inputPasswordPageElement = inputPasswordPageElement;
	}

	public Schneider_PageElement getBtnNextPageElement() {
		return btnNextPageElement;
	}

	public void setBtnNextWebElement(WebElement btnNextWebElement) {
		this.btnNextWebElement = btnNextWebElement;
	}

	public void setBtnSaveWebElement(WebElement btnSaveWebElement) {
		this.btnSaveWebElement = btnSaveWebElement;
	}

	public WebElement getBtnNextWebElement() {
		return btnNextWebElement;
	}

	public void setBtnNextPageElement(Schneider_PageElement btnNextPageElement) {
		this.btnNextPageElement = btnNextPageElement;
	}

	public void setLblStatusPageElement(Schneider_PageElement lblStatusPageElement) {
		this.lblStatusPageElement = lblStatusPageElement;
	}

	public void setLblStatusWebElement(WebElement lblStatusWebElement) {
		this.lblStatusWebElement = lblStatusWebElement;
	}

	public void setLblStatusWebElementValue(String lblStatusWebElementValue) {
		this.lblStatusWebElementValue = lblStatusWebElementValue;
	}

	public Schneider_PageElement getLblStatusPageElement() {
		return lblStatusPageElement;
	}

	public WebElement getLblStatusWebElement() {
		return lblStatusWebElement;
	}

	public String getLblStatusWebElementValue() {
		return lblStatusWebElementValue;
	}

}
