package com.SE.WebMachine.pageElements;



public class Schneider_Button extends Schneider_PageElement{

public Schneider_Button(String type){
	super(type);
}

public Schneider_Button() {
	super();
}

@Override
protected boolean isClickable() {
	return true;
}
	
}
