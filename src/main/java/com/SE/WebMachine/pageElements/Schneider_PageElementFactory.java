package com.SE.WebMachine.pageElements;

public class Schneider_PageElementFactory {

	public static final String BUTTON = "Button";
	public static final String RADIO_BUTTON = "RadioButton";
	public static final String URL = "url";
	public static final String LABEL = "Label";
	public static final String INPUT = "Input";
	public static final String CHECKBOX = "CheckBox";
	public static final String DROPDOWN = "Dropdown";
	public static final String MULTIPLE_SELECT = "MultipleSelect";
	public static final String TABLE = "Table";
	public static final String LINK = "Link";	
	public static final String TYPE = "type";

	public static Schneider_PageElement getPageElement(String type) {

		if (type.equalsIgnoreCase(BUTTON)) {
			return new Schneider_Button();
		} else if (type.equalsIgnoreCase(RADIO_BUTTON)) {
			return new Schneider_RadioButton();			
		} else if (type.equalsIgnoreCase(URL)) {
			return new Schneider_Url();
		} else if (type.equalsIgnoreCase(LABEL)) {
			return new Schneider_Label();
		} else if (type.equalsIgnoreCase(INPUT)) {
			return new Schneider_Input();
		} else if (type.equalsIgnoreCase(CHECKBOX)) {
			return new Schneider_CheckBox();	
		} else if (type.equalsIgnoreCase(DROPDOWN)) {
			return new Schneider_Dropdown();		
		} else if (type.equalsIgnoreCase(MULTIPLE_SELECT)) {
			return new Schneider_MultipleSelect();	
		} else if (type.equalsIgnoreCase(TABLE)) {
			return new Schneider_Table();		
		} else if (type.equalsIgnoreCase(LINK)) {
			return new Schneider_Link();				
		} else if (type.equalsIgnoreCase(TYPE)) {
			return new Schneider_Type();		
		} else {
			return null;
		}
	}
	
}
