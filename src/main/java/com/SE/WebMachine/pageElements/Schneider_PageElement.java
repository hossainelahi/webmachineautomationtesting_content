package com.SE.WebMachine.pageElements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

public abstract class Schneider_PageElement implements WebElement, Comparable<Schneider_PageElement>{

	private String linkedWebElement;
	private String link;
	private String description;
	private int attributeLocation;
	private Integer locationIndex;
	private String type;
	private String linkedPageText;
	private String key;
	private String scriptValue;
	private String locator;
	private String pageLocation;
	private String name;
	private String message;
	private static Schneider_PageElement pageElement;
	private Schneider_PageElement linkedPageElement;
	private String parentPage;
	
	public Schneider_PageElement(String type) {
		this.type = type;
	}

	public Schneider_PageElement() {
	}

	public int compareTo(Schneider_PageElement pageElement){
		return this.getPageLocationIndex().compareTo(pageElement.getPageLocationIndex());
	}
		
	public String getPageLocationIndex() {
		return getPageLocation().concat(String.valueOf(getLocationIndex()));
	}

	public String getName() {
		return name;
	}

	public String getLink() {
		return link;
	}

	public String getDescription() {
		return description;
	}

	public void setScriptValue(String scriptValue) {
		this.scriptValue = scriptValue;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void click() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void submit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendKeys(CharSequence... keysToSend) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAttribute(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLinkedWebElement() {
		return linkedWebElement;
	}

	public Schneider_PageElement getLabelPageElement() {
		return pageElement;
	}

	public void setLabelPageElement(Schneider_PageElement pageElement) {
		pageElement = pageElement;
	}

	@Override
	public List<WebElement> findElements(By by) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebElement findElement(By by) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLocationIndex(Integer locationIndex) {
		this.locationIndex = locationIndex;
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Point getLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getMessage() {
		return message;
	}

	public Schneider_PageElement getLinkedPageElement() {
		return linkedPageElement;
	}

	@Override
	public Dimension getSize() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCssValue(String propertyName) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setAttributeLocation(int attributeLocation) {
		// TODO Auto-generated method stub
		this.attributeLocation = attributeLocation;
		
	}

	public int getAttributeLocation() {
		return attributeLocation;
	}

	public void setType(String dataValue) {
		this.type = dataValue;
	}

	public void setLinkedPageText(String dataValue) {
		this.linkedPageText = dataValue;		
	}

	public String getType() {
		return type;
	}

	public String getPageLocation() {
		return pageLocation;
	}

	public void setPageLocation(String pageLocation) {
		this.pageLocation = pageLocation;
	}

	public String getLinkedPageText() {
		return linkedPageText;
	}

	public String getKey() {
		return key;
	}

	public String getLocator() {
		return locator;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getScriptValue() {
		return scriptValue;
	}

	public void setLinkedWebElement(String linkedWebElement) {
		this.linkedWebElement = linkedWebElement;
	}

	public void setLocator(String locator) {
		this.locator = locator;
	}

	public Schneider_PageElement getInstance() {
		return pageElement;
	}
	protected abstract boolean isClickable();

	public Integer getLocationIndex() {
		return locationIndex;
	}

	public void setPageLocationIndex(Integer locationIndex) {
		this.locationIndex = locationIndex;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setLinkedPageElement(Schneider_PageElement pageElement) {
		this.linkedPageElement = pageElement;
	}

	public void setParentPage(String parentPage) {
		this.parentPage = parentPage;
	}

	public String getParentPage() {
		return parentPage;
	}


}
