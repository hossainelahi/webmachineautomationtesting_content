package com.SE.WebMachine.pageElements;

import java.util.Set;

public abstract class Schneider_PageModel {
	public abstract void setActivePageElementsOnly(Set<Schneider_PageElement> activePageElements);
	public abstract Set<Schneider_PageElement> getActivePageElements();
}
