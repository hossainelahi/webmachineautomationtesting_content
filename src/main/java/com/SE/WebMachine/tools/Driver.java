package com.SE.WebMachine.tools;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
//import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.exceptions.Schneider_TimeoutException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.util.Constants;
import com.google.common.base.Function;

public class Driver {
	public static WebDriver driver;
	private static String timeOut = new String();
	private static Hashtable<String, String> domains = new Hashtable<String, String>();
	private static String baseDomain;
	private static String landingPageDomain;
	public static void initialize(Hashtable<String, String> configData){
		setTimeOut(configData.get("Timeout"));
		if (configData.get("Browser").equalsIgnoreCase("Chrome")){
			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")	+ configData.get("driverExecutable"));
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")	+ configData.get("driverExecutable"));
			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")	+ "\\src\\test\\java\\com\\SE\\WebMachine\\util\\chromedriver.exe");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setBrowserName("googlechrome");
			cap.setPlatform(Platform.ANY);
			long time = Long.valueOf(timeOut).longValue();
			driver = new ChromeDriver(DesiredCapabilities.chrome());
			
			driver.manage().window().maximize();  // added by Lokesh
			
			driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
		}
	}
	public static WebDriver getInstance() {
		return driver;
	}
	private static String getTimeOut() {
		return timeOut;
	}
	private static void setTimeOut(String timeOut) {
		Driver.timeOut = timeOut;
	}
	public static String getLinkIdentifier(String locator, String location) {
		By by = getBy(locator, location);
		WebElement webElement = getInstance().findElement(by);
		location = webElement.getAttribute("title").trim().length() > 0 ? webElement.getAttribute("title").trim() : webElement.getText().trim();
		//System.out.println(">>>>>>>>in getLinkIdentifier>>>>>>"+location);
		if (location.length() > 0) {
		} 
		else {
			String hReflink = webElement.getAttribute("href");
			if (hReflink != null){
				int firstIndex = hReflink.contains("www") ? webElement
						.getAttribute("href").indexOf(".") : webElement
						.getAttribute("href").indexOf("/") + 1;
				String hReflinkRemainder = webElement.getAttribute("href")
						.substring(firstIndex + 1);
				int secondIndex = hReflinkRemainder.indexOf(".");
				location = webElement.getAttribute("href").substring(
						firstIndex + 1, firstIndex + secondIndex + 1);
			}
		}
		return location;
	}
	public static String getLinkIdentifier(WebElement webElementLink) {
		String linkIdentifier = webElementLink.getAttribute("title").trim()
				.length() > 0 ? webElementLink.getAttribute("title").trim()
				: webElementLink.getText().trim();
		if (linkIdentifier.length() > 0) {

		} else {
			String hReflink = webElementLink.getAttribute("href");
			int firstIndex = hReflink.contains("www") ? webElementLink
					.getAttribute("href").indexOf(".") : webElementLink
					.getAttribute("href").indexOf("/") + 1;
			String hReflinkRemainder = webElementLink.getAttribute("href")
					.substring(firstIndex + 1);
			int secondIndex = hReflinkRemainder.indexOf(".");
			linkIdentifier = webElementLink.getAttribute("href").substring(
					firstIndex + 1, firstIndex + secondIndex + 1);
		}
		return linkIdentifier;
	}
	public static boolean checkLink(Schneider_PageElement pageElement) {
		try {
			By by = getBy(pageElement);
			WebElement webElement = getInstance().findElement(by);
			if (webElement.isEnabled() && webElement.isDisplayed()){
				return true;
			}
		} catch (NoSuchElementException nse) {
			//Reporter.log("Error ! Item with locator " + pageElement.getLink() + " not found.", true);
			System.out.println(pageElement.getKey());
			Reporter.log("Error ! Item with locator " + pageElement.getKey()+ " not found.", true);
		}
		return false;
	}
	public static By getBy(Schneider_PageElement pageElement) {
		By by = null;
		if (pageElement.getLocator().equalsIgnoreCase("id")){
			by = By.id(pageElement.getLink());
		}
		else if (pageElement.getLocator().equalsIgnoreCase("xPath")){
			by = By.xpath(pageElement.getLink());
		}
		else if (pageElement.getLocator().equalsIgnoreCase("linkText")){
			by = By.linkText(pageElement.getLink());
		}
		else if (pageElement.getLocator().equalsIgnoreCase("cssSelector")){
			by = By.cssSelector(pageElement.getLink());
		}
		else if (pageElement.getLocator().equalsIgnoreCase("name")){
			by = By.name(pageElement.getLink());
		}
		else if (pageElement.getLocator().equalsIgnoreCase("className")){
			by = By.className(pageElement.getLink());
		}
		return by;
	}
	public static By getBy(String locator, String location) {
		By by = null;
		if (locator.equalsIgnoreCase("id")){
			by = By.id(location);
		}
		else if (locator.equalsIgnoreCase("xPath")){
			by = By.xpath(location);
		}
		else if (locator.equalsIgnoreCase("linkText")){
			by = By.linkText(location);
		}
		else if (locator.equalsIgnoreCase("cssSelector")){
			by = By.cssSelector(location);
		}
		else if (locator.equalsIgnoreCase("name")){
			by = By.name(location);
		}
		return by;
	}
	public static Hashtable<String, String> click(Schneider_PageElement pageElement, String baseURL, String... testParameters)
			throws ElementNotVisibleException, Schneider_IndexOutOfBoundsException {
		boolean linkClickedSuccessfully = true;
		@SuppressWarnings("unused")
		String itemName = new String();
		String landingPageDomain = new String();
		try {
					
			baseDomain = baseURL.substring(baseURL.indexOf("/") + 2,
					baseURL.indexOf(".") + 1);
			//System.out.println(baseDomain +" >> ");
			By by = getBy(pageElement);
			WebElement webElement = getInstance().findElement(by);
			
			//System.out.println(webElement);

			if (webElement != null){
				//System.out.println("are we in ");
				if (webElement.getText() != null){
					//System.out.println("are we again in");
					itemName = webElement.getText();
				}
				webElement.click();
			}
			landingPageDomain = getLandingPageDomain(baseDomain, testParameters);
			if (landingPageDomain.equalsIgnoreCase("")){
				Reporter.log("NO DRIVER INSTANCE IS SETUP", true);
			}
			linkClickedSuccessfully = verifyLandingPageDomain(baseDomain, landingPageDomain, baseURL,
					testParameters);
			if (linkClickedSuccessfully){
				domains.put("baseDomain", baseDomain);
				domains.put("landingPageDomain", landingPageDomain);
			}
		} catch (ElementNotVisibleException e) {
			throw e;
		} catch (WebDriverException e) {
			if (testParameters[0].equalsIgnoreCase("Accept Cookies.")) {
				//return true;
			}
			if (e.getMessage().startsWith(
					"unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
		return domains;
	}
	
	//added by Lokesh 7-2015
	public static void click(String locator, String location)
	{
		@SuppressWarnings("unused")
		String itemName = new String();
		try{
			By by = getBy(locator,location);
			WebElement webElement = getInstance().findElement(by);
			if (webElement != null){
				if (webElement.getText() != null){
					itemName = webElement.getText();
				}
				webElement.click();
			
			}
		}catch (ElementNotVisibleException e) {
			throw e;
		} catch (WebDriverException e) {
			if (e.getMessage().startsWith(
					"unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
		
		
	}
	
	public static Hashtable<String, String> click(String locator, String location, String baseURL, String... testParameters)
			throws ElementNotVisibleException, Schneider_IndexOutOfBoundsException {
		@SuppressWarnings("unused")
		boolean linkClickedSuccessfully = true;
		@SuppressWarnings("unused")
		String itemName = new String();
		String landingPageDomain = new String();
		try {
			baseDomain = baseURL.substring(baseURL.indexOf("/") + 2,
					baseURL.indexOf(".") + 1);
			
			By by = getBy(locator, location);
			WebElement webElement = getInstance().findElement(by);
			//System.out.println(webElement.getSize().height+">>"+ webElement.getSize().width+">>"+webElement.isEnabled());

			if (webElement != null){
				if (webElement.getText() != null){
					itemName = webElement.getText();
				}
				
				webElement.click();
				//System.out.println("did it click");
				
			}
		
			landingPageDomain = getLandingPageDomain(baseDomain, testParameters);
			
			if (landingPageDomain.equalsIgnoreCase("")){
				Reporter.log("NO DRIVER INSTANCE IS SETUP", true);
			}
			linkClickedSuccessfully = verifyLandingPageDomain(baseDomain, landingPageDomain, baseURL,
					testParameters);
			
			//if (linkClickedSuccessfully){
				domains.put("baseDomain", baseDomain);
				domains.put("landingPageDomain", landingPageDomain);
			//}
		} catch (ElementNotVisibleException e) {
			System.out.println("does it come here??");
			throw e;
		} catch (WebDriverException e) {
			if (testParameters[0].equalsIgnoreCase("Accept Cookies.")) {
			}
			if (e.getMessage().startsWith(
					"unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
		return domains;
	}
	private static String getLandingPageDomain(String baseDomain,
			String[] testParameters) throws Schneider_IndexOutOfBoundsException {

		String landingPageDomain = new String();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		switchToNewWindow(testParameters[0]);
		timeout(3000,10);
		
		if (getInstance() == null || getInstance().getCurrentUrl() == null){
			return landingPageDomain;
		}
		
//		fnWaitForLoad(getInstance());
//		Driver.getInstance().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		String currentPageURL = getInstance().getCurrentUrl();
		
		
		if (currentPageURL.contains("about:blank")){
			fnWaitForLoad(getInstance());
			currentPageURL = getInstance().getCurrentUrl();
		
		}
		if (currentPageURL.contains("gateway")){
			currentPageURL = testPageURL(currentPageURL);
			currentPageURL = getInstance().getCurrentUrl();
		
		}
		try{
			
			timeout(3000,10); // timeout for page load
			//System.out.println(currentPageURL +" is current page url");
			landingPageDomain = currentPageURL.substring(
					currentPageURL.indexOf("//") + 2,
					currentPageURL.indexOf(".") + 1);
			
		
			if (testParameters[2].contains("Social Media")) {
				currentPageURL = getInstance().getCurrentUrl();
				//System.out.println("currentpageurl >>" + currentPageURL);
				int startOfCurrentPageDomain = currentPageURL.indexOf("//") + 2;
				String tempCurrentPageDomain = currentPageURL
						.substring(startOfCurrentPageDomain);
				int endOfCurrentPageDomain = tempCurrentPageDomain.indexOf("/");
				landingPageDomain = currentPageURL.substring(
						startOfCurrentPageDomain, startOfCurrentPageDomain
						+ endOfCurrentPageDomain);
			}

		
			if (landingPageDomain.contains(baseDomain)) {
				if (!testParameters[2].contains("Social Media")) {
					
					landingPageDomain = baseDomain;
				}
			}
			
			//System.out.println(landingPageDomain);
		}
		catch(StringIndexOutOfBoundsException sIOOBE){
			throw new Schneider_IndexOutOfBoundsException("CurrentPageDomain is "
					+ landingPageDomain);
		}
		return landingPageDomain;
	}
	private static boolean verifyLandingPageDomain(String baseDomain, String landingPageDomain, String baseURL, String... testParameters ) {
		if (baseDomain.contains("www-int1.")) {
			baseDomain = "www-int1.";
		}
		if (baseDomain.contains("www-sqe1.")) {
			baseDomain = "www-sqe1.";
		}		
		if (baseDomain.contains("www-sqewm.")) {
			baseDomain = "www-sqewm.";
		}
		if (baseDomain.contains("www-pre1.")) {
			baseDomain = "www-pre1.";
		}
		if (baseDomain.contains("www.")) {
			baseDomain = "www.";
		}
		boolean landingPageIsValid = true;
		//Driver.chooseWindow(Driver.getCurrentUrl(), baseURL, testParameters);
		//fnSwitchToNewWindow(testParameters[0]);


		if (getInstance() == null){
			Reporter.log(logTime()	+ "  ....DRIVER IS NULL", true);
			if (getInstance().getCurrentUrl() == null){
				Reporter.log(logTime()	+ "  ....CURRENT URL IS NULL", true);
			}
		}
		if (getInstance().getTitle().contains("Maintenance") 
				|| getInstance().getTitle().contains("Timeout")
				|| getInstance().getTitle().contains("Gateway")
				|| getInstance().getTitle().equalsIgnoreCase("error")
				|| getInstance().getTitle().startsWith("Error")
				|| getInstance().getTitle().contains("404 Not Found")
				){
			Reporter.log(logTime()	+ "  ....ERROR! The word \"" + driver.getTitle() + "\" is contained in the page title.", true);
			landingPageIsValid = false;
		}
		if (landingPageIsValid){
			String currentPageURL = getInstance().getCurrentUrl();
			
			currentPageURL = testPageURL(currentPageURL);
			String currentPageDomain = new String();
			try{
				currentPageDomain = getCurrentPageDomain(currentPageURL);
				
				if (testParameters[2].contains("Social Media")) {
					
					currentPageURL = getInstance().getCurrentUrl();
					int startOfCurrentPageDomain = currentPageURL.indexOf("//") + 2;
					String tempCurrentPageDomain = currentPageURL
							.substring(startOfCurrentPageDomain);
					int endOfCurrentPageDomain = tempCurrentPageDomain.indexOf("/");
					currentPageDomain = currentPageURL.substring(
							startOfCurrentPageDomain, startOfCurrentPageDomain
							+ endOfCurrentPageDomain);
				}
				
				
				if (currentPageDomain.contains(baseDomain)) {
					
					currentPageDomain = baseDomain;
				}

				String status = !baseDomain.equalsIgnoreCase(currentPageDomain) ? "WARNING!"
						: "INFO!";
				Reporter.log(logTime() + "  ....." 
						+ status
						+ " Base Domain = \"" + baseDomain
						+ "\" . Current page domain = \"" + currentPageDomain + "\"", true);

				if (baseDomain.equalsIgnoreCase(currentPageDomain)
						|| testParameters[2].contains("Social Media") 
						|| currentPageDomain.equalsIgnoreCase("partners.")
						|| currentPageDomain.equalsIgnoreCase("home.")
						) {
					// Scenario 1.0 ... ALL OK, move on with the test.
					Reporter.log(
							logTime()
							+ "  ....."
							+ status
							+ ". NO CHANGE in domain OR SOCIAL MEDIA domain. This is OK, move on with testing ...",
							true);
				} else {
					if (baseDomain.contains("www-int")){
						if (currentPageDomain.contains("www-sqe")
							|| currentPageDomain.contains("www-pre")
							|| currentPageDomain.contains("www-prd")) {
							landingPageIsValid = false;
							Reporter.log(
									logTime()
									+ "  ...."
									+ " ERROR! Switched from " + baseDomain + " to " + 
									currentPageDomain + " - This is not allowed.",true);
						}
						else{
							landingPageIsValid = true;
						}
					}
					if (baseDomain.contains("www-sqe")){
						if (currentPageDomain.contains("www-int")
							|| currentPageDomain.contains("www-pre")
							|| currentPageDomain.contains("www-prd")) {
							landingPageIsValid = false;
							Reporter.log(
									logTime()
									+ "  ...."
									+ " ERROR! Switched from " + baseDomain + " to " + 
									currentPageDomain + " - This is not allowed.",true);
						}
						else{
							landingPageIsValid = true;
						}
					}					
					if (baseDomain.contains("www-pre")){
						if (currentPageDomain.contains("www-int")
							|| currentPageDomain.contains("www-sqe")
							|| currentPageDomain.contains("www-prd")) {
							landingPageIsValid = false;
							Reporter.log(
									logTime()
									+ "  ...."
									+ " ERROR! Switched from " + baseDomain + " to " + 
									currentPageDomain + " - This is not allowed.",true);
						}
						else{
							landingPageIsValid = true;
						}
					}
					if (baseDomain.contains("www-prd")){
						if (currentPageDomain.contains("www-int")
							|| currentPageDomain.contains("www-sqe")
							|| currentPageDomain.contains("www-pre")) {
							landingPageIsValid = false;
							Reporter.log(
									logTime()
									+ "  ...."
									+ " ERROR! Switched from " + baseDomain + " to " + 
									currentPageDomain + " - This is not allowed.",true);
						}
						else{
							landingPageIsValid = true;
						}
					}
					if (baseDomain.contains("www.")){
						if (currentPageDomain.contains("www-int")
							|| currentPageDomain.contains("www-sqe")
							|| currentPageDomain.contains("www-pre")
							|| currentPageDomain.contains("www-prd")) {
							landingPageIsValid = false;
							Reporter.log(
									logTime()
									+ "  ...."
									+ " ERROR! Switched from " + baseDomain + " to " + 
									currentPageDomain + " - This is not allowed.",true);
						}
						else{
							landingPageIsValid = true;
						}
					}
				}
			}
			catch(StringIndexOutOfBoundsException sIOOBE){
				Reporter.log(
						logTime()
						+ " String Index Out of Bounds Exception"
						+ sIOOBE.getMessage()
						+ ", currentPageDomain is "
						+ currentPageDomain
						+ ". Has the page fully loaded?",
						true);
				sIOOBE.printStackTrace();
				landingPageIsValid = false;
			}
		}
	
		return landingPageIsValid;
	}
	private static String getCurrentPageDomain(String currentPageURL) {
		String currentPageDomain;
		currentPageDomain = currentPageURL.substring(currentPageURL.indexOf("//") + 2,	currentPageURL.indexOf(".") + 1);
		return currentPageDomain;
	}
	protected static String testPageURL(String currentPageURL) {
		if (currentPageURL.contains("about:blank") || currentPageURL.contains("gateway")){
			WebDriverWait wait = new WebDriverWait(getInstance(), Integer.valueOf(10));
			wait.until(ExpectedConditions.titleContains("Schneider Electric"));
		}
		currentPageURL = getInstance().getCurrentUrl();
		return currentPageURL;
	}
//	private static boolean checkLandingPageDomain(String baseDomain,String... testParameters) {
//
//		if (baseDomain.contains("int1.")) {
//			baseDomain = "int1.";
//		}
//		if (baseDomain.contains("sqe1.")) {
//			baseDomain = "sqe1.";
//		}		
//		if (baseDomain.contains("sqewm.")) {
//			baseDomain = "sqewm.";
//		}
//		if (baseDomain.contains("pre1.")) {
//			baseDomain = "pre1.";
//		}
//		if (baseDomain.contains("www.")) {
//			baseDomain = "www.";
//		}
//		boolean landingPageIsValid = true;
//		switchToNewWindow(testParameters[0]);
//
//		if (getInstance() == null){
//			Reporter.log("DRIVER IS NULL", true);
//			if (getInstance().getCurrentUrl() == null){
//				Reporter.log("CURRENT URL IS NULL", true);
//			}
//		}
//		String currentPageURL = getInstance().getCurrentUrl();
//		//chooseWindow(currentPageURL, testParameters);
//
//		if (currentPageURL.contains("about:blank")){
//			fnWaitForLoad(getInstance());
//			currentPageURL = getInstance().getCurrentUrl();
//		}
//		String currentPageDomain = new String();
//		try{
//			currentPageDomain = currentPageURL.substring(
//				currentPageURL.indexOf("//") + 2,
//				currentPageURL.indexOf(".") + 1);
//
//		if (testParameters[2].contains("Social Media")) {
//			currentPageURL = getInstance().getCurrentUrl();
//			int startOfCurrentPageDomain = currentPageURL.indexOf("//") + 2;
//			String tempCurrentPageDomain = currentPageURL
//					.substring(startOfCurrentPageDomain);
//			int endOfCurrentPageDomain = tempCurrentPageDomain.indexOf("/");
//			currentPageDomain = currentPageURL.substring(
//					startOfCurrentPageDomain, startOfCurrentPageDomain
//							+ endOfCurrentPageDomain);
//		}
//
//		if (currentPageDomain.contains(baseDomain)) {
//			currentPageDomain = baseDomain;
//		}
//
////		landingPageIsValid = verifyLandingPageDomain(baseDomain,
////				testParameters, landingPageIsValid, currentPageURL,
////				currentPageDomain);
//		}
//		catch(StringIndexOutOfBoundsException sIOOBE){
//			Reporter.log(
//					logTime()
//							+ " String Index Out of Bounds Exception"
//							+ sIOOBE.getMessage()
//							+ ", currentPageDomain is "
//							+ currentPageDomain
//							+ ".",
//					true);
//			landingPageIsValid = false;
//		}
//		return landingPageIsValid;
//	}
	private static void fnWaitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, Integer.valueOf(getTimeOut())); //DH you can play with the time integer
		wait.until(pageLoadCondition);
	}
	public static void switchToNewWindow(String... testParameters) {
		try {
			ArrayList<String> tabNew = new ArrayList<String>(
					getInstance().getWindowHandles());
			int intTotalhandles = tabNew.size();
			getInstance().switchTo().window(tabNew.get((intTotalhandles - 1)));
		} catch (WebDriverException wde) {
			Reporter.log(logTime() + "  ...." 
					+ testParameters[0]
					+ " " + wde.getMessage() + " 3141", true);	
			wde.printStackTrace();
		} catch (Throwable t) {
			Reporter.log(logTime() + "  ...." 
					+ testParameters[0]
					+ " ERROR! Could not switch to the New Window", true);
			Assert.fail("Could not switch to the New Window");
		}
	}

	private static String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	public static void awaitPresenceOfElement(Schneider_PageElement pageElement) {
		By by = getBy(pageElement);
		try{
			WebDriverWait wait = new WebDriverWait(getInstance(), Integer.valueOf(getTimeOut())); //DH you can play with the time integer `
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
		}
		catch (WebDriverException wde){
			Reporter.log(logTime() + "  ....WebDriverException - Time out while "
					+ "checking for a link with locator path "
					+ pageElement.getLink()
					, true);
		}
	}
	public static void awaitPresenceOfElement(By by, WebElement webElement) {
		try{

			WebDriverWait wait = new WebDriverWait(driver, Integer.valueOf(getTimeOut())); //DH you can play with the time integer `
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
		}
		catch (WebDriverException wde){
			Reporter.log(logTime() + "  ....WebDriverException - Time out while "
					+ "checking for a link "
					+ "href = " + webElement.getAttribute("href")
					+ " to country \"" + webElement.getText() + "\""
					, true);
		}
	}	
	
	public static boolean isElementAvailable(String locator, String location)
	{	
		boolean elemPresent = false;
		
		if (locator.equalsIgnoreCase("id")){
			if(driver.findElements(By.id(location)).size() != 0)
				elemPresent=true;
		}
		else if (locator.equalsIgnoreCase("xPath")){
			if(driver.findElements(By.xpath(location)).size() != 0)
				elemPresent=true;
		}
		else if (locator.equalsIgnoreCase("linkText")){
			if(driver.findElements(By.linkText(location)).size() != 0)
				elemPresent=true;
		}
		else if (locator.equalsIgnoreCase("cssSelector")){
			if(driver.findElements(By.cssSelector(location)).size() != 0)
				elemPresent=true;
		}
		else if (locator.equalsIgnoreCase("name")){
			if(driver.findElements(By.name(location)).size() != 0)
				elemPresent=true;
		}
		return elemPresent;
	}
	
	public static boolean isElementPresent(String locator, 	String location) {
			By by = getBy(locator, location);
			return getInstance().findElement(by).isEnabled();
	}	
	
	public static boolean isElementPresent(String Xpath) {
		WebElement webElement = findElement(getInstance(), By.xpath(Xpath), Integer.valueOf(getTimeOut())); //DH you can play with the time integer 
		return webElement.isEnabled();
	}
	public static boolean isElementPresent(By by, int limit) {
		try {
			WebElement webElement = findElement(getInstance(), by, limit);
			return webElement.isEnabled() && webElement.isDisplayed();
		} catch (TimeoutException nsee) {
			return false;
		}
	}
	public static WebElement findElement(final WebDriver driver,
			final By locator, final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(300, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return driver.findElement(locator);
			}
		});
	}
	public static WebElement findElement(final By locator, final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getInstance())
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(300, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return getInstance().findElement(locator);
			}
		});
	}
	public static String getPageTitle() {
		return getInstance().getTitle();
	}
	public static String getCurrentUrl() {
		return getInstance().getCurrentUrl();
	}
	public static void chooseWindow(String testURL, String baseURL, String... testParameters) {
		ArrayList<String> tabBase = new ArrayList<String> (getInstance().getWindowHandles());
		 if (tabBase.size() > 1){
			 switchtoMainWindow(baseURL);
		 }
		 else{
			switchToTestWindow(testURL, testParameters);
		 }
	}
	public static void switchtoMainWindow(String baseURL){
		try{
			 ArrayList<String> tabBase = new ArrayList<String> (getInstance().getWindowHandles());
			 int intotalhandles = tabBase.size();
			 for(int i=intotalhandles;i!=1;i--){
				 getInstance().switchTo().window(tabBase.get(i-1));
				 Thread.sleep(100);
				 getInstance().close();
				
			 }
			 getInstance().switchTo().window(tabBase.get(0));
			 
			
			 
		}catch(NoSuchWindowException nswe){
			switchtoMainWindow(baseURL);
			getInstance().switchTo().window(baseURL);
		}catch(Exception e){
			Reporter.log("Could not switch back to base Window", true);
		}
	}
	public static void switchToTestWindow(String currentURL, String... testParameters) {
		getInstance().get(currentURL);
	}
	public static List<WebElement> findElements(String locator, String location) {
		By by = getBy(locator, location);
		return getInstance().findElements(by);
	}
	public static void navigateTo(String currentURL) {
		getInstance().navigate().to(currentURL);
	}
	public static WebElement findElement(String locator,
			String location, int timeoutSeconds) {
		By by = getBy(locator, location);
		return findElement(getInstance(), by, timeoutSeconds);
	}
	public static void getBaseUrl(String baseURL) {
		getInstance().get(baseURL);
	}
	

	public static boolean checkCanonicalURL(String currentURL, String... testParameters)
			throws UnsupportedEncodingException {
		boolean canonicalURLExists = true;
		if (currentURL.contains(".page")){
			Reporter.log(logTime() + "  .....Interwoven page, no Canonical url required.", true);
		}
		else{
			try{
				canonicalURLExists = Driver.isElementPresent(Constants.WebMachine_CommonConstants.CanonicalURL_Check);
				WebElement canonicalURL = Driver.findElement("xPath", Constants.WebMachine_CommonConstants.CanonicalURL_Check, 15);
				Reporter.log(logTime() + "  .....Canonical Url: "+ canonicalURL.getAttribute("href"), true);
			}
			catch (TimeoutException toe) {
				canonicalURLExists = false;
				checkForMaintenanceMessage();			
				Reporter.log(logTime() + "  .....ERROR! Time out searching for Canonical URL for page: "
						+ Driver.getPageTitle(), true);
			}	
			checkForMaintenanceMessage();	
		}
		return canonicalURLExists;
	}

	private static void checkForMaintenanceMessage() {
		String pageTitle = Driver.getPageTitle();
		if (pageTitle.contains("Maintenance Message")){
			throw new Schneider_TimeoutException(pageTitle);
		}
	}
	public static boolean sendKeys(String locator, String location, int timeoutSeconds, String keys) {
		WebElement inputBox = Driver.findElement(locator, location, timeoutSeconds);
		if (inputBox.isEnabled()){
			inputBox.sendKeys(keys);
			return true;
		}
		return false;
	}
	public static String findElementAttribute(String locator, String location, int timeoutSeconds, String attribute) {
		String attributeValue = null;
		WebElement webElement = Driver.findElement(locator, location, timeoutSeconds);
		if (webElement.isEnabled()){
			if (attribute.equalsIgnoreCase("text")){
				attributeValue = webElement.getText();
			}
			if (webElement.getAttribute(attribute) != null && webElement.getAttribute(attribute).length() > 0){
				attributeValue = webElement.getAttribute(attribute);
			}
		}
		return attributeValue;
	}
	public static List<WebElement> enumerateElements(String locator, String location, String baseURL, String... testParameters) {
			List<WebElement> webElements = new ArrayList<WebElement> ();
			try{
				Driver.chooseWindow(Driver.getCurrentUrl(), baseURL, testParameters);
				webElements = findElements(locator, location);
//				System.out.println("webelems size is "+webElements.size());
				checkForMaintenanceMessage();
			}catch(TimeoutException toe){
				if (getInstance().getTitle().contains("Maintenance Message")){
					throw new Schneider_TimeoutException(getInstance().getTitle());
				}
				throw toe;
			}catch(Throwable tw){
				throw tw;
			}
			return webElements;	
	}
	protected static List<WebElement> getGeographyElements(String locator, String location) {
		List<WebElement> webElements = findElements(locator, location);
		return webElements;
	}
	public static String startOfTest(String purpose, String... testParameters) {
		String start = logTime() + " " + testParameters[0]; 
		String summary = " \"" + testParameters[1] + "\" \"" 
				+ testParameters[2] + "\" " 
				+ purpose + "";
		Reporter.log(start + " START " + summary , true);
		return summary;
	}
	public static void endOfTest(String summary, boolean testOutcome,
			String... testParameters) {
		String status = testOutcome == true ? "PASSED" : "FAILED";
		Reporter.log(logTime() + " " + testParameters[0] + " END. " + status + ": " + 
		summary.substring(summary.indexOf(testParameters[1])), true);
	}
	public static void tearDown() {
		getInstance().close();
		getInstance().quit();
	}
	public static String getLinkIdentifier(String linkIdentifier) {
		WebElement webElementLink = getInstance().findElement(By.xpath(linkIdentifier));
		
		linkIdentifier = webElementLink.getAttribute("href".trim());
		
		if (linkIdentifier.length() > 0) 
			return linkIdentifier;
		
		else
			return linkIdentifier="Social Media Text not found"; 
/*		linkIdentifier = webElementLink.getAttribute("title").trim().length() > 0 ? webElementLink.getAttribute("title").trim() : webElementLink.getText().trim();
		if (linkIdentifier.length() > 0) {
		} 
		else {
			String hReflink = webElementLink.getAttribute("href");
			if (hReflink != null){
				int firstIndex = hReflink.contains("www") ? webElementLink
						.getAttribute("href").indexOf(".") : webElementLink
						.getAttribute("href").indexOf("/") + 1;
				String hReflinkRemainder = webElementLink.getAttribute("href")
						.substring(firstIndex + 1);
				int secondIndex = hReflinkRemainder.indexOf(".");
				linkIdentifier = webElementLink.getAttribute("href").substring(
						firstIndex + 1, firstIndex + secondIndex + 1);
			}
		}*/
		//return linkIdentifier;
	}
	public static Hashtable<String,String> click(WebElement webElement, String... testParameters) throws Schneider_IndexOutOfBoundsException {
		try {
			webElement.click();
			baseDomain = getBaseDomain(testParameters[4]);
			landingPageDomain = getLandingPageDomain(baseDomain, testParameters);
			if (verifyLandingPageDomain(testParameters[4], landingPageDomain, testParameters[4], testParameters)){
				if (testParameters[2].equalsIgnoreCase("Social Media")){
					domains.put("socialMedia", testParameters[2]);
				}
				domains.put("baseDomain", baseDomain);
				domains.put("landingPageDomain", landingPageDomain);
			}
		} catch (Schneider_IndexOutOfBoundsException e) {
			throw e;
		}
		return domains;
	}
	private static String getBaseDomain(String baseDomain) {
		if (baseDomain.contains("int1.")) {
			baseDomain = "int1.";
		}
		if (baseDomain.contains("sqe1.")) {
			baseDomain = "sqe1.";
		}		
		if (baseDomain.contains("sqewm.")) {
			baseDomain = "sqewm.";
		}
		if (baseDomain.contains("pre1.")) {
			baseDomain = "pre1.";
		}
		if (baseDomain.contains("www.")) {
			baseDomain = "www.";
		}
		return baseDomain;
	}
	public static Set<String> getWindowHandles() {
		return getInstance().getWindowHandles();
	}
	
	// wait for page load
	public static boolean waitForPageLoad(WebDriver driver, int waitTimeInSec, String title) {
	    boolean isLoaded = false;
	    WebDriverWait wait = new WebDriverWait(driver, waitTimeInSec);
	    

	    isLoaded = wait.until(ExpectedConditions.titleContains(title));  //"Schneider"

	    return isLoaded;
	}

	// wait for page load			---------------- wait time suppose to be long data
	public static boolean waitForPageLoadWithElement(WebDriver driver, int waitTimeInSec,String locator,  String elementLocation) {
	    boolean isLoaded = true;
	    WebDriverWait wait = new WebDriverWait(driver, waitTimeInSec);
	    
	    By by = getBy(locator,elementLocation);
	    isLoaded = wait.until(ExpectedConditions.invisibilityOfElementLocated(by));  //"Schneider"

	    return !isLoaded;
	}
	
	public static void timeout(){
		try {
			Thread.sleep(1000);
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void timeout(int milliseconds, int seconds){
		try {
			Thread.sleep(milliseconds);
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}


}
