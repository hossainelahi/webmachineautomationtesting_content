package com.SE.WebMachine.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.tools.Driver;

public class WebMachine_HomePage extends WebMachine_Base_Page{

	public static void initializeHome() {

		StickyBars.initialize();
	}


	public static StickyBars getStickyBars() {
		return StickyBars.getInstance();
	}



	


	// verification of Tips (Need Inspiration)

	public static boolean validateTips(String... testParameters)
	{
		boolean tipsValidated = true;
		String titleAttribute = new String();
		String hrefAttribute = new String();
		String description = new String();

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it
		try{
			String xpathToBeUsed ="//*[@class='tips-section']/li"; 
			//Constants.WebMachine_CommonConstants.rotatingCarousel;

			List<WebElement> tipsCount = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);

			System.out.println("total number of tips are : "+tipsCount.size());

			if(tipsCount.size()>0)
			{
				tipsValContinue :
					for(int i=1;i<=tipsCount.size();i++)
					{
						titleAttribute = Driver.findElement("xPath", "//*[@class='tips-section']/li"+"[" + i +"]" + "/a/h3", 15).getText();
						hrefAttribute = Driver.findElementAttribute("xPath", "//*[@class='tips-section']/li"+"[" + i +"]" + "/a", 15, "href");
						description = Driver.findElement("xPath", "//*[@class='tips-section']/li"+"[" + i +"]" + "/a/span", 15).getText();


						Reporter.log(logTime() + "  ....Element : " + titleAttribute + " | Description : " + description +" | Link : " + hrefAttribute, true);

						//						boolean elemPresent = Driver.isElementPresent("//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a");
						//						System.out.println(elemPresent + " ..... >>> ");

						try{
							//							Driver.click("xPath", "//*[@for='support-bar-toggle']");
							//							System.out.println("element clicked");

							//							String activeClassAttr = Driver.findElementAttribute("xPath", "//*[@id='support-bar']", 15, "class");
							//
							//							if("active".equalsIgnoreCase(activeClassAttr))
							//							{


							domains = Driver.click("xPath", "//*[@class='tips-section']/li"+"[" + i +"]" + "/a", testParameters[3], testParameters);

							Thread.sleep(5000);

							//	Driver.waitForPageLoad(Driver.getInstance(), 20, title)
							String afterClickUrl = Driver.getCurrentUrl();


							Thread.sleep(2000);


							Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttribute, true);

							if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
							{
								if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
								{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
									if (checkDomains())
										Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
									else{
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										//footerMainValidated = false;
									}
								}
								else{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
									Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
									//footerMainValidated = false;
								}
							}
							else{
								Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
								tipsValidated = false;
							}

							Driver.navigateTo(testParameters[3]);
							//	Driver.click("xPath", "//*[@for='support-bar-toggle']");
							//							}else
							//							{
							//								Reporter.log(logTime() + "  ....Could not click on the Support Bar Toggle", true);
							//								tipsValidated = false;
							//							}
						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue tipsValContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue tipsValContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
							e.printStackTrace();
							continue tipsValContinue;
						}
						catch (InterruptedException e) {

							e.printStackTrace();
						}

					}

			}

		}catch (StaleElementReferenceException sere) {
			Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
		}
		catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

		} catch (ElementNotVisibleException e) {

			e.printStackTrace();
		} 

		return tipsValidated;
	}


	// verification of Today's pick 

	public static boolean validatePicks(String... testParameters)
	{
		boolean picksValidated = true;
		String titleAttribute = new String();
		String hrefAttribute = new String();
		String description = new String();
	//	boolean elemPresent = false;
		ArrayList<String> tabNew;

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it
		try{
			String xpathToBeUsed ="//*[@class='picks'] //*[@class='tiles']/li"; 
			//Constants.WebMachine_CommonConstants.rotatingCarousel;

			List<WebElement> picksCount = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);

			System.out.println("total number of Today's picks are : "+picksCount.size());

			if(picksCount.size()>0)
			{
				picksValContinue :
					for(int i=1;i<=picksCount.size();i++)
					{
						try{
							int descendantPresent = Driver.findElements("xPath", xpathToBeUsed+"[" + i +"]" + "[descendant::a]").size();
							
							if(descendantPresent>0)
							
							{
								titleAttribute = Driver.findElement("xPath", xpathToBeUsed+"[" + i +"]" + "/a //em", 15).getText();
								hrefAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[" + i +"]" + "/a", 15, "href");

								Reporter.log(logTime() + "  ....Element " + i + " : " + titleAttribute +" | Link : " + hrefAttribute, true);				

								domains = Driver.click("xPath", xpathToBeUsed+"[" + i +"]" + "[descendant::a]", testParameters[3], testParameters);
								
								Thread.sleep(5000);
								//System.out.println("is it clicked "+Driver.getCurrentUrl());

								tabNew = new ArrayList<String>(
										Driver.getInstance().getWindowHandles());

								// if there are more windows then go inside if
								if(tabNew.size()>1)
								{
//									System.out.println("inside if");
									Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

									String tabSwitchUrl = Driver.getCurrentUrl();

									Reporter.log(logTime() + "  ....Current URL : " + tabSwitchUrl + " | href url : " + hrefAttribute, true);

									if((hrefAttribute.length()!=0) && (tabSwitchUrl.length() !=0))
									{
										if((hrefAttribute.contains(tabSwitchUrl)) || (tabSwitchUrl.contains(hrefAttribute)) )
										{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
											if (checkDomains())
												Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
											else{
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
												//footerLinksValidated = false;
											}
										}
										else{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerLinksValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
										picksValidated = false;
									}

									Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
									Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

								}else{   // of tabnew size
									
//									System.out.println("inside else of tab new");
									Driver.waitForPageLoad(Driver.getInstance(), 20, "Schneider");
									String afterClickUrl = Driver.getCurrentUrl();


									Thread.sleep(2000);


									Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttribute, true);

									if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
									{
										if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
										{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
											if (checkDomains())
												Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
											else{
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
												//footerMainValidated = false;
											}
										}
										else{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerMainValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
										picksValidated = false;
									}

									Driver.navigateTo(testParameters[3]);
									Thread.sleep(3000);
								}
							} // if elem present ends here
							else
							{
								Reporter.log(logTime() + "  ....Could not click on the Element " + i + " , Please check manually." , true);
								continue picksValContinue;
							}

						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue picksValContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue picksValContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
							e.printStackTrace();
							continue picksValContinue;
						}
						catch (InterruptedException e) {

							e.printStackTrace();
						}


						
					}		// end of for loop

			}

		}catch (StaleElementReferenceException sere) {
			Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
		}
		catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

		} catch (ElementNotVisibleException e) {

			e.printStackTrace();
		} 

		return picksValidated;
	}


}
