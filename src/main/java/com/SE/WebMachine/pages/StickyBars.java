package com.SE.WebMachine.pages;

import java.util.List;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.tools.Driver;
//import com.SE.WebMachine.util.Constants;

public class StickyBars extends WebMachine_Base_Page{
	public static StickyBars stickyBars;

	public static void initialize() {
		stickyBars = new StickyBars();
	}

	public static StickyBars getInstance() {
		return stickyBars;
	}


	public static boolean validateStickyBars(String... testParameters)
	{
		boolean stickyBarsValidated = true;
		String titleAttribute = new String();
		String hrefAttribute = new String();

		//		System.out.println(testParameters[0] + " >> " +testParameters[1]+ " >> " +testParameters[2]+ " >> "  +testParameters[3]);
		//		String xpathToBeUsed = Constants.WebMachine_CommonConstants.rotatingCarousel;

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it


		try{
			if(Driver.isElementAvailable("xPath","//*[@for='support-bar-toggle']"))
			{
				Driver.click("xPath", "//*[@for='support-bar-toggle']");
				//			System.out.println("support bar clicked");

				if(Driver.isElementPresent("//div[@id='support-bar-icons'] //li"))
				{

					List<WebElement> stickyBarsCount = Driver.enumerateElements("xPath", "//div[@id='support-bar-icons'] //li", testParameters[3], testParameters);

					//			System.out.println(stickyBarsCount.size() + " is the count on stickyBars section");
					Reporter.log(logTime() + "....Total number of Links in Sticky Bars are :  " + stickyBarsCount.size(), true);

					if(stickyBarsCount.size()>0)
					{
						stickyBarsValContinue :
							for(int i=1;i<=stickyBarsCount.size();i++)
							{

								titleAttribute = Driver.findElementAttribute("xPath", "//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a", 15,"title");
								hrefAttribute = Driver.findElementAttribute("xPath", "//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a", 15, "href");


								Reporter.log(logTime() + "  ....Element : " + titleAttribute + " | Link : " + hrefAttribute, true);

//								boolean elemPresent = Driver.isElementPresent("//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a");
								//						System.out.println(elemPresent + " ..... >>> ");
								
								
								try {
									
									String activeClassAttr = Driver.findElementAttribute("xPath", "//*[@id='support-bar']", 15, "class");

									if("active".equalsIgnoreCase(activeClassAttr))
									{

										domains = Driver.click("xPath", "//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a", testParameters[3], testParameters);

										Driver.timeout();
										//	Driver.waitForPageLoad(Driver.getInstance(), 20, title)
										String afterClickUrl = Driver.getCurrentUrl();


										Thread.sleep(2000);


										Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttribute, true);

										if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
										{
											if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
											{
												Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
												if (checkDomains())
													Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
												else{
													Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
													//footerMainValidated = false;
												}
											}
											else{
												Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
												//footerMainValidated = false;
											}
										}
										else{
											Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
											stickyBarsValidated = false;
										}

										Driver.navigateTo(testParameters[3]);
										//Thread.sleep(3000);
										Driver.timeout();
										Driver.click("xPath", "//*[@for='support-bar-toggle']");
									}else
									{
										Driver.click("xPath", "//*[@for='support-bar-toggle']");
										domains = Driver.click("xPath", "//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a", testParameters[3], testParameters);

										Driver.timeout();
										//	Driver.waitForPageLoad(Driver.getInstance(), 20, title)
										String afterClickUrl = Driver.getCurrentUrl();


										Thread.sleep(2000);


										Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttribute, true);

										if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
										{
											if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
											{
												Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
												if (checkDomains())
													Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
												else{
													Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
													//footerMainValidated = false;
												}
											}
											else{
												Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
												//footerMainValidated = false;
											}
										}
										else{
											Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
											stickyBarsValidated = false;
										}

										Driver.navigateTo(testParameters[3]);
										//Thread.sleep(3000);
										Driver.timeout();
										Driver.click("xPath", "//*[@for='support-bar-toggle']");
									}
								}catch (NoSuchElementException nsee) {

									Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
									continue stickyBarsValContinue;

								} catch (ElementNotVisibleException e) {
									Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
									e.printStackTrace();
									continue stickyBarsValContinue;

								} catch (Schneider_IndexOutOfBoundsException e) {
									Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
									e.printStackTrace();
									continue stickyBarsValContinue;
								}
								catch (InterruptedException e) {

									e.printStackTrace();
								}
							}  // end of for loop


					}else{
						Reporter.log(logTime() + "  ....Support Bar Icon List is found blank", true);
						stickyBarsValidated = false;
					}
				}else{
					Reporter.log(logTime() + "  ....Support Bar Icon List is found blank", true);
					stickyBarsValidated = false;
				}
			}else
			{
				Reporter.log(logTime() + "  ....Support Bar is not found", true);
				stickyBarsValidated = false;
			}

		}catch (StaleElementReferenceException sere) {
			Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
		}
		catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

		} catch (ElementNotVisibleException e) {

			e.printStackTrace();
		} 		

		return stickyBarsValidated;
	}

}