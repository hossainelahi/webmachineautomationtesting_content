package com.SE.WebMachine.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.Validate;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;

public class WebMachine_WorkPage extends WebMachine_Base_Page{

	public static void initializeWork() {

		StickyBars.initialize();
	}


	public static StickyBars getStickyBars() {
		return StickyBars.getInstance();
	}


	// verification of Solution Boxes and solution items along with Alternative solutions

	public static boolean validateSolutionItems(String... testParameters)
	{
		boolean solutionItemsValidated = true;
		String titleAttribute = new String();
		//	String hrefAttribute = new String();
		String classAttribute = new String();
		//String description = new String();

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it
		try{
			String xpathToBeUsed = "//*[@class='tiles']/li";
			String xpathExtension = "//*[contains(@class,'picks-questions')]";
			//"//*[contains(@class,'picks-questions')]";
			//Constants.WebMachine_WorkPage.solutionItemBoxes; 
			//Constants.WebMachine_CommonConstants.rotatingCarousel;

			List<WebElement> solutionBoxCount = Driver.enumerateElements("xPath", xpathToBeUsed+xpathExtension, testParameters[3], testParameters);

			System.out.println("total number of Solution Item Boxes are : "+solutionBoxCount.size());

			if(solutionBoxCount.size()>0)
			{
				solutionBoxContinue :
					for(int i=1;i<=solutionBoxCount.size();i++)
					{
						try{
							titleAttribute = Driver.findElement("xPath", xpathToBeUsed +"[" + i +"]" + xpathExtension + "/h3", 15).getText();

							classAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[" + i +"]" + xpathExtension,15,"class");

							Reporter.log(logTime() + "  ....Element : " + titleAttribute, true);

							//					if(classAttribute.contains("alt"))   // if class attr is present then main solution items else alternative solution items
							//					{
							//						System.out.println("inside if I am");
							//					}
							//					else{
							//						System.out.println("inside else bhai");
							validateSolutions(xpathToBeUsed+"[" + i +"]" + xpathExtension, testParameters);  // validation of Solution items

							//					}
						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue solutionBoxContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue solutionBoxContinue;

						}

					}

			} // if ends here solutionBox count

		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionBoxContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionBoxContinue;

		}catch(NullPointerException npe){
			Reporter.log(logTime() + "  ....Null pointer exception message is " + npe.getMessage(), true);
			npe.printStackTrace();
		}

		return solutionItemsValidated;

	} // end of method


	// validation of Solution Items
	public static void validateSolutions(String xpathToBeUsedSolutionItems, String... testParameters)
	{
		String titleAttributeSolutions = new String();
		String hrefAttributeSolutions = new String();

		String xpathInUseSolutionItems = xpathToBeUsedSolutionItems + " //li";

		try{
			List<WebElement> solutionItemCount = Driver.enumerateElements("xPath", xpathInUseSolutionItems, testParameters[3], testParameters);
			System.out.println("total number of Solution Items are : "+solutionItemCount.size());

			if(solutionItemCount.size()>0)
			{
				solutionItemContinue :
					for(int k =1;k<=solutionItemCount.size();k++)
					{
						try{
							titleAttributeSolutions = Driver.findElement("xPath", xpathInUseSolutionItems +"[" + k +"]" +"/a", 15).getText();
							hrefAttributeSolutions = Driver.findElementAttribute("xPath", xpathInUseSolutionItems +"[" + k +"]" +"/a", 15, "href");

							Reporter.log(logTime() + "  ....Element : " + titleAttributeSolutions + "  |  Link : "+ hrefAttributeSolutions, true);

							domains = Driver.click("xPath", xpathInUseSolutionItems+"["+k+"]" +"/a", testParameters[3], testParameters);


							Driver.waitForPageLoad(Driver.getInstance(), 20, "Schneider");
							String afterClickUrl = Driver.getCurrentUrl();

//							System.out.println(afterClickUrl);

							if((hrefAttributeSolutions.length()!=0) && (afterClickUrl.length() !=0))
							{
								if((hrefAttributeSolutions.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeSolutions)) )
								{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeSolutions +"' is redirecting to correct URL, Hence Verified", true);
									if (checkDomains()){
										Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeSolutions, true);
										Reporter.log(logTime() + "  ....Checking Tip boxes on Solutions' Page ", true);
										validateTipBoxes(testParameters);  // to validate tip boxes
										//*[@class='main'] //*[@class='tips-section sub-ul']
									}
									else{
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

									}
								}
								else{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeSolutions +"' is not redirecting to correct URL, Hence not Verified", true);
									Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

								}
							}else{
								Reporter.log(logTime() + "  ...." + "Link "+titleAttributeSolutions+ " is blank or broken", true);

							}




							Driver.navigateTo(testParameters[3]);    // navigating to Work page

						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue solutionItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue solutionItemContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
							e.printStackTrace();
						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}
	}


	// validating tip boxes in solutions' page

	public static boolean validateTipBoxes(String...testParameters)
	{
		boolean validateTipBoxes = true;
		String titleAttributeTip = new String();
		//	String hrefAttribute = new String();
		String xpathToBeUsedTip = "//*[@class='main'] //*[@class='tips-section sub-ul']";
		String xpathInUseTip = "//li[not(@class)]";
		//	String xpathInUse = xpathToBeUsed + " //li";

		try{
			List<WebElement> solutionItemCount = Driver.enumerateElements("xPath", xpathToBeUsedTip, testParameters[3], testParameters);
			System.out.println("total number of Tip Box lists in Solutions' Page are : "+solutionItemCount.size());

			if(solutionItemCount.size()>0)
			{
				solutionItemContinue :
					for(int l =1;l <=solutionItemCount.size();l++)
					{
						try{

							List<WebElement> tipBoxCount = Driver.enumerateElements("xPath", xpathToBeUsedTip+"[" + l +"]"+xpathInUseTip, testParameters[3], testParameters);
							System.out.println("total number of Tip Boxes in Solutions' Page for List : "+ l + " are : "+tipBoxCount.size());

							if(tipBoxCount.size()>0){
								tipBoxContinue :
									for(int j =1;j <=tipBoxCount.size();j++)
									{
										try{
											titleAttributeTip = Driver.findElement("xPath", xpathToBeUsedTip+"[" + l +"]"+ xpathInUseTip+ "[" + j +"]" +"/h3", 15).getText();

											Reporter.log(logTime() + "  ....Element : " + titleAttributeTip , true);

											if((!("".equalsIgnoreCase(titleAttributeTip))) || (titleAttributeTip.isEmpty())){

//												System.out.println("title attri is not empty check for tip box as well");
												boolean isButtonPresent = Driver.isElementPresent(xpathToBeUsedTip+"[" + l +"]"+ xpathInUseTip+ "[" + j +"]" +" //a");
												String buttonName = Driver.findElement("xPath", xpathToBeUsedTip+"[" + l +"]"+ xpathInUseTip+ "[" + j +"]" +" //a", 15).getText();
												if(isButtonPresent)
													System.out.println("button is present and title is  :"+buttonName);
												else{ 
													System.out.println("button is not present");
													validateTipBoxes = false;
												}
											}
											else{
												System.out.println("Box is blank, hence no check is performed");
												validateTipBoxes = false;
											}
										}catch (NoSuchElementException nsee) {

											Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
											continue tipBoxContinue;

										} catch (ElementNotVisibleException e) {
											Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
											e.printStackTrace();
											continue tipBoxContinue;

										}
									}
							}

						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue solutionItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue solutionItemContinue;

						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}
		return validateTipBoxes;

	}
	/*

							//						boolean elemPresent = Driver.isElementPresent("//div[@id='support-bar-icons'] //li"+"[" + i +"]" + "/a");
							//						System.out.println(elemPresent + " ..... >>> ");

							try{
								//							Driver.click("xPath", "//*[@for='support-bar-toggle']");
								//							System.out.println("element clicked");

								//							String activeClassAttr = Driver.findElementAttribute("xPath", "//*[@id='support-bar']", 15, "class");
								//
								//							if("active".equalsIgnoreCase(activeClassAttr))
								//							{


								domains = Driver.click("xPath", "//*[@class='tips-section']/li"+"[" + i +"]" + "/a", testParameters[3], testParameters);

								Thread.sleep(5000);

								//	Driver.waitForPageLoad(Driver.getInstance(), 20, title)
								String afterClickUrl = Driver.getCurrentUrl();


								Thread.sleep(2000);


								Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttribute, true);

								if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
								{
									if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
									{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
										if (checkDomains())
											Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
										else{
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerMainValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										//footerMainValidated = false;
									}
								}
								else{
									Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
									solutionItemsValidated = false;
								}

								Driver.navigateTo(testParameters[3]);
								//	Driver.click("xPath", "//*[@for='support-bar-toggle']");
								//							}else
								//							{
								//								Reporter.log(logTime() + "  ....Could not click on the Support Bar Toggle", true);
								//								tilesValidated = false;
								//							}
							}catch (NoSuchElementException nsee) {

								Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
								continue tilesValContinue;

							} catch (ElementNotVisibleException e) {
								Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
								e.printStackTrace();
								continue tilesValContinue;

							} catch (Schneider_IndexOutOfBoundsException e) {
								Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
								e.printStackTrace();
								continue tilesValContinue;
							}
							catch (InterruptedException e) {

								e.printStackTrace();
							}

						}

				}

			}catch (StaleElementReferenceException sere) {
				Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
			}
			catch (NoSuchElementException nsee) {

				Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

			} catch (ElementNotVisibleException e) {

				e.printStackTrace();
			} 

			return solutionItemsValidated;
		}*/

	// validation of Tiles and insights if present

	public static boolean validateTiles(String...testParameters)
	{
		boolean tilesValidated = true;
		String titleAttributeTiles = new String();
		String hrefAttributeTiles = new String();


		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it


		try{  // need to change all logic inside try

			String xpathToBeUsed = "//*[@class='main']/*[@class='picks']/*[@class='tiles']/li[descendant ::em]";
			String xpathExtension = "/a";
			//"//*[contains(@class,'picks-questions')]";
			//Constants.WebMachine_WorkPage.solutionItemBoxes; 
			//Constants.WebMachine_CommonConstants.rotatingCarousel;

			List<WebElement> tilesCount = Driver.enumerateElements("xPath", xpathToBeUsed+xpathExtension, testParameters[3], testParameters);

			System.out.println("total number of Tiles are : "+tilesCount.size());

			if(tilesCount.size()>0)
			{
				tileItemContinue :
					for(int k =1;k<=tilesCount.size();k++)
					{
						try{
							titleAttributeTiles = Driver.findElement("xPath", xpathToBeUsed +"[" + k +"]" + xpathExtension + " //em", 15).getText();
							hrefAttributeTiles = Driver.findElementAttribute("xPath", xpathToBeUsed +"[" + k +"]" +xpathExtension, 15, "href");

							Reporter.log(logTime() + "  ....Element : " + titleAttributeTiles + "  |  Link : "+ hrefAttributeTiles, true);

							//						System.out.println( xpathToBeUsed +"[" + k +"]" +" //img[@alt='Default Alternative Text']");

							domains = Driver.click("xPath", xpathToBeUsed+"["+k+"]" + " //img", testParameters[3], testParameters);

							//[@alt='Default Alternative Text']
							//try {
							Thread.sleep(2000);

//							System.out.println("clicked on tile link");

							ArrayList<String> tabNew = new ArrayList<String>(
									Driver.getInstance().getWindowHandles());		// number of tabs in arraylist and then if size is >1 then if otherwise else

							if(tabNew.size()>1)
							{

								Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

								//Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
								String tabSwitchUrl = Driver.getCurrentUrl();

								if((hrefAttributeTiles.length()!=0) && (tabSwitchUrl.length() !=0))
								{
									if((hrefAttributeTiles.contains(tabSwitchUrl)) || (tabSwitchUrl.contains(hrefAttributeTiles)) )
									{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is redirecting to correct URL, Hence Verified", true);
										if (checkDomains())
											Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeTiles, true);
										else{
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerLinksValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is not redirecting to correct URL, Hence not Verified", true);
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										//footerLinksValidated = false;
									}
								}
								else{
									Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttributeTiles, true);
									tilesValidated = false;
								}

								Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
								Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

							}else{

								Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
								String afterClickUrl = Driver.getCurrentUrl();

								//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

//								System.out.println(afterClickUrl);

								if((hrefAttributeTiles.length()!=0) && (afterClickUrl.length() !=0))
								{
									if((hrefAttributeTiles.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeTiles)) )
									{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is redirecting to correct URL, Hence Verified", true);
										if (checkDomains()){
											Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeTiles, true);

										}
										else{
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

										}
									}
									else{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is not redirecting to correct URL, Hence not Verified", true);
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										tilesValidated = false;

									}
								}else{
									Reporter.log(logTime() + "  ...." + "Link "+titleAttributeTiles+ " is blank or broken", true);

								}

								Driver.navigateTo(testParameters[3]);    // navigating to Work page
							}

						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue tileItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue tileItemContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
							e.printStackTrace();
						}catch (InterruptedException e) {
							e.printStackTrace();
						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}
		//Driver.isElementPresent(Xpath)
		
		// insights are present if elempres = true
		boolean elemPres = Driver.isElementAvailable("xPath","//*[@class='multiple-picks'] //*[@class='tiles']/li");
//		System.out.println(elemPres);
		
		if(elemPres)
			validateTilesInsights(testParameters);
		else
			Reporter.log(logTime() + "  ....Insights are not found on this page ", true);

		return tilesValidated;
	}

	// validation of Insight Items
	public static boolean validateTilesInsights(String... testParameters)
	{
		boolean insightTileValidated = true;
		String titleAttributeInsights = new String();
		String hrefAttributeInsights = new String();
		String xpathToBeUsed = new String();

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it
		//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);
		//String xpathInUseSolutionItems = xpathToBeUsedSolutionItems + " //li";

		try{
			xpathToBeUsed=	"//*[@class='multiple-picks'] //*[@class='tiles']/li";
			List<WebElement> insightItemCount = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);
			System.out.println("total number of Insight Items are : "+insightItemCount.size());

			if(insightItemCount.size()>0)
			{
				insightItemContinue :
					for(int k =1;k<=insightItemCount.size();k++)
					{
						try{
							titleAttributeInsights = Driver.findElement("xPath", xpathToBeUsed +"[" + k +"]" +" //*[@class='subtitle translucid']", 15).getText();
							hrefAttributeInsights = Driver.findElementAttribute("xPath", xpathToBeUsed +"[" + k +"]" +"/a", 15, "href");

							Reporter.log(logTime() + "  ....Element : " + titleAttributeInsights + "  |  Link : "+ hrefAttributeInsights, true);

							//						System.out.println( xpathToBeUsed +"[" + k +"]" +" //img[@alt='Default Alternative Text']");

							domains = Driver.click("xPath", xpathToBeUsed+"["+k+"]" +" //img[@alt='Default Alternative Text']", testParameters[3], testParameters);

							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
//							System.out.println("clicked on insight link");

							Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
							String afterClickUrl = Driver.getCurrentUrl();

							//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

//							System.out.println(afterClickUrl);

							if((hrefAttributeInsights.length()!=0) && (afterClickUrl.length() !=0))
							{
								if((hrefAttributeInsights.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeInsights)) )
								{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeInsights +"' is redirecting to correct URL, Hence Verified", true);
									if (checkDomains()){
										Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeInsights, true);

									}
									else{
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

									}
								}
								else{
									Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeInsights +"' is not redirecting to correct URL, Hence not Verified", true);
									Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
									insightTileValidated = false;

								}
							}else{
								Reporter.log(logTime() + "  ...." + "Link "+titleAttributeInsights+ " is blank or broken", true);

							}



							Driver.navigateTo(testParameters[3]);    // navigating to Work page

						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue insightItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue insightItemContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							Reporter.log(logTime() + "  ....Index out of bound message is " + e.getMessage(), true);
							e.printStackTrace();
						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}

		return insightTileValidated;
	}



}
