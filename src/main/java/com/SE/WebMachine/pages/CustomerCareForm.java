package com.SE.WebMachine.pages;

//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;

//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
//import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;

public class CustomerCareForm extends WebMachine_Base_Page{


	public static CustomerCareForm customerCareForm;

	public static void initialize() {
		customerCareForm = new CustomerCareForm();
	}

	public static CustomerCareForm getInstance() {
		return customerCareForm;

	}

	public boolean validateCustomerCareForm(String...testParameters)
	{
		boolean validateCustomerForm = true;
		String topMenuNames = new String();
//		String xpathTopMenu = new String();

		Driver.getInstance().navigate().refresh();

		if (testParameters[3] != null && testParameters[3].length() > 0){	

			try{
				boolean isElemPresent = Driver.isElementAvailable("xPath", testParameters[4]+"/a[text()='Support']");
				if(isElemPresent){

					Driver.click("xPath", testParameters[4]+"/a[text()='Support']");
					Thread.sleep(2000);
					
					if(Driver.isElementAvailable("xPath", testParameters[4]+"//ul[not(@*)] //a[text()='Contacts']")){
						
						Driver.click("xPath", testParameters[4]+"//ul[not(@*)] //a[text()='Contacts']");
						Thread.sleep(2000);
						
						if(Driver.isElementAvailable("xPath", testParameters[4]+"//ul[not(@*)]"+"//a[text()='Contact Customer Care']")){
							Thread.sleep(2000);
							Driver.click("xPath", testParameters[4]+"//a[text()='Contact Customer Care']");
							Thread.sleep(2000);
							Driver.getInstance().navigate().refresh();
							Thread.sleep(2000);
							//Driver.waitForPageLoadWithElement(Driver.getInstance(), 25, "xPath","//*[@id='FirstName']");
							
							Driver.getInstance().navigate().refresh();
							
							Reporter.log(logTime() + "  ...." + "Entering the relevant data in input boxes", true);

							Driver.sendKeys("id", "FirstName", 15, "Lokesh");
							Driver.sendKeys("id", "LastName", 15, "Jain");
							Driver.sendKeys("id", "Company", 15, "Schneider Electric Ltd");
							Driver.sendKeys("id", "Address", 15, "cyber hub");
							Driver.sendKeys("id", "AdditionalAddress__c", 15, "Gurgaon, Haryana");
							Driver.sendKeys("id", "PostalCode", 15, "122002");
							Driver.sendKeys("id", "Phone", 15, "9999002200");
							Driver.sendKeys("id", "Email", 15, "lokesh.jain@non.schneider-electric.com");
							Driver.sendKeys("id", "description", 15, "This is test for automation");
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCareRadioButton); // clicking radio button
							
							//Reporter.log(logTime() + "  ...." + "clicked", true);
							
							Reporter.log(logTime() + "  ...." + "Selecting the option from 'What can we help you with?' Select Box", true);
							//clicking on what can we help you with
							Thread.sleep(2000);
							
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCare_whatCanHelpWith);
							
							//Reporter.log(logTime() + "  ...." + "clicked again", true);
							
							Thread.sleep(3000);
							Driver.click("xPath",Constants.WebMachine_CommonConstants.customerCare_whatCanHelpWithOptionOne);
							
							
							Reporter.log(logTime() + "  ...." + "Selecting the option from 'What Segmant or Brand would you like help with?' Select Box", true);
							//clicking on What segment or brand would you like help with?
							Thread.sleep(2000);
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCare_whatSegmant);
							Thread.sleep(3000);
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCare_whatSegmantOptionOne);
							
							Reporter.log(logTime() + "  ...." + "Selecting the option from 'Please narrow down your selection?' Select Box", true);
							//clicking on Please narrow down your selection
							Driver.click("xPath",  Constants.WebMachine_CommonConstants.customerCare_narrowYourSelection);
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCare_narrowYourSelectionOptionOne);
							
							// //*[@class='tagline'] //text   to confirm this element on next page
							Driver.click("xPath", Constants.WebMachine_CommonConstants.customerCare_submitButton);
							
							Thread.sleep(5000);
						}else{
							Reporter.log(logTime() + "  ...." + "Link 'Contact Customer Care' is not present, so test will be skipped", true);
							validateCustomerForm = false;
						}
					}else{
						Reporter.log(logTime() + "  ...." + "Link 'Contacts' is not present, so test will be skipped", true);
						validateCustomerForm = false;
					}	
				}else{
					Reporter.log(logTime() + "  ...." + "Link 'Support' which contains 'Contact Customer Care' is not present, so test will be skipped", true);
					validateCustomerForm = false;
				}
			}catch (StaleElementReferenceException sere) {
				validateCustomerForm = false;
				Reporter.log(logTime() + "  ....StaleElementReferenceException on Link '" + topMenuNames+"'. Move on to next test...", true);
				Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
				//continue topMenu;
			}
			catch (NoSuchElementException nsee) {
				validateCustomerForm = false;
				Reporter.log(logTime() + "  ....NoSuchElementException on Link '" + topMenuNames + "'. Move on to next test...", true);
				Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
				//continue topMenu;
			}catch (ElementNotVisibleException e) {	
				validateCustomerForm = false;
				Reporter.log(logTime() + "  ....ElementNotVisibleException on Link '" + topMenuNames + "'. Move on to next Left link...", true);
				Reporter.log(logTime() + "  ....ElementNotVisibleException message is " + e.getMessage(), true);
				//continue leftMenuContinue;
			}catch (InterruptedException e) {
				e.printStackTrace();
			}




		}

		return validateCustomerForm;
	}
	
	
	
		
}
