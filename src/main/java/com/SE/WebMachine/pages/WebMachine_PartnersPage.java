package com.SE.WebMachine.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.tools.Driver;

public class WebMachine_PartnersPage extends WebMachine_Base_Page{


	public static void initializeWork() {

		StickyBars.initialize();
	}


	public static StickyBars getStickyBars() {
		return StickyBars.getInstance();
	}


	// validation of Tiles and insights if present

	public static boolean validateTiles(String...testParameters)
	{
		boolean tilesValidated = true;
		String titleAttributeTiles = new String();
		String hrefAttributeTiles = new String();


		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it


		try{  // need to change all logic inside try

			String xpathToBeUsed = "//*[@class='picks'] //*[@class='tiles']/li";
			//	String xpathExtension = "/a";
			//"//*[contains(@class,'picks-questions')]";
			//Constants.WebMachine_WorkPage.solutionItemBoxes; 
			//Constants.WebMachine_CommonConstants.rotatingCarousel;

			List<WebElement> tilesCount = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);

			System.out.println("total number of Tiles are : "+tilesCount.size());

			if(tilesCount.size()>0)
			{
				tileItemContinue :
					for(int k =1;k<=tilesCount.size();k++)
					{
						try{
							boolean isTitlePresent = Driver.isElementAvailable("xPath", xpathToBeUsed +"[" + k +"]" + " //em");
							
							if(isTitlePresent)
							{
								titleAttributeTiles = Driver.findElement("xPath", xpathToBeUsed +"[" + k +"]" + " //em", 15).getText();

								String classAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[" + k +"]" +" //div", 15, "class");
								//hrefAttributeTiles = Driver.findElementAttribute("xPath", xpathToBeUsed +"[" + k +"]" +xpathExtension, 15, "href");

							//	Reporter.log(logTime() + "  ....Element : " + titleAttributeTiles , true);

								//						System.out.println( xpathToBeUsed +"[" + k +"]" +" //img[@alt='Default Alternative Text']");

								/*
								 *  Requirement where we need to validate and enumerate the blue box items 
								 */

								if("picks-how-can-we-help".equalsIgnoreCase(classAttribute))   
								{
									validatePick_HowCanWeHelp(xpathToBeUsed, testParameters);
								}else if("picks-welcome".equalsIgnoreCase(classAttribute))
								{
									validatePick_ProgramPage(xpathToBeUsed, testParameters);
								}else
								{
									validateOtherPicks(xpathToBeUsed+"["+k+"]", testParameters);
								}
							}else{
								Reporter.log(logTime() + "  ....This element does not contain any name, please check manually", true);
							}



						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue tileItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue tileItemContinue;

						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}
		//Driver.isElementPresent(Xpath)
		boolean elemPres = Driver.isElementAvailable("xPath","//*[@class='multiple-picks'] //*[@class='tiles']/li");
//		System.out.println(elemPres);

		if(elemPres)
			//	validateTilesInsights(testParameters);
			Reporter.log(logTime() + "  ....Insights are not found on this page ", true);
		else
			Reporter.log(logTime() + "  ....Insights are not found on this page ", true);

		return tilesValidated;
	}

	// validation of how can we help section which is in blue color
	public static boolean validatePick_HowCanWeHelp(String xpathToBeUsed, String... testParameters)
	{
		boolean blueTilesValidated = true;

		try{
			String xpathExtended = "//*[@class='scroller'] //li";
			String titleAttributeBlueTiles = new String();
			String hrefAttributeBlueTiles = new String();


			List<WebElement> blueTilesCount = Driver.enumerateElements("xPath", xpathToBeUsed+xpathExtended, testParameters[3], testParameters);

			System.out.println("total number of Blue Tiles are : "+blueTilesCount.size());

			if(blueTilesCount.size()>0)
			{
				blueTileItemContinue :
					for(int i =1;i<=blueTilesCount.size();i++)
					{
						try{

							titleAttributeBlueTiles = Driver.findElement("xPath", xpathToBeUsed +xpathExtended+"[" + i + "]" + "/a", 15).getText();
							hrefAttributeBlueTiles = Driver.findElementAttribute("xPath", xpathToBeUsed +xpathExtended+"[" + i + "]" + "/a", 15,"href");

							Reporter.log(logTime() + "  ....Element : " + titleAttributeBlueTiles + "  |  Link : " + hrefAttributeBlueTiles , true);

							domains = Driver.click("xPath", xpathToBeUsed +xpathExtended+"[" + i + "]" + "/a", testParameters[3], testParameters);

							Thread.sleep(2000);

//							System.out.println("clicked on blue tile link");

							ArrayList<String> tabNew = new ArrayList<String>(
									Driver.getInstance().getWindowHandles());		// number of tabs in arraylist and then if size is >1 then if otherwise else

							if(tabNew.size()>1)
							{

								Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

								//Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
								String tabSwitchUrl = Driver.getCurrentUrl();

								if((hrefAttributeBlueTiles.length()!=0) && (tabSwitchUrl.length() !=0))
								{
									if((hrefAttributeBlueTiles.toLowerCase().contains(tabSwitchUrl.toLowerCase())) || (tabSwitchUrl.toLowerCase().contains(hrefAttributeBlueTiles.toLowerCase())) )
									{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeBlueTiles +"' is redirecting to correct URL, Hence Verified", true);
										if (checkDomains())
											Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeBlueTiles, true);
										else{
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerLinksValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeBlueTiles +"' is not redirecting to correct URL, Hence not Verified", true);
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										//footerLinksValidated = false;
									}
								}
								else{
									Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttributeBlueTiles, true);
									blueTilesValidated = false;
								}

								Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
								Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

							}else{

								Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
								String afterClickUrl = Driver.getCurrentUrl();

								//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

//								System.out.println(afterClickUrl);

								if((hrefAttributeBlueTiles.length()!=0) && (afterClickUrl.length() !=0))
								{
									if((hrefAttributeBlueTiles.toLowerCase().contains(afterClickUrl.toLowerCase())) || (afterClickUrl.toLowerCase().contains(hrefAttributeBlueTiles.toLowerCase())) )
									{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeBlueTiles +"' is redirecting to correct URL, Hence Verified", true);
										if (checkDomains()){
											Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeBlueTiles, true);

										}
										else{
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

										}
									}
									else{
										Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeBlueTiles +"' is not redirecting to correct URL, Hence not Verified", true);
										Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
										blueTilesValidated = false;

									}
								}else{
									Reporter.log(logTime() + "  ...." + "Link "+titleAttributeBlueTiles+ " is blank or broken", true);

								}

								Driver.navigateTo(testParameters[3]);    // navigating to Work page
							}



						}catch (NoSuchElementException nsee) {

							Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
							continue blueTileItemContinue;

						} catch (ElementNotVisibleException e) {
							Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
							e.printStackTrace();
							continue blueTileItemContinue;

						} catch (Schneider_IndexOutOfBoundsException e) {
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					}  // end of for loop
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue solutionItemContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue solutionItemContinue;

		}


		return blueTilesValidated;

	} // method ends here


	// validation of program page link 
	public static boolean validatePick_ProgramPage(String xpathToBeUsed, String... testParameters)
	{
		boolean programPageValidated = true;
		String xpathExtended = "//*[@class='picks-welcome']";

		String titleAttributeProgramPage = new String();
		String hrefAttributeProgramPage = new String();
		String hrefTextProgramPage = new String();
		String textOnProgramPage = new String();


		titleAttributeProgramPage = Driver.findElement("xPath", xpathToBeUsed + xpathExtended + " //em", 15).getText();
	//	textOnProgramPage = Driver.findElement("xPath", xpathToBeUsed + xpathExtended + " //p", 15).getText();
		hrefAttributeProgramPage = Driver.findElementAttribute("xPath", xpathToBeUsed + xpathExtended + " //a", 15,"href");
		hrefTextProgramPage = Driver.findElement("xPath", xpathToBeUsed + xpathExtended + " //a", 15).getText();

		//System.out.println(hrefTextProgramPage + "  is value of hrefTex");

		//Reporter.log(logTime() + "  ....Element : " + titleAttributeProgramPage + "  |  Description : " + textOnProgramPage,true);
		Reporter.log(logTime() + "  ....Element : " + titleAttributeProgramPage,true);
		Reporter.log(logTime() + "  ....Link : " + hrefAttributeProgramPage + "   |  Link Text :  "+hrefTextProgramPage  , true);
		
		if((hrefAttributeProgramPage.isEmpty())
				|| (hrefAttributeProgramPage.equalsIgnoreCase("#")) 
				//|| (hrefAttributeProgramPage.contains(""))
				|| (hrefTextProgramPage.isEmpty())
				)
			Reporter.log(logTime() + "  ....No link or link button is present on "+titleAttributeProgramPage,true);
		else{
			
			try {
	
				domains = Driver.click("xPath", xpathToBeUsed + xpathExtended + " //a", testParameters[3], testParameters);
				
				Thread.sleep(2000);

//				System.out.println("clicked on partner login link");

				ArrayList<String> tabNew = new ArrayList<String>(
						Driver.getInstance().getWindowHandles());		// number of tabs in arraylist and then if size is >1 then if otherwise else

				if(tabNew.size()>1)
				{

					Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

					//Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
					Thread.sleep(2000);
					String tabSwitchUrl = Driver.getCurrentUrl();

					if((hrefAttributeProgramPage.length()!=0) && (tabSwitchUrl.length() !=0))
					{	
						
						if((hrefAttributeProgramPage.toLowerCase().contains(tabSwitchUrl.toLowerCase())) || (tabSwitchUrl.toLowerCase().contains(hrefAttributeProgramPage.toLowerCase())) )
						{
							Reporter.log(logTime() + "  ...." + "Link '"+ hrefTextProgramPage +"' is redirecting to correct URL, Hence Verified", true);
							if (checkDomains())
								Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ hrefTextProgramPage, true);
							else{
								Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
								//footerLinksValidated = false;
							}
						}
						else{
							Reporter.log(logTime() + "  ...." + "Link '"+ hrefTextProgramPage +"' is not redirecting to correct URL, Hence not Verified", true);
							Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
							//footerLinksValidated = false;
						}
					}
					else{
						Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ hrefTextProgramPage, true);
						programPageValidated = false;
					}

					Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
					Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

				}else{

					Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
					String afterClickUrl = Driver.getCurrentUrl();

					//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

//					System.out.println(afterClickUrl);

					if((hrefAttributeProgramPage.length()!=0) && (afterClickUrl.length() !=0))
					{
						if((hrefAttributeProgramPage.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeProgramPage)) )
						{
							Reporter.log(logTime() + "  ...." + "Link '"+ hrefTextProgramPage +"' is redirecting to correct URL, Hence Verified", true);
							if (checkDomains()){
								Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ hrefTextProgramPage, true);

							}
							else{
								Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

							}
						}
						else{
							Reporter.log(logTime() + "  ...." + "Link '"+ hrefTextProgramPage +"' is not redirecting to correct URL, Hence not Verified", true);
							Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
							programPageValidated = false;

						}
					}else{
						Reporter.log(logTime() + "  ...." + "Link "+hrefTextProgramPage+ " is blank or broken", true);

					}

					Driver.navigateTo(testParameters[3]);    // navigating to Work page
				}  // end of if tabsize>1
				
			}catch (NoSuchElementException nsee) {

				Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
				//continue blueTileItemContinue;

			} catch (ElementNotVisibleException e) {
				Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
				e.printStackTrace();
				//continue blueTileItemContinue;

			} catch (Schneider_IndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}  // end of if href attr is empty
		return programPageValidated;
		}	// method ends here
		

	
	// validation of other picks
	
	public static boolean validateOtherPicks(String xpathToBeUsed, String...testParameters)
	{
		boolean picksValidated = true;
		String titleAttributeOtherPicks = new String();
		String hrefAttributeOtherPicks = new String();
		String xpathExtended = new String();
		//	boolean elemPresent = false;
		ArrayList<String> tabNew;

		Driver.getInstance().navigate().refresh();   // to escape any pop up , refresh skips it


		try{
			
			boolean isTitlePresent = Driver.isElementAvailable("xPath",  xpathToBeUsed + "/a //em");
			boolean isHrefPresent = Driver.isElementAvailable("xPath",  xpathToBeUsed + "/a");
			
			if(isTitlePresent && isHrefPresent)
			{
			
			titleAttributeOtherPicks = Driver.findElement("xPath", xpathToBeUsed + "/a //em", 15).getText();
			hrefAttributeOtherPicks = Driver.findElementAttribute("xPath", xpathToBeUsed + "/a", 15, "href");

			Reporter.log(logTime() + "  ....Element : " + titleAttributeOtherPicks +" | Link : " + hrefAttributeOtherPicks, true);	
			
			domains = Driver.click("xPath", xpathToBeUsed+" //img", testParameters[3], testParameters);
			Thread.sleep(5000);
			
			tabNew = new ArrayList<String>(
					Driver.getInstance().getWindowHandles());

			// if there are more windows then go inside if
			if(tabNew.size()>1)
			{
//				System.out.println("inside if");
				Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

				String tabSwitchUrl = Driver.getCurrentUrl();

				Reporter.log(logTime() + "  ....Current URL : " + tabSwitchUrl + " | href url : " + hrefAttributeOtherPicks, true);

				if((hrefAttributeOtherPicks.length()!=0) && (tabSwitchUrl.length() !=0))
				{
					if((hrefAttributeOtherPicks.contains(tabSwitchUrl)) || (tabSwitchUrl.contains(hrefAttributeOtherPicks)) )
					{
						Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeOtherPicks +"' is redirecting to correct URL, Hence Verified", true);
						if (checkDomains())
							Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeOtherPicks, true);
						else{
							Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
							//footerLinksValidated = false;
						}
					}
					else{
						Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeOtherPicks +"' is not redirecting to correct URL, Hence not Verified", true);
						Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
						//footerLinksValidated = false;
					}
				}
				else{
					Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttributeOtherPicks, true);
					picksValidated = false;
				}

				Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
				Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

			}else{   // of tabnew size

//				System.out.println("inside else of tab new");
				Driver.waitForPageLoad(Driver.getInstance(), 20, "Schneider");
				String afterClickUrl = Driver.getCurrentUrl();


				Thread.sleep(2000);


				Reporter.log(logTime() + "  ....Current URL : " + afterClickUrl + " | href url : " + hrefAttributeOtherPicks, true);

				if((hrefAttributeOtherPicks.length()!=0) && (afterClickUrl.length() !=0))
				{
					if((hrefAttributeOtherPicks.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeOtherPicks)) )
					{
						Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeOtherPicks +"' is redirecting to correct URL, Hence Verified", true);
						if (checkDomains())
							Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeOtherPicks, true);
						else{
							Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
							//footerMainValidated = false;
						}
					}
					else{
						Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeOtherPicks +"' is not redirecting to correct URL, Hence not Verified", true);
						Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
						//footerMainValidated = false;
					}
				}
				else{
					Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttributeOtherPicks, true);
					picksValidated = false;
				}

				Driver.navigateTo(testParameters[3]);
			}
	
			}else{
				Reporter.log(logTime() + "  ....Could not click on the Element, Please check manually." , true);
			}
		}catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
			//continue picksValContinue;

		} catch (ElementNotVisibleException e) {
			Reporter.log(logTime() + "  ....Element Not visible message is " + e.getMessage(), true);
			e.printStackTrace();
			//continue picksValContinue;

		} catch (Schneider_IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}



		//		}		// end of for loop

		//	}

		//		}catch (StaleElementReferenceException sere) {
		//			Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
		//		}
		//		catch (NoSuchElementException nsee) {
		//
		//			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);
		//
		//		} catch (ElementNotVisibleException e) {
		//
		//			e.printStackTrace();
		//		} 

		return picksValidated;

		//System.out.println("inside other picks");
	}

/*

								domains = Driver.click("xPath", xpathToBeUsed+"["+k+"]" + " //img", testParameters[3], testParameters);

								//[@alt='Default Alternative Text']
								//try {
								Thread.sleep(2000);

//								System.out.println("clicked on tile link");

								ArrayList<String> tabNew = new ArrayList<String>(
										Driver.getInstance().getWindowHandles());		// number of tabs in arraylist and then if size is >1 then if otherwise else

								if(tabNew.size()>1)
								{

									Driver.getInstance().switchTo().window(tabNew.get(tabNew.size()-1)); // switching to new window

									//Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
									String tabSwitchUrl = Driver.getCurrentUrl();

									if((hrefAttributeTiles.length()!=0) && (tabSwitchUrl.length() !=0))
									{
										if((hrefAttributeTiles.contains(tabSwitchUrl)) || (tabSwitchUrl.contains(hrefAttributeTiles)) )
										{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is redirecting to correct URL, Hence Verified", true);
											if (checkDomains())
												Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeTiles, true);
											else{
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
												//footerLinksValidated = false;
											}
										}
										else{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is not redirecting to correct URL, Hence not Verified", true);
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											//footerLinksValidated = false;
										}
									}
									else{
										Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttributeTiles, true);
										tilesValidated = false;
									}

									Driver.switchtoMainWindow(testParameters[3]);   // getting back to main window
									Driver.getInstance().manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);  // waiting for loading the window completely

								}else{

									Driver.waitForPageLoad(Driver.getInstance(), 30, "Schneider");
									String afterClickUrl = Driver.getCurrentUrl();

									//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

//									System.out.println(afterClickUrl);

									if((hrefAttributeTiles.length()!=0) && (afterClickUrl.length() !=0))
									{
										if((hrefAttributeTiles.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttributeTiles)) )
										{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is redirecting to correct URL, Hence Verified", true);
											if (checkDomains()){
												Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttributeTiles, true);

											}
											else{
												Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

											}
										}
										else{
											Reporter.log(logTime() + "  ...." + "Link '"+ titleAttributeTiles +"' is not redirecting to correct URL, Hence not Verified", true);
											Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
											tilesValidated = false;

										}
									}else{
										Reporter.log(logTime() + "  ...." + "Link "+titleAttributeTiles+ " is blank or broken", true);

									}

									Driver.navigateTo(testParameters[3]);    // navigating to Work page
								}
 */
}
