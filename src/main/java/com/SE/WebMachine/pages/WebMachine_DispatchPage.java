package com.SE.WebMachine.pages;

/* DH Java SE Libraries*/
//import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


import java.util.Set;







import java.util.concurrent.TimeUnit;


/* DH Java third party Libraries*/
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;











/* DH Java Schneider Libraries*/
import com.SE.WebMachine.base.WebMachine_Base_Page;
//import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;
//import com.thoughtworks.selenium.Selenium;

public class WebMachine_DispatchPage extends WebMachine_Base_Page{

	public WebDriver driver = null;
	private static Set<Schneider_PageElement> pageElements;

	public static boolean validateZonesContent(String... testParameters){
		boolean dispatchPageContents = true;
		String currentURL = Driver.getCurrentUrl();

//		System.out.println(testParameters[1]+" >> "+testParameters[2]+" >> "+testParameters[3]);

		String xPathToBeUsed = getLocator(testParameters);
		List<WebElement> zonesElements = Driver.enumerateElements("xPath", xPathToBeUsed, testParameters[4], testParameters);

		Reporter.log(logTime() + "  ....Total Zones present: "+ zonesElements.size(), true);

		dispatchPageContents = listElements(zonesElements, xPathToBeUsed, testParameters);

		/*	if (dispatchPageContents){
			dispatchPageContents = validatePageZones(dispatchPageContents, testParameters);	
		}*/
		Driver.chooseWindow(currentURL, testParameters[3], testParameters);
		return dispatchPageContents;
	}

	/*//validation of page Zones

	private static boolean validatePageZones(boolean dispatchPageContents, String... testParameters) {
		boolean dispatchPageContent = true;
		try{
			dispatchPageContent = validateZoneA("xPath", Constants.WebMachine_DispatchPage.AtHome_Link, testParameters);
			if (dispatchPageContents){
				dispatchPageContents = dispatchPageContent == true ? dispatchPageContent : false;
			}
			testParameters[1] = "Dispatch";
			dispatchPageContent = validateZoneB("xPath", Constants.WebMachine_DispatchPage.ZoneB_Link, testParameters);
			if (dispatchPageContents){
				dispatchPageContents = dispatchPageContent == true ? dispatchPageContent : false;
			}
			Driver.getBaseUrl(testParameters[4]);
			List<WebElement> zoneSection_Links = Driver.findElements("xPath", Constants.WebMachine_DispatchPage.ZoneSection_Link);
			if (zoneSection_Links.size()==3){
				testParameters[1] = "Dispatch";
				dispatchPageContent = validateZoneC("xPath", Constants.WebMachine_DispatchPage.ZoneC_Link, testParameters);
				if (dispatchPageContents){
					dispatchPageContents = dispatchPageContent == true ? dispatchPageContent : false;
				}
			}
		}catch(Throwable t){
			Reporter.log(logTime() + "  ERROR! No Zones found on Dispatch page.", true);
			ErrorUtil.addVerificationFailure(t);
		}
		return dispatchPageContents;						
	}*/
	//	private boolean enumeratePageZones(String... testParameters) {
	//
	//		String purpose = "Enumeration Of Zones";
	//		String summary = startOfTest(purpose, testParameters);
	//
	//		boolean pageZonesEnumerated = true;
	//		try{
	//			int zoneSectionSize = ZoneSection_Links.size();
	//			if (zoneSectionSize > 0){
	//				String zoneLink = Constants.WebMachine_DispatchPage.ZoneSection_Link;
	//				for(int i=1;i<=ZoneSection_Links.size();i++){
	//					try{
	//						zoneLink = zoneLink.replaceAll("//a", "/div[@id='nav']/a[" + i + "]/span/img");
	//						Assert.assertEquals(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneSection_Link), true,"ERROR! NO Zone link found");
	//					}catch(Throwable e){
	//						pageZonesEnumerated = false;
	//						ErrorUtil.addVerificationFailure(e);
	//					}
	//				}
	//				Reporter.log(logTime() + "  ...." + testParameters[0] + " Total " + ZoneSection_Links.size() + " zones are available on dispatch page", true);
	//			}
	//			else{
	//				pageZonesEnumerated = false;
	//				Reporter.log(logTime() + "  ....Ther are ZERO Zones Sections on Dispatch page", true);				
	//			}
	//		} catch (AssertionError ae) {
	//			pageZonesEnumerated = false;
	//			Reporter.log(logTime() + "  ....ERROR! No Zones found on Dispatch page.", true);
	//		}catch(Throwable t){
	//			Reporter.log(logTime() + " " + testParameters[0] + " ERROR! No Zones found on Dispatch page.", true);
	//			ErrorUtil.addVerificationFailure(t);
	//		}
	//		endOfTest(summary, pageZonesEnumerated, testParameters);	
	//		return pageZonesEnumerated;	
	//	}
	@SuppressWarnings("unused")
	private WebElement refreshCountryName_Link() {
		WebElement CountryName_Link = driver.findElement(By.xpath(Constants.WebMachine_DispatchPage.CountryName_Link));
		return CountryName_Link;
	}
	@SuppressWarnings("unused")
	private static boolean validateZoneA(String locator, String location, String... testParameters) {
		Reporter.log(logTime() + "  ....Validation of Zone (A) START", true);
		boolean dispatchPageContent = false;
		dispatchPageContent = fnValidate_ZoneA(locator, location, testParameters);
		logResult(dispatchPageContent, "A");
		return dispatchPageContent;
	}
	@SuppressWarnings("unused")
	private static boolean validateZoneB(String locator, String location, String... testParameters) {
		Reporter.log(logTime() + "  ....Validation of Zone (B) START", true);
		boolean dispatchPageContent = false;
		dispatchPageContent = fnValidate_ZoneB(locator, location, testParameters);
		logResult(dispatchPageContent, "B");
		return dispatchPageContent;
	}
	@SuppressWarnings("unused")
	private static boolean validateZoneC(String locator, String location, String... testParameters) {
		Reporter.log(logTime() + "  ....Validation of Zone (C) START", true);
		boolean dispatchPageContent = false;
		dispatchPageContent = fnValidate_ZoneC(locator, location, testParameters);
		logResult(dispatchPageContent, "C");
		return dispatchPageContent;
	}

	private static void logResult(boolean dispatchPageContent, String zone) {
		if (dispatchPageContent){
			Reporter.log(logTime() + "  ....Validation of Zone ("+zone+") PASSED", true);	
		}
		else{
			Reporter.log(logTime() + "  ....Validation of Zone ("+zone+") FAILED", true);	
		}
	}

	//	private static boolean validateZoneC(String locator, String location, String... testParameters) {
	//		Reporter.log(logTime() + "  " + testParameters[0] + " START. Validation of Third Zone (C)", true);
	//		String currentUrl = Driver.getCurrentUrl();
	//		boolean dispatchPageContent = false;
	//		try{
	//			dispatchPageContent = fnValidate_ZoneC(testParameters);
	//			Assert.assertTrue(dispatchPageContent, "FAILED. Third Zone (C) page is Not available.");
	//			Reporter.log(logTime() + " " + testParameters[0] + " PASSED. Validation of Third Zone (C)", true);	
	//		} catch (AssertionError ae){
	//			Reporter.log(logTime() + " " + "ZoneC Validation failed, needs this to be checked manually. Moving on to the next test ...", true);
	//			Driver.chooseWindow(currentUrl, testParameters[4], testParameters);
	//			//fnSwitchToTestWindow(currentUrl, testParameters);
	//		}			
	//		return dispatchPageContent;
	//	}

	public static boolean fnValidate_ZoneA(String locator, String location, String... testParameters){
		boolean zoneAValidation = true;
		try{
			//Driver.getBaseUrl(testParameters[4]);
			testParameters[2] = "Zone A";
			domains = Driver.click(locator, location, testParameters[4], testParameters);
			checkDomains();
			if (zoneAValidation){
				String strCurrentURL = Driver.getCurrentUrl();
				Reporter.log(logTime() + "  ....Landing page URL : "+strCurrentURL, true);
				if(Driver.getPageTitle().contains("Error") || Driver.getPageTitle().contains("404")){
					zoneAValidation = false;
				}
				zoneAValidation = Driver.checkCanonicalURL(strCurrentURL, testParameters);
			}
		}catch(Throwable t){
			Driver.getBaseUrl(testParameters[4]);
			ErrorUtil.addVerificationFailure(t);
			return false;

		}
		return zoneAValidation;
	}

	public static boolean fnValidate_ZoneB(String locator, String location, String... testParameters){
		boolean zoneBValidation = true;
		try{
			Driver.getBaseUrl(testParameters[4]);
			testParameters[2] = "Zone B";
			domains = Driver.click(locator, location, testParameters[4], testParameters);
			checkDomains();
			if (zoneBValidation){
				String strCurrentURL = Driver.getCurrentUrl();
				Reporter.log(logTime() + "  ....Landing page URL : "+strCurrentURL, true);
				if(Driver.getPageTitle().contains("Error") || Driver.getPageTitle().contains("404")){
					zoneBValidation = false;
				}
				zoneBValidation = Driver.checkCanonicalURL(strCurrentURL, testParameters);
			}
		}catch(Throwable t){
			Driver.getBaseUrl(testParameters[4]);
			ErrorUtil.addVerificationFailure(t);
			return false;

		}
		return zoneBValidation;
	}

	public static boolean fnValidate_ZoneC(String locator, String location, String... testParameters){

		//  This function validates if clicking @Home zone on Dispatch page navigates user to @Home home page.
		boolean zoneCValidation = true;
		try{
			Driver.getBaseUrl(testParameters[4]);
			testParameters[2] = "Zone C";
			domains = Driver.click(locator, location, testParameters[4], testParameters);
			checkDomains();
			if (zoneCValidation){
				String strCurrentURL = Driver.getCurrentUrl();
				Reporter.log(logTime() + "  ....Landing page URL : "+strCurrentURL, true);
				if(Driver.getPageTitle().contains("Error") || Driver.getPageTitle().contains("404")){
					zoneCValidation = false;
				}
				zoneCValidation = Driver.checkCanonicalURL(strCurrentURL, testParameters);
			}
		}catch(Throwable t){
			Driver.getBaseUrl(testParameters[4]);
			ErrorUtil.addVerificationFailure(t);
			return false;

		}
		return zoneCValidation;
	}

	public static boolean validateLifeIsOnLogo(String... testParameters) {
		boolean lifeIsOnLogoIsPresent = true;

		String xPathToBeUsed = new String();
		if (testParameters[1].contains("Header")) {
			// Maybe some day life is on logo links will appear on header
			// xPathToBeUsed = Constants.WebMachine_CommonConstants.LifeIsOnHeader_Link;
		} else {
			xPathToBeUsed = Constants.WebMachine_CommonConstants.LifeIsOnFooter_Link;
		}
		try {
			if (Driver.isElementPresent(xPathToBeUsed)){
				Reporter.log(logTime() + "  ...."+ testParameters[1] + " " + testParameters[3] + " PASSED", true);
				if (Driver.getPageTitle().contains("Error") 
						|| Driver.getPageTitle().contains("404")) {
					Assert.fail("ERROR! Link " + "" + " is Broken");
				}
			}
		} catch (Throwable t) {
			lifeIsOnLogoIsPresent = false;
			Reporter.log(logTime() + "  ....ERROR! "+ testParameters[2] + " NOT found on "
					+ testParameters[1] + "", true);
		}
		return lifeIsOnLogoIsPresent;

	}
	@SuppressWarnings("unused")
	private static boolean validateLandingPageLinks(String... testParameters) {
		boolean lifeIsOnLogoIsPresent = true;

		String xPathToBeUsed = new String();
		if (testParameters[1].contains("Header")) {
			// Maybe some day life is on logo links will appear on header
			// xPathToBeUsed = Constants.WebMachine_CommonConstants.LifeIsOnHeader_Link;
		} else {
			xPathToBeUsed = Constants.WebMachine_CommonConstants.LifeIsOnFooter_Link;
		}
		try {
			if (Driver.isElementPresent(xPathToBeUsed)){
				Reporter.log(logTime() + "  ...." + testParameters[0] + " "
						+ testParameters[2] + " " + testParameters[1]
								+ " : " + "passed", true);
				if (Driver.getPageTitle().contains("Error")
						|| Driver.getPageTitle().contains("404")) {
					Assert.fail("ERROR! Link " + "" + " is Broken");
				}
			}
			//Assert.assertTrue(Driver.isElementPresent(xPathToBeUsed), "Life Is On logo NOT found on dispatch page.");

		} catch (AssertionError ae) {
			lifeIsOnLogoIsPresent = false;
			Reporter.log(logTime() + "  ....Life Is On logo NOT found on dispatch page.", true);
		} catch (Throwable t) {
			lifeIsOnLogoIsPresent = false;
			Reporter.log(logTime() + "  " + testParameters[0] + " "
					+ testParameters[2] + " NOT found on "
					+ testParameters[1] + "", true);
			ErrorUtil.addVerificationFailure(t);
		}
		return lifeIsOnLogoIsPresent;

	}
	//	public void fnValidate_LandingPage(String... testParameters) {
	//		String purpose = "- Validation.";
	//		String summary = startOfTest(purpose, testParameters);
	//		boolean landingPageLinksOK = true;
	//
	//		landingPageLinksOK = validateLandingPageHeaderOrFooter(testParameters);
	//
	//		endOfTest(summary, landingPageLinksOK, testParameters);	
	//	}
	//	private boolean validateLandingPageHeaderOrFooter(
	//			String... testParameters) {
	//		boolean zoneLandingPage = true;
	//		try{
	//			driver.get(testData.get("baseURL"));
	//			WebElement wbElement = ZoneA_Link;
	//			testParameters[1] = "@HOME " + testParameters[1];
	//			if (testParameters[2].contains("Zone B")){
	//				wbElement = ZoneB_Link;
	//				testParameters[1] = "@WORK " + testParameters[1];
	//			}
	//			else if (testParameters[2].contains("Zone C")){
	//				wbElement = ZoneC_Link;
	//				testParameters[1] = "Partners " + testParameters[1];
	//			}
	//
	//			zoneLandingPage = fnClick(wbElement, testParameters);
	//			String currentURL = driver.getCurrentUrl();
	//			if (zoneLandingPage){
	//
	//				zoneLandingPage = validate_SELogo(Constants.WebMachine_CommonConstants.SELogoHeader_Link, testParameters);//PASSED	
	//				fnSwitchToTestWindow(currentURL);
	//				zoneLandingPage = fnValidateHeaderLinks(testParameters);//PASSED
	//				fnSwitchToTestWindow(currentURL);
	//				zoneLandingPage = validateSearch("Req 4", "Dispatch Header", "Search Icon");  //PASSED
	//				fnSwitchToTestWindow(currentURL);
	//				zoneLandingPage = fnValidate_SocialMedia(testParameters);//PASSED	
	//				fnSwitchToTestWindow(currentURL);
	//
	//				String StrCurrentURL = driver.getCurrentUrl();
	//				Reporter.log(logTime() + "  Landing page URL : "+StrCurrentURL, true);
	//				if(Driver.getPageTitle().contains("Error") || Driver.getPageTitle().contains("404")){
	//					zoneLandingPage = false;
	//				}
	//				fnCheckCanonicalURL();
	//			}
	//		}catch(Throwable t){
	//			driver.get(testData.get("baseURL"));
	//			ErrorUtil.addVerificationFailure(t);
	//			Reporter.log(testParameters[2] + " Link Not working ", true);
	//			return false;
	//
	//		}
	//		return zoneLandingPage;
	//	}




	public static void initialize(Hashtable<String, String> configData) {
		Driver.initialize(configData);
//		Header.initialize();
//		Footer.initialize();
//		UpperHeader.initialize();
		CustomerCareForm.initialize();
		testData.put("Timeout", configData.get("Timeout")); // for use by other parts of the application
		Reporter.log(logTime() + "  ....Timeout set to " + configData.get("Timeout") + " seconds", true);
		Reporter.log(logTime() + "  ....(Test Data worksheet in \"" + Constants.Paths.XlsReader_FILE_PATH + "\").", true);
	}

	public static void launch(String baseURL) {
	
		Driver.getInstance().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		Driver.getInstance().get("http://www.google.com");
//		Driver.waitForPageLoad(Driver.getInstance(), 45, "Google");
		try {
			Thread.sleep(300);
			System.out.println("inside launch waiting upto 3 seconds");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Driver.getInstance().get(baseURL);
		//Footer.initialize();
	}

	public static void awaitPresenceOfElement(Schneider_PageElement pageElement) {
		Reporter.log(logTime() + " " + "Awaiting the availability of the Select Country Element as its presence indicates that the page has fully loaded ...", true);	
		Driver.awaitPresenceOfElement(pageElement);
	}
//	public static Header getHeader() {
//		return Header.getInstance();
//	}
//	public static Footer getFooter(){
//		return Footer.getInstance();
//	}
//	
//	public static UpperHeader getUpperHeader() {
//		return UpperHeader.getInstance();
//	}
	
	public static CustomerCareForm getCustomerCareForm() {
		return CustomerCareForm.getInstance();
	}
	
	public static void setPageElements(Set<Schneider_PageElement> pageElements) {
		WebMachine_DispatchPage.pageElements = pageElements;
	}
	public static Set<Schneider_PageElement> getPageElements() {
		return pageElements;
	}



	/*public boolean validate_Countries(String... testParameters) {

		String startOfTest = logTime();
		Reporter.log(startOfTest + " " + testParameters[0]
				+ " START. Validation of " + testParameters[1] + " "
				+ testParameters[2], true);

		boolean countryValidated = true;		
		boolean countriesValidated = true;

		if (testParameters[3] != null && testParameters[3].length() > 0){
			if (testParameters[3].contains("www.") || 
					testParameters[3].contains("www-sqewm.")){
				String currentURL = driver.getCurrentUrl();
				List<WebElement> geographyElements = enumerateGeographyElements("Req 2", "Header", "Validation of Country Selection", "Africa", "Botswana");//
				countriesValidated = geographyElements.size() > 0 ? true : false; 

				if (countriesValidated){
					ArrayList<String> continents = new ArrayList<String>();
					for (WebElement geographyElement : geographyElements){
						continents.add(geographyElement.getText());
					}
					// sample objDispatchPage.enumerateCountryElements("Europe", 4, "Req 2", "Header", "Country Selection");	
					continents:
						for (int continentIndex = 0; continentIndex < geographyElements.size(); continentIndex++){
							driver.navigate().to(currentURL);
							String[] continent = continents.get(continentIndex).split("\n");
							By by = By.xpath(Constants.WebMachine_DispatchPage.SelectCountry_Header_Link);
							WebElement countryLink = Driver.findElement(driver, by, 2);
							fnClick(countryLink, "Req 2", "Header", "\"" + continent + "\" - Validation of Country Selection");
							awaitPresenceOfElement(By.xpath(Constants.WebMachine_DispatchPage.Geography_Elements), countryLink);
							for (WebElement geographyElement : getGeographyElements()){
								countryValidated = enumerateCountryElements(continent[0], continentIndex+1, "Req 2", "Header", "Validation of Country Selection");
								if (countriesValidated){
									countriesValidated = countryValidated == true ? countryValidated : false;	
								}
								continue continents;
							}
						}					
				}
			}
			else{
				Reporter.log(logTime() + "  ...." + "Test domain = INT. Known Issue. Country selector not implemented so test will be skipped", true);
				countriesValidated = true;
			}
		}
		String endOfTest = logTime();	
		String status = countriesValidated == true ? "PASSED" : "FAILED";
		Reporter.log(endOfTest + " " + testParameters[0] + " END. " + status + ": Validation " + testParameters[1] + " " + testParameters[2] + ".", true);
		return countriesValidated;
	}*/

	// validates Dispatch page Zones
	public static boolean carouselDispatch(String... testParameters) {
		boolean dispatchPageCarosel = true;
		
		Driver.navigateTo(testParameters[3]);

		String xpathToBeUsed = Constants.WebMachine_CommonConstants.rotatingCarousel;
		try{
			boolean carouselPresent = Driver.isElementPresent(xpathToBeUsed);

			if(carouselPresent){
				List<WebElement> carouselList = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);

				Reporter.log(logTime() + " Number of Carousels on the " + testParameters[1] + " Page are : "+carouselList.size(), true);

				try{
					if(carouselList.size()>0)
					{
						String classAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[1]", 15, "class");

						if("current".equalsIgnoreCase(classAttribute)){
							Reporter.log(logTime() + " The Carousel for clicking on Zones on " + testParameters[1] + " Page is already selected !! ",true);
							validate_DispatchPageZonesContent(testParameters); 
						}
						else{
							Driver.click("xPath", xpathToBeUsed+"[1]");
							Reporter.log(logTime() + " The Carousel for clicking on Zones on " + testParameters[1] + " Page is clicked now !! ",true);
							validate_DispatchPageZonesContent(testParameters);
						}

					}

				}catch (StaleElementReferenceException sere) {
					Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
				}
				catch (NoSuchElementException nsee) {

					Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

				} catch (ElementNotVisibleException e) {

					e.printStackTrace();
				} 		}

		}catch (StaleElementReferenceException sere) {
			Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
		}
		catch (NoSuchElementException nsee) {

			Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

		} catch (ElementNotVisibleException e) {

			e.printStackTrace();
		} 
		return dispatchPageCarosel;						
	}

	public static boolean validate_DispatchPageZonesContent(String... testParameters){
		boolean dispatchPageContents = true;
		@SuppressWarnings("unused")
		String currentURL = Driver.getCurrentUrl();
		int zoneLinksNum = enumeratePageZones(testParameters);
		if(zoneLinksNum >0)
		{
			validatePageZones(zoneLinksNum, testParameters);

		}
		return dispatchPageContents;
	}

	public static int enumeratePageZones(String... testParameters) {

		String xpathToBeUsed = Constants.WebMachine_DispatchPage.dispatchZones;
		List<WebElement> zoneSection_Links = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);
		int pageZonesEnumerated = 0;
		try{
			int zoneSectionSize = zoneSection_Links.size();
			if (zoneSectionSize > 0){
				try{

					Assert.assertEquals(Driver.isElementPresent("xPath",Constants.WebMachine_DispatchPage.zoneSection_Link), true,"ERROR! NO Zone link found");
					pageZonesEnumerated = zoneSectionSize;	

				}catch(Throwable e){
					pageZonesEnumerated = 0;

				}

				Reporter.log(logTime() + "  ...." + testParameters[0] + " Total " + zoneSection_Links.size() + " zones are available on dispatch page", true);
			}
			else{
				pageZonesEnumerated = 0;
				Reporter.log(logTime() + "  ....There are ZERO Zones Sections on "+ testParameters[1] +" page", true);				
			}
		} catch (AssertionError ae) {
			pageZonesEnumerated = 0;
			Reporter.log(logTime() + "  ....ERROR! No Zones found on "+ testParameters[1] +" page.", true);
		}catch(Throwable t){
			Reporter.log(logTime() + " " + testParameters[0] + " ERROR! No Zones found on "+ testParameters[1] +" page.", true);

		}

		return pageZonesEnumerated;	
	}

	//validation of page Zones

	public static boolean validatePageZones(int zoneLinksNumber, String... testParameters) {
		boolean dispatchPageContent = true;
		try{

			for(int i=1;i<=zoneLinksNumber;i++)
			{
				dispatchPageContent = validateZone("xPath",Constants.WebMachine_DispatchPage.zoneSection_Link + "[" + i + "]", i, testParameters);
			}

		}catch(Throwable t){
			Reporter.log(logTime() + "  ERROR! No Zones found on "+ testParameters[1] +" page.", true);
			dispatchPageContent = false;
		}
		return dispatchPageContent;						
	}


	public static boolean validateZone(String locator, String location,int count,  String... testParameters) {

		Reporter.log(logTime() + "  ....Validation of Zones START", true);

		boolean dispatchPageContent = false;

		dispatchPageContent = validate_Zones(locator, location,count, testParameters);

		return dispatchPageContent;
	}


	public static boolean validate_Zones(String locator, String location,int num, String... testParameters){
		boolean zoneValidation = true;
		String hrefAttribute = new String();


		try{
			testParameters[2] = "Zone A";
			hrefAttribute = Driver.findElementAttribute(locator, location, 15, "href");

			String titleAttribute = Driver.findElement(locator, "//div[@id='nav'] //a["+num+"]", 15).getText();

			domains = Driver.click(locator, location, testParameters[3], testParameters);

			String afterClickUrl = Driver.getCurrentUrl();

			Reporter.log(logTime() + "  ....Landing page URL : "+afterClickUrl, true);

			zoneValidation = Driver.checkCanonicalURL(afterClickUrl, testParameters);

			if((hrefAttribute.length()!=0) && (afterClickUrl.length() !=0))
			{
				if((hrefAttribute.contains(afterClickUrl)) || (afterClickUrl.contains(hrefAttribute)) )
				{
					if(Driver.getPageTitle().contains("Error") || Driver.getPageTitle().contains("404")){

						Reporter.log(logTime() + "  ....Redirecting link for " + titleAttribute +" is redirecting to Error 404", true);
						zoneValidation = false;

					}else{
						Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is redirecting to correct URL, Hence Verified", true);
						if (checkDomains())
							Reporter.log(logTime() + "  ....Hence the domain is OK  for "+ titleAttribute, true);
						else{
							Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);

						}
					}
				}
				else{
					Reporter.log(logTime() + "  ...." + "Link '"+ titleAttribute +"' is not redirecting to correct URL, Hence not Verified", true);
					Reporter.log(logTime() + "  ...." + "Domain not verified. Known Issue.", true);
					zoneValidation = false;
				}
			}
			else{
				Reporter.log(logTime() + "  ....Redirecting link is found blank for "+ titleAttribute, true);
				zoneValidation = false;
			}

			Driver.navigateTo(testParameters[4]);    // navigating to Dispatch page


		}catch(Throwable t){
			Driver.getBaseUrl(testParameters[3]);
			//	ErrorUtil.addVerificationFailure(t);
			return false;

		}
		return zoneValidation;
	}

	/*	// click on carousel and then click on respective link like @Home, @Work
		public static boolean carouselClick(String... testParameters) {

			boolean dispatchPageCarosel = true;

			String xpathToBeUsed = Constants.WebMachine_DispatchPage.carousel_DispatchPage;

			boolean carouselPresent = Driver.isElementPresent(xpathToBeUsed);

			if(carouselPresent){
				List<WebElement> carouselList = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[3], testParameters);

				Reporter.log(logTime() + " Number of Carousels on the Dispatch Page are : "+carouselList.size(), true);

				try{
					if(carouselList.size()>0)
					{
						String classAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[1]", 15, "class");

						if("current".equalsIgnoreCase(classAttribute)){
							Reporter.log(logTime() + " The Carousel for clicking on Zones on Dispatch Page is already selected !! ",true);
							Driver.click("xPath", "//*[@id='zoneA']");
							Reporter.log(logTime() + " We are on @Home Page !! ",true);
							//validate_DispatchPageZonesContent(testParameters); 
						}
						else{
							Driver.click("xPath", xpathToBeUsed+"[1]");
							Reporter.log(logTime() + " The Carousel for clicking on Zones on Dispatch Page is clicked now !! ",true);
							Driver.click("xPath", "\\*[@id='zoneA']");
							Reporter.log(logTime() + " We are on @Home Page !! ",true);
							//validate_DispatchPageZonesContent(testParameters);
						}

					}

				}catch (StaleElementReferenceException sere) {
					Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
				}
				catch (NoSuchElementException nsee) {

					Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

				} catch (ElementNotVisibleException e) {

					e.printStackTrace();
				} 		}
			return dispatchPageCarosel;						
		}*/


}







