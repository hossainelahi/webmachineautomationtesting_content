package com.SE.WebMachine.base;

/* DH Java SE Libraries*/
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
//import java.util.concurrent.TimeUnit;






/* DH Java third party Libraries*/
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;






/* DH Java Schneider Libraries*/
import com.SE.WebMachine.exceptions.Schneider_IndexOutOfBoundsException;
import com.SE.WebMachine.exceptions.Schneider_TimeoutException;
import com.SE.WebMachine.pageElements.Schneider_PageElement;
import com.SE.WebMachine.tools.Driver;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;
import com.SE.WebMachine.util.Xls_Reader;


//@Listeners({ ATUReportsListener.class, MethodListener.class, ConfigurationListener.class })
public class WebMachine_Base_Page {
	
	//public static final Class<ExtentReports> extent = ExtentReports.class;
	
//	private static final TimeUnit SECONDS = null;
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	{
		System.setProperty("atu.reporter.config", Constants.Paths.ATU_ReporterConfig_FILE_PATH);
	}
	protected static By byAccessSite;
	protected static By byCloseVisit;
	public static WebDriver driver = null;
	protected Properties Config = null; // Initialize Properties
	protected Properties ENV = null;
	static WebMachine_Base_Page objBase;
	public static Xls_Reader xls = new Xls_Reader(
			Constants.Paths.XlsReader_FILE_PATH);

	@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.SelectCountry_Header_Link)
	public static WebElement SelectCountry_Header_Link;
	@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.SelectCountry_Header_Link)
	public WebElement Geography_DropDown;	

	protected static Hashtable<String, String> testData = new Hashtable<String, String>();
	protected static Hashtable<String, String> domains;
	
	public static void clickCookiesLink(Schneider_PageElement pageElement, String baseURL, String... testParameters) throws ElementNotVisibleException, Schneider_IndexOutOfBoundsException {
		//closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);
		if (Driver.checkLink(pageElement)){
			testParameters[1] = "Cookies";
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Cookies popup has been closed", true);
		}
		/*else
		{
			System.out.println("in else part");
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Cookies popup is not found", true);
		}*/
	}
	
	public static void skipVideoLink(Schneider_PageElement pageElement, String baseURL, String... testParameters) throws ElementNotVisibleException, Schneider_IndexOutOfBoundsException {
		//closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);
		if (Driver.checkLink(pageElement)){
			testParameters[1] = "Cookies";
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Skip Video popup has been closed", true);
		}
		/*else
		{
			System.out.println("in else part");
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Cookies popup is not found", true);
		}*/
	}
	
	public static void stayOnGlobalWebsite(Schneider_PageElement pageElement, String baseURL, String... testParameters) throws ElementNotVisibleException, Schneider_IndexOutOfBoundsException {
		//closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);
		if (Driver.checkLink(pageElement)){
			testParameters[1] = "Cookies";
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Stay on Global Website popup has been closed", true);
		}
		/*else
		{
			System.out.println("in else part");
			Driver.click(pageElement, baseURL, testParameters);
			Reporter.log(logTime() + "  ....Cookies popup is not found", true);
		}*/
	}

	public void fnLanguageConverter(String strPrintMessage)
			throws UnsupportedEncodingException {
		PrintStream sysout = new PrintStream(System.out, true, "UTF-8");
		sysout.println(strPrintMessage);
	}

	public static boolean enumerateCountryElements(String continent, int continentIndex, String... testParameters) {
		boolean enumerateCountryElements = true;
		try{	
			/* geographyOptionInstance :
			 * 1 means Africa, 2 is Asia, 3 is Australasia, 4 is Europe
			 * 5 is North America, 6 is South America*/
			/* countryOptionInstance :
			 * For 1 Africa: 3 means Botswana, 8 is Chad
			 * For 4 Europe: 5 means Belgium, 30 is Norway*/

			String geographyElements = Constants.WebMachine_DispatchPage.Geography_Elements
					.concat("[" + continentIndex + "]/ul/li");
			
			List<WebElement> wbCountry_Elements = Driver.findElements("xPath", geographyElements);

			Reporter.log(logTime() + "  ..." + "Geography Link "+ continentIndex+ " \"" + continent	+ "\" has " 
					+ wbCountry_Elements.size() + " countries." , true);
			/*this line clicks each individual country*/
			/*
			 * String specificContinentLink = Constants.WebMachine_DispatchPage.Geography_Elements.concat("[" + continentIndex + "]/a");
			 enumerateCountryElements = validateCountryElements(continentIndex, wbCountry_Elements, specificContinentLink, testParameters);
			if(enumerateCountryElements){
				fnClick(SelectCountry_Header_Link,	testParameters);
				Driver.awaitPresenceOfElement(By.xpath(Constants.WebMachine_DispatchPage.Geography_Elements), SelectCountry_Header_Link);	
			}
			*/
		}
		catch(TimeoutException toe){
			checkForMaintenanceMessage();
			Reporter.log(logTime() + " " + testParameters[0] + " ERROR! " + testParameters[2] + " Timeout Exception on " + SelectCountry_Header_Link.getText(), true);
			enumerateCountryElements = false;
			return enumerateCountryElements;				
		}		
		catch(Throwable t){
			enumerateCountryElements = false;
			Reporter.log(logTime() + " " + testParameters[0] + " ERROR! No " + testParameters[2] + " found on Dispatch page.", true);
			ErrorUtil.addVerificationFailure(t);
		}
		checkForMaintenanceMessage();
		return enumerateCountryElements;
	}
//	private static boolean validateCountryElements(int continentIndex, List<WebElement> wbCountry_Elements, 
//			String specificContinentLink, String... testParameters)
//					throws UnsupportedEncodingException {
//		boolean validateCountryElements = true;
//		boolean validateIndividualCountryElement = true;
//		int countryElementInstance = 1;
//
//		String xPath = new String();
//		try{
//			countryElements:
//			for (WebElement wbCountry_Element : wbCountry_Elements) {
//				xPath = specificContinentLink;
//				WebElement specificGeographyElement = driver.findElement(By.xpath(xPath));	
//				Reporter.log(logTime() + "  ...Clicking " + specificGeographyElement.getText() , true);
//				fnClick(specificGeographyElement, testParameters);
//				String specificCountryLink = new String();
//				specificCountryLink = Constants.WebMachine_DispatchPage.Country_Options
//						.replace("X", Integer.toString(continentIndex));
//				specificCountryLink = specificCountryLink.replace("Y", Integer.toString(countryElementInstance));	
//				xPath = specificCountryLink;
//				WebElement specificCountryElement = driver.findElement(By.xpath(xPath));	
//				String strCountryElementName = getLinkIdentifier(specificCountryLink);
//				String hrefValueForTest = specificCountryElement.getAttribute("href");
//				String currentTestURL = driver.getCurrentUrl();
//				String landingPageURL = null;
//
//				Reporter.log(logTime() + "  ....Click " + strCountryElementName + ". (Link is: " + specificCountryLink + ").", true);
//				try{
//					validateIndividualCountryElement = fnClick(specificCountryElement, testParameters);
//				}
//				catch(NoSuchElementException nsee){
//					Reporter.log(logTime() + "  NoSuchElementException! When clicking xPath is " + xPath + " for link " + specificCountryLink + " (Country " + strCountryElementName + ")", true);
//					validateIndividualCountryElement = false;
//					if (validateCountryElements){
//						validateCountryElements = validateIndividualCountryElement == true ? validateIndividualCountryElement : false;
//					}
//					fnSwitchToTestWindow(currentTestURL, testParameters);
//					countryElementInstance++;
//					continue countryElements;
//				}
//				if (validateIndividualCountryElement){
//					validateIndividualCountryElement = validateLandingPage(strCountryElementName);
//				}
//				else{
//					Reporter.log(logTime() + "  ....Click failed." , true);
//					validateIndividualCountryElement = false;
//					if (validateCountryElements){
//						validateCountryElements = validateIndividualCountryElement == true ? validateIndividualCountryElement : false;
//					}
//				}
//
//				if (validateIndividualCountryElement){
//					Reporter.log(logTime() + "  ..." 
//							+ "Country "
//							+ countryElementInstance 
//							+ " \"" 
//							+ strCountryElementName
//							+ "\": Passed Assertion "
////							+ "- Actual URL : " 
////							+ landingPageURL.trim() + ""
////							+ " - Expected URL : " 
////							+ hrefValueForTest.trim()					
//							, true);
//					validateIndividualCountryElement = true;
//					fnSwitchToTestWindow(currentTestURL, testParameters);
//					countryElementInstance++;
//				}
//				else{
//					Reporter.log(logTime() + "  ...." 
//							+ "Country "
//							+ countryElementInstance 
//							+ " \"" 
//							+ strCountryElementName
//							+ "\": Failed Assertion "
//							+ "- Actual URL : " 
//							+ landingPageURL + ""
//							+ " - Expected URL : " 
//							+ hrefValueForTest					
//							, true);
//					if (validateCountryElements){
//						validateCountryElements = validateIndividualCountryElement == true ? validateIndividualCountryElement : false;
//					}
//					fnSwitchToTestWindow(currentTestURL, testParameters);
//					countryElementInstance++;
//				}
//			}	
//		}
//		catch(NoSuchElementException nsee){
//			Reporter.log(logTime() + "  No Such Element EXCEPTION! xPath is " + xPath, true);
//			validateIndividualCountryElement = false;
//			if (validateCountryElements){
//				validateCountryElements = validateIndividualCountryElement == true ? validateIndividualCountryElement : false;
//			}
//		}
//		catch(TimeoutException t){
//			if (driver.getTitle().contains("Maintenance Message")){
//				throw new Schneider_TimeoutException(driver.getTitle());
//			}	
//			Reporter.log(logTime() + " " + testParameters[0] + " EXCEPTION! \"" + testParameters[2] + "\" failed due to timeout.", true);
//			validateIndividualCountryElement = false;
//			if (validateCountryElements){
//				validateCountryElements = validateIndividualCountryElement == true ? validateIndividualCountryElement : false;
//			}
//		}
//		if (driver.getTitle().contains("Maintenance Message")){
//			throw new Schneider_TimeoutException(driver.getTitle());
//		}
//		return validateCountryElements;
//	}

//	private boolean validateLandingPage(String countryName) {
//		boolean countryNameIsPresent = false;
//		ArrayList<WebElement> CountryNames_Link = (ArrayList<WebElement>) driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.CountryNames_Link));
//		String strCountry = new String();
//		if (CountryNames_Link.size() > 0){
//			for (WebElement country : CountryNames_Link){
//				strCountry = country.getText();
//				if (countryName.contains(strCountry)){
//					countryNameIsPresent = true;
//					Reporter.log(logTime() + "  ...." + countryName+ " is listed on this landing page", true);
//				}
//			}
//		}
//		else{
//			Reporter.log(logTime() + "  ....There are no countries listed on this page", true);
//		}
//		return countryNameIsPresent;
//	}

//	private boolean navigateToNextCountry(int geographyElementInstance,
//			String... testParameters) {
//		testParameters[1] = "PreDispatch";
////		boolean navigateToNextCountry = fnClick(SelectCountry_Header_Link,testParameters);
////		Driver.awaitPresenceOfElement(By.xpath(Constants.WebMachine_DispatchPage.SelectCountry_Header_Link), SelectCountry_Header_Link);
////		
//		String specificGeographyLink = Constants.WebMachine_DispatchPage.Geography_Elements.
//				concat("[" + geographyElementInstance + "]/a");
//		WebElement specificElement = driver.findElement(By.xpath(specificGeographyLink));
//		Reporter.log(logTime() + "  ...." + "Navigate to the next country ", true);
//		testParameters[1] = "Dispatch";
//		boolean navigateToNextCountry = fnClick(specificElement,testParameters); 
//		return navigateToNextCountry;
//	}

	public static boolean checkCanonicalURL(String currentURL, String... testParameters)
			throws UnsupportedEncodingException {
		boolean canonicalURLExists = true;
		if (currentURL.contains(".page")){
			Reporter.log(logTime() + "  .....Interwoven page, no Canonical url required.", true);
		}
		else{
			try{
				canonicalURLExists = Driver.isElementPresent(Constants.WebMachine_CommonConstants.CanonicalURL_Check);
				WebElement canonicalURL = Driver.findElement("xPath", Constants.WebMachine_CommonConstants.CanonicalURL_Check, 15);
				Reporter.log(logTime() + "  .....Canonical Url: "+ canonicalURL.getAttribute("href"), true);
			}
			catch (TimeoutException toe) {
				canonicalURLExists = false;
				checkForMaintenanceMessage();			
				Reporter.log(logTime() + "  .....ERROR! Time out searching for Canonical URL for page: "
								+ Driver.getPageTitle(), true);
			}	
			checkForMaintenanceMessage();	
		}
		return canonicalURLExists;
	}

	private static void checkForMaintenanceMessage() {
		String pageTitle = Driver.getPageTitle();
		if (pageTitle.contains("Maintenance Message")){
			throw new Schneider_TimeoutException(pageTitle);
		}
	}

/*	public void navigateItems(int numberOfDisplayedItems, int currentItem) {
		WebElement navigationArrow = driver
				.findElement(By
						.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Arrow));
		int numberOfClicksRequired = 0; // currentProduct % numberOfDisplayedProducts; //OK up to 5
		if (currentItem > numberOfDisplayedItems) {
			numberOfClicksRequired = currentItem % numberOfDisplayedItems; // OK up to 9
			if (numberOfClicksRequired == 0) {
				numberOfClicksRequired = currentItem - numberOfDisplayedItems; // OK for 10, 15
			}
			if (currentItem % numberOfDisplayedItems != 0 && currentItem > 10) {
				numberOfClicksRequired = currentItem - numberOfDisplayedItems;
			}
		}
		for (int click = 1; click <= numberOfClicksRequired; click++) {
			fnClick(navigationArrow, "Navigation Arrow.", "Navigation", "", "");
		}
	}

	public boolean fnCheckCanonicalURL() throws UnsupportedEncodingException {
		boolean urlIsAvailable = true;
		SoftAssert sa = new SoftAssert();
		try {
			fnSwitchtoMainwindow();
			sa.assertTrue(
					fnisElementPresent(Constants.WebMachine_CommonConstants.CanonicalURL_Check),
					"ERROR! Canonical URL Not found for page : "
							+ driver.getTitle());
			Reporter.log(
					logTime() + "  .....Canonical Url: "
							+ CanonicalURL_Check.getAttribute("href"), true);
		} catch (TimeoutException toe) {
			urlIsAvailable = false;
			if (driver.getTitle().contains("Maintenance Message")){
				throw new Schneider_TimeoutException(driver.getTitle());
			}
			Reporter.log(   
					logTime() + "  ....ERROR! Time out on searching for Canonical URL. Not found for the page : "
							+ driver.getTitle(), true);
			if (driver.getTitle().contains("Error report") && 
			testData.get("testDomain").trim().contains("www-int")){
				Reporter.log(logTime() + "  ....The above error is OK because the Country selector has not been implemented "
						+ "on the INT platform. Testing will continue  ....", true);
			}
			else{
				try {
					sa.assertTrue(
							fnisElementPresent(Constants.WebMachine_CommonConstants.CanonicalURL_Check_Alternative),
							"ERROR! Canonical URL Not found for page : "
									+ driver.getTitle());
					urlIsAvailable = true;
					Reporter.log(logTime() + "  .....Canonical Url: "
							+ CanonicalURL_Check.getAttribute("href"), true);
				} catch (TimeoutException altToe) {
					if (driver.getTitle().contains("Maintenance Message")){
						throw new Schneider_TimeoutException(driver.getTitle());
					}
					urlIsAvailable = false;
					Reporter.log(   
							logTime() + "  ....ERROR! Time out on searching for Canonical URL. Not found for the page : "
									+ driver.getTitle(), true);
				}
			}
			Reporter.log(   
					logTime() + "  ...." + "ERROR! Time out on searching for Canonical URL. Not found for the page : "
							+ driver.getTitle(), true);
			urlIsAvailable = false;
		} catch (Throwable t) {
			Reporter.log(
					logTime() + "  ....ERROR! Canonical URL Not found for page : "
							+ driver.getTitle(), true);
			urlIsAvailable = false;
		}
		if (driver.getTitle().contains("Maintenance Message")){
			throw new Schneider_TimeoutException(driver.getTitle());
		}
		return urlIsAvailable;

	}*/



	protected static boolean checkLink(String webElementIdentifier,
			WebElement webElement) {
		try {
			return webElement.isEnabled() && webElement.isDisplayed();
		} catch (NoSuchElementException nse) {
			return false;
		}
	}




	public static boolean continueAfterAlert() throws UnsupportedEncodingException {
		boolean continueAfterAlert = true;
		try {
			Alert alert = Driver.getInstance().switchTo().alert();
			if (alert != null) {
				String strAlertText = alert.getText();
				continueAfterAlert = alert.getText().contains(
						"Connection error") ? false : true;
				if (strAlertText.length() < 1){
					strAlertText = "NOTHING! This alert is a blank string ....";
				}
				Reporter.log(logTime() + "  ....Alert says : " + strAlertText, true);
				alert.accept();
			}
		} catch (NoAlertPresentException nape) {
			// no need to do anything here, it's enough to have caught the exception
			//Reporter.log(logTime() + "  ....No Alert present", false);
		}
		return continueAfterAlert;
	}
	
	protected static boolean checkDomains() {
		boolean domainsAreOK = false;
		//System.out.println(domains.get("baseDomain") +" >>>>  " + domains.get("landingPageDomain"));
		if (domains.get("baseDomain").equalsIgnoreCase(domains.get("landingPageDomain")) 
			|| domains.get("socialMedia") != null
			|| domains.get("landingPageDomain").equalsIgnoreCase("partners.")
			|| domains.get("landingPageDomain").equalsIgnoreCase("home.")			
			|| (domains.get("baseDomain").equalsIgnoreCase("www-prd.") && domains.get("landingPageDomain").equalsIgnoreCase("www."))
			) {
			Reporter.log(logTime() + "  .....Base Domain = \"" + domains.get("baseDomain")
					+ "\"; Current page domain = \"" + domains.get("landingPageDomain") + "\". Domains are valid.", true);
			domainsAreOK = true;
		}
		return domainsAreOK;
	}

	public boolean validateSocialMedia(String... testParameters) {
		testParameters[2] = "Social Media";
		if (testParameters != null && testParameters.length > 3){
			if (testParameters[3].equalsIgnoreCase("Dispatch")){
				Driver.getBaseUrl(testParameters[4]);
			}
		}
		boolean socialMediaLinks = true;
		
		String xPathToBeUsed = getLocator(testParameters); // this is not needed
		List<WebElement> socialMediaElements = Driver.enumerateElements("xPath", xPathToBeUsed, testParameters[4], testParameters);
		
		Reporter.log(logTime() + "  ....Total links present: "+ socialMediaElements.size(), true);
		
		socialMediaLinks = listElements(socialMediaElements, xPathToBeUsed, testParameters);
		
		if (socialMediaLinks) {
			socialMediaLinks = validateElements(socialMediaElements, xPathToBeUsed, testParameters);
		}
		return socialMediaLinks;
	}

	//  validating elements added by Lokesh 7-2015
	
	protected static boolean validateElements(
			List<WebElement> elements, String xPathToBeUsed, String... testParameters) {
		boolean validateLinks = true;
		
		
		HashMap<String, List<WebElement>> linksWithIds = getLinksWithIds(testParameters);
		
		List<WebElement> links = null;
		String id = new String();
		for (Map.Entry<String, List<WebElement>> entry : linksWithIds.entrySet()){
			id = entry.getKey();
			links = entry.getValue();
		}
		
		
		String currentTestArea = testParameters[1];
		try {
			validateLinks = Driver.isElementPresent(xPathToBeUsed);
			if (validateLinks){
				String strLink = new String();
				validateLinks:
				for (int i = 1; i <= links.size(); i++) {
					String extension = "[" + i + "]";
					id = id.replace("//div", "//*");
					String location = id.concat(extension);
					if (currentTestArea.contains("More") ){
						location = location.concat("/a");
					}
					strLink = Driver.getLinkIdentifier("xPath", location);
					try {
						WebElement link = null;
						Reporter.log(logTime() + "  ....Validating " + strLink + ".....", true);
						
						String linkAttributeValue = Driver.findElementAttribute("xPath", location, 2, "class");
						
						if (linkAttributeValue != null){
							link = Driver.findElement("xPath", location, 10);
							if (linkAttributeValue.trim().equalsIgnoreCase("social-more")) {
								Reporter.log(logTime() + "  ....Now enumerating Social Media \"More\" Options.", true);						
								
								testParameters[1] = testParameters[1] + " More";
								xPathToBeUsed = getLocator(testParameters);
								elements = Driver.enumerateElements("xPath", xPathToBeUsed, testParameters[4], testParameters);
								
								Reporter.log(logTime() + "  ....Total links available: " + elements.size(), true);
								testParameters[1] = testParameters[1] + " Link";
								xPathToBeUsed = getLocator(testParameters);
								validateLinks = listElements(elements, xPathToBeUsed, testParameters);
								
/*								if (testParameters[1].contains("Header")){
									location = "//*[@id='header-menu']/header/div[2]/a[3]";
								}
								else if (testParameters[1].contains("Footer")){
									location = "//*[@id='footer-social']/div/a[3]";
								}*/
									
								link = Driver.findElement("xPath", location, 10);
								domains = Driver.click(link, testParameters);
								validateLinks = checkDomains();
								
								if (validateLinks) {
									validateLinks = validateElements(elements, xPathToBeUsed, testParameters);
								}
								
							} else {
								domains = Driver.click(link, testParameters);
								validateLinks = checkDomains();
							}
						}
						else{
							link = Driver.findElement("xPath", location, 10);
							domains = Driver.click(link, testParameters);
							validateLinks = checkDomains();
						}

						if (!validateLinks){
							continue validateLinks;
						}

						if (Driver.getPageTitle().contains("Error")
								|| Driver.getPageTitle().contains("404")) {
							Assert.fail("ERROR! Link " + strLink
									+ " is Broken");
						}
						Driver.chooseWindow(Driver.getCurrentUrl(), testParameters[4], testParameters);
					} catch (Throwable t) {
						validateLinks = false;
						Reporter.log(logTime() + "  ...." + testParameters[0] + " "
								+ currentTestArea + " " + testParameters[2]
								+ " links for Facebook and Twitter NOT working",
								true);
					}
				}
				Driver.chooseWindow(Driver.getCurrentUrl(), testParameters[4], testParameters);
			}
			else{
				validateLinks = false;
				Reporter.log(logTime() + "  ....ERROR! " + testParameters[1] + " " + testParameters[2]
						+ " links for Facebook and Twitter NOT found", true);
			}
		} catch (Throwable t) {
			Reporter.log(logTime() + "  ...." + testParameters[0] + " ERROR! "
					+ currentTestArea + " " + testParameters[2]
					+ " links for Facebook and Twitter NOT found", true);
			validateLinks = false;
		}
	
		return validateLinks;
	}	
	
	/*protected static boolean validateElements(
			List<WebElement> elements, String xPathToBeUsed, String... testParameters) {
		boolean validateLinks = true;
		HashMap<String, List<WebElement>> linksWithIds = getLinksWithIds(testParameters);
		
		List<WebElement> links = null;
		String id = new String();
		for (Map.Entry<String, List<WebElement>> entry : linksWithIds.entrySet()){
			id = entry.getKey();
			links = entry.getValue();
		}
		
		
		String currentTestArea = testParameters[1];
		try {
			validateLinks = Driver.isElementPresent(xPathToBeUsed);
			if (validateLinks){
				String strLink = new String();
				validateLinks:
				for (int i = 1; i <= links.size(); i++) {
					String extension = "[" + i + "]";
					id = id.replace("//div", "//*");
					String location = id.concat(extension);
					if (currentTestArea.contains("More") ){
						location = location.concat("/a");
					}
					strLink = Driver.getLinkIdentifier("xPath", location);
					try {
						WebElement link = null;
						Reporter.log(logTime() + "  ....Validating " + strLink + ".....", true);
						
						String linkAttributeValue = Driver.findElementAttribute("xPath", location, 2, "class");
						
						if (linkAttributeValue != null){
							link = Driver.findElement("xPath", location, 10);
							if (linkAttributeValue.trim().equalsIgnoreCase("social-more")) {
								Reporter.log(logTime() + "  ....Now enumerating Social Media \"More\" Options.", true);						
								
								testParameters[1] = testParameters[1] + " More";
								xPathToBeUsed = getLocator(testParameters);
								elements = Driver.enumerateElements("xPath", xPathToBeUsed, testParameters[4], testParameters);
								
								Reporter.log(logTime() + "  ....Total links available: " + elements.size(), true);
								testParameters[1] = testParameters[1] + " Link";
								xPathToBeUsed = getLocator(testParameters);
								validateLinks = listElements(elements, xPathToBeUsed, testParameters);
								
//								if (testParameters[1].contains("Header")){
//									location = "//*[@id='header-menu']/header/div[2]/a[3]";
//								}
//								else if (testParameters[1].contains("Footer")){
//									location = "//*[@id='footer-social']/div/a[3]";
//								}
									
								link = Driver.findElement("xPath", location, 10);
								domains = Driver.click(link, testParameters);
								validateLinks = checkDomains();
								
								if (validateLinks) {
									validateLinks = validateElements(elements, xPathToBeUsed, testParameters);
								}
								
							} else {
								domains = Driver.click(link, testParameters);
								validateLinks = checkDomains();
							}
						}
						else{
							link = Driver.findElement("xPath", location, 10);
							domains = Driver.click(link, testParameters);
							validateLinks = checkDomains();
						}

						if (!validateLinks){
							continue validateLinks;
						}

						if (Driver.getPageTitle().contains("Error")
								|| Driver.getPageTitle().contains("404")) {
							Assert.fail("ERROR! Link " + strLink
									+ " is Broken");
						}
						Driver.chooseWindow(Driver.getCurrentUrl(), testParameters[4], testParameters);
					} catch (Throwable t) {
						validateLinks = false;
						Reporter.log(logTime() + "  ...." + testParameters[0] + " "
								+ currentTestArea + " " + testParameters[2]
								+ " links for Facebook and Twitter NOT working",
								true);
					}
				}
				Driver.chooseWindow(Driver.getCurrentUrl(), testParameters[4], testParameters);
			}
			else{
				validateLinks = false;
				Reporter.log(logTime() + "  ....ERROR! " + testParameters[1] + " " + testParameters[2]
						+ " links for Facebook and Twitter NOT found", true);
			}
		} catch (Throwable t) {
			Reporter.log(logTime() + "  ...." + testParameters[0] + " ERROR! "
					+ currentTestArea + " " + testParameters[2]
					+ " links for Facebook and Twitter NOT found", true);
			validateLinks = false;
		}
	
		return validateLinks;
	}	*/
	
	
		//Check link page title
	protected static boolean listElements(
			List<WebElement> elements, String xPathToBeUsed, String... testParameters) {
		boolean enumerateElements = false;
		String xPath = xPathToBeUsed;
		try {
			String elementName = "";
			for (int elementInstance = 1; elementInstance <= elements.size(); elementInstance++) {
				xPathToBeUsed = xPath+"["+elementInstance+"]"+"/a";

				String attribute = Driver.findElementAttribute("xPath", xPathToBeUsed, 15, "title");
				if (attribute != null && testParameters[3].equals("Dispatch")){
					elementName = attribute;
					elementName = elementName + " - " + Driver.getLinkIdentifier(xPathToBeUsed);
				}
				else{
					elementName = Driver.getLinkIdentifier(xPathToBeUsed);
				}
				/*if (testParameters[1].contains("More")){
					xPathToBeUsed = xPath+"["+elementInstance+"]/a";
					elementName = Driver.findElementAttribute("xPath", xPathToBeUsed, 10, "text");
				}
				*/
				Reporter.log(logTime() + "  ....These are " + elementInstance
						+ ": " + elementName, true);
			}
			enumerateElements = true;
		} catch (Throwable t) {
			Reporter.log(logTime() + "  ....ERROR! "
					+ testParameters[2] + " Links column NOT found on "
					+ testParameters[1], true);
			ErrorUtil.addVerificationFailure(t);
			enumerateElements = false;
		}
		return enumerateElements;
	}

	//What defines test perameters?
	protected static String getLocator(String... testParameters) {
		String xPathToBeUsed = new String();
		if (testParameters.length > 3 && testParameters[3].length() > 0){
			switch (testParameters[3]) {
			case "Dispatch":  
				if (testParameters[1].equals("Header")) {
					xPathToBeUsed = Constants.WebMachine_DispatchPage.header_Top_Menu; 
							//Constants.WebMachine_CommonConstants.HeaderSocial_Links;
				} else if (testParameters[1].equals("Logo")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.schneiderHeaderLogo;
				} else if (testParameters[1].equals("SearchIcon")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.searchIconOnHeader;					
				} else if (testParameters[1].equals("LoginPartner")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.loginJoinPartner;
				} else if (testParameters[1].equals("Footer More")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links;					
				} else if (testParameters[1].equals("Footer More Link")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Link;
				} else if (testParameters[1].equals("Footer")) {
					xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HomeFooterSocial_Links;
				}			
				break;
			case "Home":
				if (testParameters[1].equals("Header")) {
					//xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HeaderSocial_Links;
				} else if (testParameters[1].equals("Footer More")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links;
				} else if (testParameters[1].equals("Footer More Link")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Link;						
				} else if (testParameters[1].equals("Footer")) {
					xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HomeFooterSocial_Links;
				}
				break;
				
			case "Work":
				if (testParameters[1].equals("Header")) {
					//xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HeaderSocial_Links;
				} else if (testParameters[1].equals("Footer More")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links;
				} else if (testParameters[1].equals("Footer More Link")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Link;						
				} else if (testParameters[1].equals("Footer")) {
					xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HomeFooterSocial_Links;
				}
				break;
				
			case "Partners":
				if (testParameters[1].equals("Header")) {
					//xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HeaderSocial_Links;
				} else if (testParameters[1].equals("Footer More")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links;
				} else if (testParameters[1].equals("Footer More Link")) {
					xPathToBeUsed = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Link;						
				} else if (testParameters[1].equals("Footer")) {
					xPathToBeUsed = Constants.WebMachine_HomeLandingPage.HomeFooterSocial_Links;
				}
				break;
			}
		}
		return xPathToBeUsed;
	}
	
	private static HashMap<String, List<WebElement>> getLinksWithIds(String... testParameters) {
		HashMap<String, List<WebElement>> linksWithIds = new HashMap<String, List<WebElement>>();
		List<WebElement> links = new ArrayList<WebElement>();
		String location = new String();
		if (testParameters.length > 3 && testParameters[3].length() > 0){
			switch (testParameters[3]) {
			case "Dispatch":  
				if (testParameters[1].equalsIgnoreCase("Header")) {
					if (testParameters[2].contains("Social Media")) {
						location = Constants.WebMachine_CommonConstants.HeaderSocial_Links;
					}
					links = Driver.findElements("xPath", location);
					linksWithIds.put(location, links);
					break;
				} 
				if (testParameters[1].equalsIgnoreCase("Header More Link")) {
					if (testParameters[2].contains("Social Media")) {
						location = Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Link;
					}
					links = Driver.findElements("xPath", location);
					linksWithIds.put(location, links);
					break;
				}
				if (testParameters[1].equalsIgnoreCase("Footer More Link")) {
					if (testParameters[2].contains("Social Media")) {
						location = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Link;
					}
					links = Driver.findElements("xPath", location);
					linksWithIds.put(location, links);
					break;
				}				
				else if (testParameters[1].equalsIgnoreCase("Footer")) {
					if (testParameters[2].contains("Social Media")) {
						location = Constants.WebMachine_CommonConstants.FooterSocial_Links;
					}
					if (testParameters[2].contains("About Us")) {
						location = Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link;
					}
					if (testParameters[2].contains("Legal Links")) {
						location = Constants.WebMachine_CommonConstants.FooterLegal_Links;
					}
					links = Driver.findElements("xPath", location);
					linksWithIds.put(location, links);
					break;
				}
			case "Home":
				if (testParameters[1].contains("Header")) {
					//links = HomeFooterSocial_Links;
					//Maybe someday there will be social links on the header ! DH
				} else {
					if (testParameters[2].contains("Social Media")) {
						location = Constants.WebMachine_HomeLandingPage.HomeFooterSocial_Links;
					}
					if (testParameters[2].contains("About Us")) {
						//location = Constants.WebMachine_CommonConstants.HeaderSocial_Links;
					}
					links = Driver.findElements("xPath", location);
					linksWithIds.put(location, links);
				}
				break;
			}
		}
		return linksWithIds;
	}
	protected static String getLinkIdentifier(String linkIdentifier) {
		WebElement webElementLink = driver.findElement(By.xpath(linkIdentifier));
		linkIdentifier = webElementLink.getAttribute("title").trim().length() > 0 ? webElementLink.getAttribute("title").trim() : webElementLink.getText().trim();
		if (linkIdentifier.length() > 0) {
		} 
		else {
			String hReflink = webElementLink.getAttribute("href");
			if (hReflink != null){
				int firstIndex = hReflink.contains("www") ? webElementLink
						.getAttribute("href").indexOf(".") : webElementLink
						.getAttribute("href").indexOf("/") + 1;
				String hReflinkRemainder = webElementLink.getAttribute("href")
						.substring(firstIndex + 1);
				int secondIndex = hReflinkRemainder.indexOf(".");
				linkIdentifier = webElementLink.getAttribute("href").substring(
						firstIndex + 1, firstIndex + secondIndex + 1);
			}
		}
		return linkIdentifier;
	}

	public String getLinkIdentifier(WebElement webElementLink) {
		String linkIdentifier = webElementLink.getAttribute("title").trim()
				.length() > 0 ? webElementLink.getAttribute("title").trim()
				: webElementLink.getText().trim();
		if (linkIdentifier.length() > 0) {

		} else {
			String hReflink = webElementLink.getAttribute("href");
			int firstIndex = hReflink.contains("www") ? webElementLink
					.getAttribute("href").indexOf(".") : webElementLink
					.getAttribute("href").indexOf("/") + 1;
			String hReflinkRemainder = webElementLink.getAttribute("href")
					.substring(firstIndex + 1);
			int secondIndex = hReflinkRemainder.indexOf(".");
			linkIdentifier = webElementLink.getAttribute("href").substring(
					firstIndex + 1, firstIndex + secondIndex + 1);
		}
		return linkIdentifier;
	}

	// Close window if that is open
	public static void closeSurveyWindowIfItExists(String... testParameters) {
		ArrayList<String> tabBase = new ArrayList<String> (Driver.getWindowHandles());
		 if (tabBase.size() > 1){
			 Reporter.log(logTime() + "  ....Closing Survey window", true);
			 Driver.chooseWindow(Driver.getCurrentUrl(), testParameters[4], testParameters);
		 }
	}
	
	protected By getBy(String locator) {
		return By.xpath(locator);
	}





	public void fnInputbox(WebElement wbInputbox, String strValue) {
		Reporter.log(logTime() + "  ....Entering " + strValue, true);
		try {
			wbInputbox.sendKeys(strValue);
		} catch (NoSuchElementException nsee) {
			Reporter.log(logTime() + "  ....ERROR ! No such Input box exists for \"" +wbInputbox.getText()+ "\" to enter value: " + strValue, true);
		} catch (Throwable t) {
			Reporter.log(logTime() + "  ....ERROR ! Couldn't enter value : " + strValue + " in Inputbox.", true);
		}
	}

	
	@SuppressWarnings("unused")
	protected WebMachine_Base_Page() { // Constructor is made protected

		Config = new Properties();
		ENV = new Properties();
		// driver = null;

		try {

			FileInputStream fs = new FileInputStream(
					Constants.Paths.Config_FILE_PATH);
			if (fs == null) {
				//System.out.println("fs is null");
			} else
				Config.load(fs);
			// Init ENV Properties

			String filename = Config.getProperty("environment") + ".properties";
			////System.out.println("Environment Filename :" + filename);
			// Reporter.log("Environment Filename :"+ filename, true);
			fs = new FileInputStream(Constants.Paths.Config_FOLDER_PATH
					+ filename);
			ENV.load(fs);
		} catch (Exception e) {
			// e.printStackTrace();
			//System.out.println("Unable to load file input stream");
		}
	}

	
	//Single BeforeSuite in the Whole suite
	@AfterSuite(alwaysRun = true)
	public void tearDownSuite() {
		try{
			Driver.tearDown();
			Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log(logTime() + " " + ":::::::::::::::::::::CLOSED BROWSER:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log(logTime() + " " + "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::", true);
			Reporter.log("\n", true);
		}
		catch(Exception ex){

		}
	}

	public void log(String mesg) {
		APPLICATION_LOGS.debug(mesg);
	}


	public static String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static String logDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		String s = dateFormat.format(cal.getTime());
		System.out.println(s.split(" ")[0]+"_"+s.split(" ")[1]);
		
	//	String s2 = s.split(" ")[1].split(":")[0]+"_"+s.split(" ")[1].split(":")[1]+"_"+s.split(" ")[1].split(":")[2];
		
		System.out.println(s.split(" ")[0]+s.split(" ")[1].split(":")[0]+"_"+s.split(" ")[1].split(":")[1]+"_"+s.split(" ")[1].split(":")[2]);
				//return dateFormat.format(cal.getTime());
		//return s.split(" ")[0]+"_"+s.split(" ")[1];
		return s.split(" ")[0]+"_"+s.split(" ")[1].split(":")[0]+"_"+s.split(" ")[1].split(":")[1]+"_"+s.split(" ")[1].split(":")[2];
	}


	public static boolean validateElements(List<WebElement> elements, String locator, String location, String... testParameters) {
		boolean elementsValidated = false;
		if (elements.size() > 0){
			for (int elementInstance = 1; elementInstance <= elements.size(); elementInstance++) {
				String specificElement = location.concat("[" + elementInstance + "]/a");
				String elementName = Driver.getLinkIdentifier(locator, specificElement);
				Reporter.log(logTime() + "  ....Link "
						+ elementInstance + " : " + elementName, true);
			}
			elementsValidated = true;
		}
		return elementsValidated;
	}
//
//
//	public static void tearDown() {
//		Driver.tearDown();
//	}
	
	
	// click on carousel and then click on respective link like @Home, @Work
		public static String getZoneUrl(String... testParameters) {
			
		//	closeCountrySelector(Constants.WebMachine_CommonConstants.countrySelectorClose);

			String dispatchPageCarosel = new String();
			boolean carouselPresent = false;
			
			
			Driver.navigateTo(testParameters[2]);

			Driver.waitForPageLoad(Driver.getInstance(), 30,"Schneider");

			String xpathToBeUsed = Constants.WebMachine_CommonConstants.rotatingCarousel;
			try{
				carouselPresent = Driver.isElementAvailable("xPath",xpathToBeUsed);
						//Driver.isElementPresent("xPath",xpathToBeUsed);

				if(carouselPresent){
					List<WebElement> carouselList = Driver.enumerateElements("xPath", xpathToBeUsed, testParameters[2], testParameters);

					Reporter.log(logTime() + " Number of Carousels on the "+ testParameters[1] +" Page are : "+carouselList.size(), true);

					try{
						if(carouselList.size()>0)
						{
							String classAttribute = Driver.findElementAttribute("xPath", xpathToBeUsed+"[1]", 15, "class");

							if("current".equalsIgnoreCase(classAttribute)){
								Reporter.log(logTime() + " The Carousel for clicking on Zones on "+ testParameters[1] +" Page is already selected !! ",true);
								Driver.click("xPath", testParameters[0]); // clicking the zone

								Driver.waitForPageLoad(Driver.getInstance(), 30,"Schneider");
								dispatchPageCarosel = Driver.getCurrentUrl();
							}
							else{
								Driver.click("xPath", xpathToBeUsed+"[1]");
								Reporter.log(logTime() + " The Carousel for clicking on Zones on "+ testParameters[1] +" Page is clicked now !! ",true);
								Driver.click("xPath", testParameters[0]);

								Driver.waitForPageLoad(Driver.getInstance(), 30,"Schneider");
								dispatchPageCarosel = Driver.getCurrentUrl();

							}

						}

					}catch (StaleElementReferenceException sere) {
						Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
					}
					catch (NoSuchElementException nsee) {

						Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

					} catch (ElementNotVisibleException e) {

						e.printStackTrace();
					} 		
				}
				else{
					Driver.click("xPath", testParameters[0]); // clicking the zone

					Driver.waitForPageLoad(Driver.getInstance(), 30,"Schneider");
					dispatchPageCarosel = Driver.getCurrentUrl();
				}
			}catch (StaleElementReferenceException sere) {
				Reporter.log(logTime() + "  ....StaleElementReferenceException message is " + sere.getMessage(), true);
			}
			catch (NoSuchElementException nsee) {

				Reporter.log(logTime() + "  ....NoSuchElementException message is " + nsee.getMessage(), true);

			} catch (ElementNotVisibleException e) {

				e.printStackTrace();
			} 	
			//	System.out.println(dispatchPageCarosel + "  :  is the url");
			return dispatchPageCarosel;						
		}
	
		
		// close notification about country selector and stay on current website
		
		public static void closeCountrySelector(String xpath) {
			
			boolean isElementPresent = Driver.isElementPresent(xpath);
			if(isElementPresent)
			{
				Reporter.log(logTime() + "  ....Closing Country Selector Banner window", true);
				Driver.click("xPath", xpath);
				Reporter.log(logTime() + "  ....Country Selector Banner closed", true);
			}else{
				Reporter.log(logTime() + "  ....Country Selector Banner could not be closed", true);
			}
			
		}
	
}
